var mongoose    = require('mongoose'),
  NewsItem      = require('./newsModel').NewsItem,
  striptags     = require('striptags'),
  Promise       = require('bluebird');

var host = process.env.DB_HOST || 'localhost';
var port = process.env.DB_PORT || '27018';

mongoose.Promise = global.Promise;
mongoose.set('debug', false);
mongoose.connection.on('error', (err) => {
  console.log(err);
});

new Promise((resolve, reject) => {
  var mongooseThenable;
  mongoose.connection.on('open', () => {
    return resolve({mongooseThenable: mongooseThenable});
  });
  mongoose.connection.once('error', (err) => { return reject(err); });
  mongooseThenable = mongoose.connect("mongodb://admin:p1230h6t34qd4i7ex@" + host + ":" + port + "/news-feed?authSource=admin");
})
  .then((db) => {
    NewsItem.find({}).then((news) => {

      console.log('<?xml version="1.0" encoding="utf-8"?>');
      console.log('<sphinx:docset>');
      console.log('<sphinx:schema>');

      console.log(' <sphinx:field name="content" />');
      console.log(' <sphinx:attr name="element_id" type="string" />');
      console.log(' <sphinx:field name="title" />');
      // console.log(' <sphinx:field name="tags" />');
      // console.log(' <sphinx:field name="published" type="timestamp" />');

      console.log('</sphinx:schema>');

      for (var i = 0; i <= news.length; i++) {

        if(i == news.length) {
          console.log('</sphinx:docset>');
          process.exit();
        } else {
          var doc = news[i];
          console.log('<sphinx:document id="' + (i+1) + '" element_id="' + doc._id + '">');

          console.log('<content><![CDATA[[');
          console.log(striptags(doc.description));
          console.log(']]></content>');

          console.log('<title><![CDATA[[');
          console.log(doc.title);
          console.log(']]></title>');

          // console.log('<published>' + (new Date()).getTime().toString() + '</published>');

          console.log('</sphinx:document>');
        }
      }
    });
  });
