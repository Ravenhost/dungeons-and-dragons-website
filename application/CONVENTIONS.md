CONVENTIONS
===========

1 Обработка ошибок
-----------------

### 1.1 Обработка ошибок в контроллерах

Каждый контроллер должен возвращать объект promise. Цепочка промисов в контроллере
должна завершаться вызвовом метода catch, который выглядит следующим образом:
```
.catch((err) => { return next(err instanceof Error ? err : new VError(err)); });
```

-----------------

2 Запуск задач по времени
-----------------

Файл timers.json должен находиться в корне проекта. Этот файл описывает запускаемы по расписанию скрипты.

Ключи верхнего уровня объекта должны отвечать требованиям предъявляемым к именам файлов. Фактически, это имена файлов systemd юнитов без расширений.
В systemd принято использовать kebab-case для таких имен.

В timer использован синтаксис из [systemd.timer](https://www.freedesktop.org/software/systemd/man/systemd.timer.html) (OnCalendar=,  OnActiveSec=, OnBootSec=, OnStartupSec=, OnUnitActiveSec=, OnUnitInactiveSec=) и [systemd.time](https://www.freedesktop.org/software/systemd/man/systemd.time.html)
См. https://www.freedesktop.org/software/systemd/man/systemd.time.html#Calendar%20Events

_________________

3 Обработка картинок сервером
-----------------

$ sudo add-apt-repository ppa:dhor/myway

$ sudo apt-get update

$ sudo apt-get install graphicsmagick
