'use strict';

const gulp = require('gulp');
const less = require('gulp-less');
const runSequence = require('run-sequence');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const notify = require('gulp-notify');

const srcPath = './lib/client/sources/less/style.less';
const watchPath = './lib/client/sources/less/**/*.less';
const distPath = './public/css/';

gulp.task('style', function () {
  return gulp.src(srcPath)
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(sourcemaps.init())
      .pipe(less())
      .pipe(autoprefixer('last 2 version'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(distPath))
});

gulp.task('style-build', function () {
  return gulp.src(srcPath)
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(less())
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest(distPath));
});

gulp.task('notify', function () {
  return gulp.src(srcPath)
    .pipe(notify({message: 'all your base are belong to us', onLast: true}));
});

/////////////////////////////////////////////////
// Final

gulp.task('watch', function() {
  // Watch .less files
  gulp.watch(watchPath, function(){
    runSequence('style', ['notify'])
  });
});

gulp.task('default', function(callback) {
  runSequence(['style', 'watch'], callback)
});

gulp.task('build', function(callback) {
  runSequence(['style-build'], callback)
});

/////////////////////////////////////////////////
// Utils
function onError(err) {
  console.log(err);
  this.emit('end');
}
