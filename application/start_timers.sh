#!/bin/sh -e

/bin/cp ./timers/* /lib/systemd/system
systemctl daemon-reload
systemctl enable cleanDatabase.timer
systemctl enable getNews.timer

systemctl start cleanDatabase.timer
systemctl start getNews.timer