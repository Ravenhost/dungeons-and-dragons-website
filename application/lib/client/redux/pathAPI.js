/**
 * Created by ilya on 04.03.17.
 */
try {
  const config = require(`../../server/config/env/${process.env.NODE_ENV}.json`);

  let PORT = false;

  if (process.env.APP_HOST) {
    PORT = process.env.APP_PORT ? process.env.APP_PORT : config.http.port;
  } else {
    PORT = config.http.port;
  }

  if (typeof window === 'undefined') {
    module.exports = `http://localhost${PORT && `:${PORT}`}/api`;
  } else {
    module.exports = '/api';
  }
} catch (e) {
  console.log(e.message);
  module.exports = '/api';
}
