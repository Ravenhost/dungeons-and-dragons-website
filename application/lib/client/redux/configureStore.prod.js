import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

export default function (initialState = {}) {
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunk)
  );

  store.subscribe(()=>{
    if(typeof localStorage !== 'undefined')
      localStorage.setItem('reduxState', JSON.stringify(store.getState()))
  });

  return store;
}
