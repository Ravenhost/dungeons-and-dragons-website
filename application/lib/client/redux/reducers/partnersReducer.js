import Immutable                      from 'immutable';
import {
  REQUEST_USER_PARTNERS,
  RECEIVE_USER_PARTNERS,
  ERROR_USER_PARTNERS,
  REQUEST_DELETE_PARTNERS,
  RECEIVE_DELETE_PARTNERS,
  ERROR_DELETE_PARTNERS,
  REQUEST_MUTUAL_PARTNERS,
  RECEIVE_MUTUAL_PARTNERS,
  ERROR_MUTUAL_PARTNERS,
  REQUEST_ADD_PARTNERS,
  RECEIVE_ADD_PARTNERS,
  ERROR_ADD_PARTNERS
}                                     from '../actions/partnersActions';
import {
  RECEIVE_ACCEPT_REQUEST_PARTNERS
}                                     from '../actions/partnersRequestActions';


const initialState = {
  usersPartners: {},
  mutualPartners: {},
  loaded: false,
  loading: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case REQUEST_DELETE_PARTNERS:
    case REQUEST_USER_PARTNERS:
    case REQUEST_MUTUAL_PARTNERS:
    case REQUEST_ADD_PARTNERS:
      return Immutable
        .fromJS(state)
        .set('loaded', false)
        .set('loading', true)
        .toJS();
    case RECEIVE_ACCEPT_REQUEST_PARTNERS:
      console.log(action);
      return Immutable
        .fromJS(state)
        .update('usersPartners', usersPartners => {
          const isExist = usersPartners.find((value, key) => {
            return key === action.userId;
          });

          if (typeof isExist !== 'undefined') {
            return usersPartners.update(action.userId, partners => {
              return partners
                .update('partners', list => {
                  return list.insert(0, action.partner);
                });
            });
          }
          return usersPartners;
        })
        .toJS();
    case RECEIVE_USER_PARTNERS:
      return Immutable
        .fromJS(state)
        .set('loaded', true)
        .set('loading', false)
        .update('usersPartners', usersPartners => {
          const isExist = usersPartners.find((value, key) => {
            return key === action.userId;
          });

          if (typeof isExist !== 'undefined' && action.page !== 1) {
            return usersPartners.update(action.userId, partners => {
              return partners
                .set('page', action.page)
                .update('isLast', () => {
                  return action.partners.length === 0;
                })
                .update('partners', list => {
                  return list.concat(action.partners);
                });
            });
          }
          return usersPartners.set(action.userId, {
            partners: action.partners,
            page: action.page,
            search: action.search,
            isLast: action.partners.length === 0
          });
        })
        .toJS();
    case RECEIVE_DELETE_PARTNERS:
      return Immutable
        .fromJS(state)
        .set('loaded', true)
        .set('loading', false)
        .update('usersPartners', usersPartners => {
          return usersPartners.update(action.userId, partners => {
            return partners
              .update('partners', list => {
                const deleteIndex = list.findIndex((value) => {
                  return value.toJS()._id === action.partnerId;
                });

                return list.delete(deleteIndex);
              });
          });
        })
        .toJS();
    case RECEIVE_MUTUAL_PARTNERS:
      return Immutable
        .fromJS(state)
        .set('loaded', true)
        .set('loading', false)
        .update('mutualPartners', mutualPartners => {
          const isExist = mutualPartners.find((value, key) => {
            return key === action.partnerId;
          });

          if (typeof isExist !== 'undefined' && action.page !== 1) {
            return mutualPartners.update(action.partnerId, partners => {
              return partners
                .set('page', action.page)
                .update('isLast', () => {
                  return action.partners.length === 0;
                })
                .update('partners', list => {
                  return list.concat(action.partners);
                });
            });
          }
          return mutualPartners.set(action.partnerId, {
            partners: action.partners,
            page: action.page,
            search: action.search,
            isLast: action.partners.length === 0
          });
        })
        .toJS();
    case RECEIVE_ADD_PARTNERS:
      return Immutable
        .fromJS(state)
        .set('loaded', true)
        .set('loading', false)
        .toJS();
    case ERROR_DELETE_PARTNERS:
    case ERROR_USER_PARTNERS:
    case ERROR_MUTUAL_PARTNERS:
    case ERROR_ADD_PARTNERS:
      return Immutable
        .fromJS(state)
        .set('loaded', true)
        .set('loading', false)
        .toJS();
    default:
      return state;
  }
}
