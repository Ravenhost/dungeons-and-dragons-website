import Immutable  from 'immutable';
import {
  HIDE_MAIN_MODAL,
  SHOW_MAIN_MODAL,
  NONE_MODAL_TYPE
}                 from '../actions/modalActions';

const initialState = {
  show: false,
  modalProps:{},
  modalType: ''
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SHOW_MAIN_MODAL:
      return Immutable
        .fromJS(state)
        .setIn([ 'modalType' ], action.modalType)
        .setIn([ 'modalProps' ], action.modalProps)
        .setIn([ 'show' ], true)
        .toJS();
    case HIDE_MAIN_MODAL:
      return Immutable
        .fromJS(state)
        .setIn([ 'modalType' ], NONE_MODAL_TYPE)
        .setIn([ 'modalProps' ], {})
        .setIn([ 'show' ], false)
        .toJS();
    default:
      return state;
  }
}
