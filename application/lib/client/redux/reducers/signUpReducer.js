import Immutable from 'immutable';
import {
  CHECK_BANK_DETAILS_REQUEST,
  CHECK_BANK_DETAILS_RESPONSE,
  SIGN_UP_REQUEST,
  SIGN_UP_RESPONSE
} from '../types';

const initialState = {
  ogrn: '',
  bankDetails: {
    isFetching: false,
    fetchingError: '',
    isFetched: false,
    data: {}
  },
  signingUp: {
    isInProgress: false,
    isFetched: false,
    errorMsg: ''
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CHECK_BANK_DETAILS_REQUEST:
      return Immutable
        .fromJS(state)
        .setIn([ 'ogrn' ], action.ogrn)
        .setIn(['bankDetails', 'isFetched'], false)
        .setIn(['bankDetails', 'isFetching'], true)
        .toJS();

    case CHECK_BANK_DETAILS_RESPONSE:
      return Immutable
        .fromJS(state)
        .setIn(['bankDetails', 'isFetched'], true)
        .setIn(['bankDetails', 'isFetching'], false)
        .setIn(['bankDetails', 'data'], action.json)
        .toJS();

    case SIGN_UP_REQUEST:
      return Immutable
        .fromJS(state)
        .setIn(['signingUp', 'isInProgress'], true)
        .setIn(['signingUp', 'isError'], false)
        .setIn(['signingUp', 'isSuccessful'], false)
        .toJS();

    case SIGN_UP_RESPONSE:
      return Immutable
        .fromJS(state)
        .setIn(['signingUp', 'isInProgress'], false)
        .setIn(['signingUp', 'isFetched'], true)
        .setIn(['signingUp', 'data'], action.json)
        .setIn(['signingUp', 'code'], action.code)
        .toJS();

    default:
      return state;
  }
}
