import Immutable from 'immutable';
import {
  SIGN_IN_REQUEST,
  SIGN_IN_FAIL,
  CONFIRM_EMAIL_REQUEST,
  CONFIRM_EMAIL_RESPONSE,
  SIGN_IN_RESET_STORE
} from '../types';

const initialState = {
  signingIn: {
    isFetching: false,
    isFetched: false,
    hasError: false,
    errorCode: '',
    errorMessage: '',
    data: {}
  },
  resendConfirmEmail: {
    isFetching: false,
    isFetched: false,
    hasError: false,
    data: {}
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SIGN_IN_REQUEST:
      return Immutable
        .fromJS(state)
        .setIn(['signingIn', 'isFetching'], true)
        .setIn(['signingIn', 'isFetched'], false)
        .toJS();

    case SIGN_IN_FAIL:
      return Immutable
        .fromJS(state)
        .setIn(['signingIn', 'isFetching'], false)
        .setIn(['signingIn', 'isFetched'], true)
        .setIn(['signingIn', 'hasError'], true)
        .setIn(['signingIn', 'errorCode'], action.errorCode)
        .setIn(['signingIn', 'errorMessage'], action.errorMessage)
        .setIn(['signingIn', 'data'], action.json)
        .setIn(['signingIn', 'status'], action.status)
        .toJS();

    case SIGN_IN_RESET_STORE:
      return Immutable
        .fromJS(state)
        .setIn(['signingIn', 'isFetching'], false)
        .setIn(['signingIn', 'isFetched'], true)
        .setIn(['signingIn', 'hasError'], false)
        .setIn(['signingIn', 'errorCode'], 0)
        .setIn(['signingIn', 'errorMessage'], '')
        .toJS();

    case CONFIRM_EMAIL_REQUEST:
      return Immutable
        .fromJS(state)
        .setIn(['resendConfirmEmail', 'isFetching'], true)
        .setIn(['resendConfirmEmail', 'isFetched'], false)
        .setIn(['resendConfirmEmail', 'hasError'], false)
        .toJS();
    case CONFIRM_EMAIL_RESPONSE:
      return Immutable
        .fromJS(state)
        .setIn(['resendConfirmEmail', 'isFetching'], false)
        .setIn(['resendConfirmEmail', 'isFetched'], true)
        .setIn(['resendConfirmEmail', 'hasError'], action.hasError)
        .setIn(['resendConfirmEmail', 'data'], action.json)
        .setIn(['resendConfirmEmail', 'status'], action.status)
        .toJS();

    default:
      return state;
  }
}
