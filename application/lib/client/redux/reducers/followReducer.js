import Immutable                      from 'immutable';
import {
  REQUEST_USER_SUBSCRIPTIONS,
  RECEIVE_USER_SUBSCRIPTIONS,
  ERROR_USER_SUBSCRIPTIONS,
  REQUEST_DELETE_SUBSCRIPTIONS,
  RECEIVE_DELETE_SUBSCRIPTIONS,
  ERROR_DELETE_SUBSCRIPTIONS,
  REQUEST_USER_FOLLOWERS,
  RECEIVE_USER_FOLLOWERS,
  ERROR_USER_FOLLOWERS
}                                     from '../actions/followAction';


const initialState = {
  subscriptions: {
    objects:[],
    page: 0,
    search: '',
    isLast: false,
    loaded: false,
    loading: false
  },
  followers:  {
    objects:[],
    page: 0,
    search: '',
    isLast: false,
    loaded: false,
    loading: false
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case REQUEST_USER_SUBSCRIPTIONS:
    case REQUEST_DELETE_SUBSCRIPTIONS:
      return Immutable
        .fromJS(state)
        .update('subscriptions', subscriptions => {
          return subscriptions
            .set('loaded', false)
            .set('loading', true);
        })
        .toJS();
    case REQUEST_USER_FOLLOWERS:
      return Immutable
        .fromJS(state)
        .update('followers', followers => {
          return followers
            .set('loaded', false)
            .set('loading', true);
        })
        .toJS();
    case RECEIVE_USER_SUBSCRIPTIONS:
      return Immutable
        .fromJS(state)
        .update('subscriptions', subscriptions => {
          return subscriptions
            .set('loaded', true)
            .set('loading', false)
            .set('page', action.page)
            .set('search', action.search)
            .set('isLast', action.subscriptions.length === 0)
            .update('objects', list => {
              if (action.page === 1) {
                return action.subscriptions;
              }
              return list.concat(action.subscriptions);
            });
        })
        .toJS();
    case RECEIVE_USER_FOLLOWERS:
      return Immutable
        .fromJS(state)
        .update('followers', followers => {
          return followers
            .set('loaded', true)
            .set('loading', false)
            .set('page', action.page)
            .set('search', action.search)
            .set('isLast', action.followers.length === 0)
            .update('objects', list => {
              if (action.page === 1) {
                return action.followers;
              }
              return list.concat(action.followers);
            });
        })
        .toJS();
    case RECEIVE_DELETE_SUBSCRIPTIONS:
      return Immutable
        .fromJS(state)
        .update('subscriptions', subscriptions => {
          return subscriptions
            .set('loaded', true)
            .set('loading', false)
            .update('objects', list => {
              return list.filter((value) => {
                return value.toJS()._id !== action.subscriptionId;
              });
            });
        })
        .toJS();
    case ERROR_USER_SUBSCRIPTIONS:
    case ERROR_DELETE_SUBSCRIPTIONS:
      return Immutable
        .fromJS(state)
        .update('subscriptions', subscriptions => {
          return subscriptions
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();
    case ERROR_USER_FOLLOWERS:
      return Immutable
        .fromJS(state)
        .update('followers', followers => {
          return followers
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();
    default:
      return state;
  }
}
