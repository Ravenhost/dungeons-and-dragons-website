import Immutable                      from 'immutable';
import {
  REQUEST_CREATE_NEWS,
  RECEIVE_CREATE_NEWS,
  ERROR_CREATE_NEWS,
  REQUEST_CHANGE_NEWS,
  RECEIVE_CHANGE_NEWS,
  ERROR_CHANGE_NEWS,
  REQUEST_ADD_ATTACHMENT_NEWS,
  RECEIVE_ADD_ATTACHMENT_NEWS,
  ERROR_ADD_ATTACHMENT_NEWS,
  REQUEST_DELETE_ATTACHMENT_NEWS,
  ERROR_DELETE_ATTACHMENT_NEWS,
  RECEIVE_DELETE_ATTACHMENT_NEWS,
  REQUEST_GET_EDITED_NEWS,
  RECEIVE_GET_EDITED_NEWS,
  ERROR_GET_EDITED_NEWS,
  REQUEST_REPOST_NEWS,
  RECEIVE_REPOST_NEWS,
  ERROR_REPOST_NEWS,
  REQUEST_GET_SINGLE_NEWS,
  RECEIVE_GET_SINGLE_NEWS,
  ERROR_GET_SINGLE_NEWS
}                                     from '../actions/newsActions';
import {
  REQUEST_NEWS,
  RECEIVE_NEWS,
  COMPANY_FEED_LIKE_POST_RESPONSE
}                                     from '../types';
import  {
  clearSingleNews
}                                     from '../../redux/utils/helpers';

const initialState = {
  main: {
    list: [],
    page: 0,
    loaded: false,
    loading: false
  },
  economy: {
    list: [],
    page: 0,
    loaded: false,
    loading: false
  },
  politics: {
    list: [],
    page: 0,
    loaded: false,
    loading: false
  },

  /* Create, edit, repost news object */
  singleNews: {
    _id: null,
    sourceId: null,
    likes:[],
    isHiddenComment: true,
    repostsCount:0,
    likesCount:0,
    owner:null,
    title: '',
    images: [],
    inMaking: false,
    datePublished: null,
    description: '',
    text: '',
    hashtag: '',
    files: [],
    loaded: false,
    loading: false,
    errors: {
      attachments:[],
      images:[],
      others:null
    }
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case REQUEST_NEWS:
      return Immutable
        .fromJS(state)
        .updateIn([ action.category ], category => {
          return category
            .set('loaded', false)
            .set('loading', true);
        })
        .toJS();

    case RECEIVE_NEWS:
      return Immutable
        .fromJS(state)
        .updateIn([ action.category ], category => {
          return category
            .updateIn([ 'list' ], list => {
              return list.concat(action.news);
            })
            .updateIn([ 'page' ], page => {
              return action.news.length ? action.page : page;
            })
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();

    case REQUEST_CHANGE_NEWS:
    case REQUEST_ADD_ATTACHMENT_NEWS:
    case REQUEST_DELETE_ATTACHMENT_NEWS:
    case REQUEST_REPOST_NEWS:
      return Immutable
        .fromJS(state)
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('loaded', false)
            .set('loading', true)
            .updateIn([ 'errors' ], errors => {
              return errors
                .set('attachments', [])
                .set('images', [])
                .set('others', null);
            });
        })
        .toJS();
    case REQUEST_CREATE_NEWS:
    case REQUEST_GET_EDITED_NEWS:
    case REQUEST_GET_SINGLE_NEWS:
      return clearSingleNews(Immutable
        .fromJS(state)
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('loaded', false)
            .set('loading', true);
        })
      )
        .toJS();
    case ERROR_CHANGE_NEWS:
    case ERROR_CREATE_NEWS:
    case ERROR_GET_EDITED_NEWS:
    case ERROR_ADD_ATTACHMENT_NEWS:
    case ERROR_DELETE_ATTACHMENT_NEWS:
    case ERROR_REPOST_NEWS:
    case ERROR_GET_SINGLE_NEWS:
      return Immutable
        .fromJS(state)
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('loaded', true)
            .set('loading', false)
            .updateIn([ 'errors' ], errors => {
              return errors
                .set('others', action.error);
            });
        })
        .toJS();
    case RECEIVE_CREATE_NEWS:
    case RECEIVE_REPOST_NEWS:
      return clearSingleNews(Immutable
        .fromJS(state))
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('loaded', true)
            .set('loading', false)
            .set('_id', action.id);
        })
        .toJS();
    case RECEIVE_GET_EDITED_NEWS:
      return Immutable
        .fromJS(state)
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('loaded', true)
            .set('loading', false)
            .set('_id', action.news._id)
            .set('title', action.news.title || '')
            .set('images', action.news.images || [])
            .set('text', action.news.text || '')
            .set('files', action.news.files || '')
            .set('inMaking', action.news.inMaking || false)
            .set('datePublished', action.news.datePublished)
            .set('hashtag', action.news.hashtag || '');
        })
        .toJS();
    case RECEIVE_CHANGE_NEWS:
      return Immutable
        .fromJS(state)
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();
    case RECEIVE_ADD_ATTACHMENT_NEWS:
      return Immutable
        .fromJS(state)
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('loaded', true)
            .set('loading', false)
            .updateIn([ 'errors' ], errors => {
              const attachmentsErrors = action.errors === null ? [] : action.errors.attachments || [];
              const imagesErrors = action.errors === null ? [] : action.errors.images || [];

              return errors
                .set('attachments', attachmentsErrors)
                .set('images', imagesErrors);
            })
            .updateIn([ 'files' ], files => {
              return [...files, ...action.files];
            })
            .updateIn([ 'images' ], images => {
              return [...images, ...action.images];
            });
        })
        .toJS();
    case RECEIVE_DELETE_ATTACHMENT_NEWS:
      return Immutable
        .fromJS(state)
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('loaded', true)
            .set('loading', false)
            .updateIn([ action.typeDelete ], list => {
              return list.filter((name) => {
                return name !== action.name;
              });
            });
        })
        .toJS();
    case RECEIVE_GET_SINGLE_NEWS:
      return Immutable
        .fromJS(state)
        .update('singleNews', news => {
          return news
            .set('_id', action.news._id)
            .set('sourceId', action.news.sourceId)
            .set('inMaking', action.news.inMaking)
            .set('files', action.news.files)
            .set('reposts', action.news.reposts)
            .set('likes', action.news.likes)
            .set('repostsCount', action.news.repostsCount)
            .set('likesCount', action.news.likesCount)
            .set('images', action.news.images)
            .set('owner', action.news.owner)
            .set('hashtag', action.news.hashtag)
            .set('text', action.news.text)
            .set('datePublished', action.news.datePublished)
            .set('title', action.news.title)
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();
    case COMPANY_FEED_LIKE_POST_RESPONSE:
      return Immutable
        .fromJS(state)
        .updateIn([ 'singleNews' ], news => {
          return news
            .set('likesCount', action.json.likesCount)
            .set('likes', action.json.likes);
        })
        .toJS();
    default:
      return state;
  }
}
