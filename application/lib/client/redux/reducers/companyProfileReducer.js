import Immutable                            from 'immutable';
import {
  COMPANY_PROFILE_GENERAL_REQUEST,
  COMPANY_PROFILE_GENERAL_RESPONSE,
  COMPANY_PROFILE_SET_BRAND_LOCAL,
  COMPANY_PROFILE_SET_DESCRIPTION_LOCAL,
  COMPANY_PROFILE_SET_AVATAR_LOCAL
}                                           from '../types';
import {
  REQUEST_PROFILE_COUNTERS,
  RECEIVE_PROFILE_COUNTERS,
  ERROR_PROFILE_COUNTERS
}                                           from '../actions/companyProfileActions';

const initialState = {
  id: '',
  profile: {
    isFetching: false,
    isFetched: false,
    hasError: false,
    data: {}
  },
  counters: {
    followers: 0,
    subscriptions: 0,
    outgoing: 0,
    incoming: 0,
    partners: 0,
    mutual: 0,
    followersNew: 0,
    loaded: false,
    loading: false
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case COMPANY_PROFILE_GENERAL_REQUEST:
      return Immutable
        .fromJS(state)
        .setIn(['profile', 'isFetching'], true)
        .setIn(['profile', 'isFetched'], false)
        .setIn(['profile', 'hasError'], false)
        .setIn(['profile', 'fetchingError'], '')
        .toJS();

    case COMPANY_PROFILE_GENERAL_RESPONSE:
      return Immutable
        .fromJS(state)
        .setIn(['profile', 'isFetching'], false)
        .setIn(['profile', 'isFetched'], true)
        .setIn(['profile', 'hasError'], action.hasError)
        .setIn(['profile', 'data'], action.json)
        .toJS();

    case COMPANY_PROFILE_SET_BRAND_LOCAL:
      return Immutable
        .fromJS(state)
        .setIn(['profile', 'data', 'user', 'companyData', 'brand'], action.brand)
        .toJS();

    case COMPANY_PROFILE_SET_DESCRIPTION_LOCAL:
      return Immutable
        .fromJS(state)
        .setIn(['profile', 'data', 'user', 'description'], action.description)
        .toJS();

    case COMPANY_PROFILE_SET_AVATAR_LOCAL:
      return Immutable
        .fromJS(state)
        .setIn(['profile', 'data', 'user', 'avatar'], action.url)
        .toJS();


    case REQUEST_PROFILE_COUNTERS:
      return Immutable
        .fromJS(state)
        .update('counters', counters => {
          return counters
            .set('loaded', false)
            .set('loading', true);
        })
        .toJS();

    case RECEIVE_PROFILE_COUNTERS:
      return Immutable
        .fromJS(state)
        .update('counters', () => {
          return Immutable
            .fromJS(action.counters)
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();

    case ERROR_PROFILE_COUNTERS:
      return Immutable
        .fromJS(state)
        .update('counters', counters => {
          return counters
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();

    default:
      return state;
  }
}
