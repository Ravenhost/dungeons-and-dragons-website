import Immutable                      from 'immutable';
import {
  REQUEST_INCOMING_REQUEST_PARTNERS,
  RECEIVE_INCOMING_REQUEST_PARTNERS,
  ERROR_INCOMING_REQUEST_PARTNERS,
  REQUEST_ACCEPT_REQUEST_PARTNERS,
  RECEIVE_ACCEPT_REQUEST_PARTNERS,
  ERROR_ACCEPT_REQUEST_PARTNERS,
  REQUEST_REJECT_REQUEST_PARTNERS,
  RECEIVE_REJECT_REQUEST_PARTNERS,
  ERROR_REJECT_REQUEST_PARTNERS,
  REQUEST_OUTGOING_REQUEST_PARTNERS,
  RECEIVE_OUTGOING_REQUEST_PARTNERS,
  ERROR_OUTGOING_REQUEST_PARTNERS,
  REQUEST_DELETE_REQUEST_PARTNERS,
  RECEIVE_DELETE_REQUEST_PARTNERS,
  ERROR_DELETE_REQUEST_PARTNERS
}                                     from '../actions/partnersRequestActions';

const initialState = {
  incomingRequests: {
    requests:[],
    page: 0,
    search: '',
    isLast: false,
    loaded: false,
    loading: false
  },
  outgoingRequests:  {
    requests:[],
    page: 0,
    search: '',
    isLast: false,
    loaded: false,
    loading: false
  }
};


export default function (state = initialState, action) {
  switch (action.type) {
    case REQUEST_INCOMING_REQUEST_PARTNERS:
    case REQUEST_ACCEPT_REQUEST_PARTNERS:
    case REQUEST_REJECT_REQUEST_PARTNERS:
      return Immutable
        .fromJS(state)
        .update('incomingRequests', incoming => {
          return incoming
            .set('loaded', false)
            .set('loading', true);
        })
        .toJS();
    case REQUEST_OUTGOING_REQUEST_PARTNERS:
    case REQUEST_DELETE_REQUEST_PARTNERS:
      return Immutable
        .fromJS(state)
        .update('outgoingRequests', outgoing => {
          return outgoing
            .set('loaded', false)
            .set('loading', true);
        })
        .toJS();
    case RECEIVE_INCOMING_REQUEST_PARTNERS:
      return Immutable
        .fromJS(state)
        .update('incomingRequests', incoming => {
          return incoming
            .set('loaded', true)
            .set('loading', false)
            .set('page', action.page)
            .set('search', action.search)
            .set('isLast', action.fetchedRequest.length === 0)
            .update('requests', list => {
              if (action.page === 1) {
                return action.fetchedRequest;
              }
              return list.concat(action.fetchedRequest);
            });
        })
        .toJS();
    case RECEIVE_REJECT_REQUEST_PARTNERS:
    case RECEIVE_ACCEPT_REQUEST_PARTNERS:
      return Immutable
        .fromJS(state)
        .update('incomingRequests', incoming => {
          return incoming
            .set('loaded', true)
            .set('loading', false)
            .update('requests', list => {
              const deleteIndex = list.findIndex((value) => {
                return value.toJS()._id === action.partner._id;
              });

              return list.delete(deleteIndex);
            });
        })
        .toJS();
    case RECEIVE_OUTGOING_REQUEST_PARTNERS:
      return Immutable
        .fromJS(state)
        .update('outgoingRequests', outgoing => {
          return outgoing
            .set('loaded', true)
            .set('loading', false)
            .set('page', action.page)
            .set('search', action.search)
            .set('isLast', action.fetchedRequest.length === 0)
            .update('requests', list => {
              if (action.page === 1) {
                return action.fetchedRequest;
              }
              return list.concat(action.fetchedRequest);
            });
        })
        .toJS();
    case RECEIVE_DELETE_REQUEST_PARTNERS:
      return Immutable
        .fromJS(state)
        .update('outgoingRequests', outgoing => {
          return outgoing
            .set('loaded', true)
            .set('loading', false)
            .update('requests', list => {
              return list.filter((value) => {
                return value.toJS()._id !== action.partnerId;
              });
            });
        })
        .toJS();
    case ERROR_INCOMING_REQUEST_PARTNERS:
    case ERROR_ACCEPT_REQUEST_PARTNERS:
    case ERROR_REJECT_REQUEST_PARTNERS:
      return Immutable
        .fromJS(state)
        .update('incomingRequests', incoming => {
          return incoming
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();
    case ERROR_DELETE_REQUEST_PARTNERS:
    case ERROR_OUTGOING_REQUEST_PARTNERS:
      return Immutable
        .fromJS(state)
        .update('outgoingRequests', outgoing => {
          return outgoing
            .set('loaded', true)
            .set('loading', false);
        })
        .toJS();
    default:
      return state;
  }
}
