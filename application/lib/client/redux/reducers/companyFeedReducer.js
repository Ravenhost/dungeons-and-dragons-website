import Immutable                    from 'immutable';
import {
  COMPANY_FEED_REQUEST,
  COMPANY_FEED_RESPONSE,
  COMPANY_FEED_LIKE_POST_REQUEST,
  COMPANY_FEED_LIKE_POST_RESPONSE,
  COMPANY_FEED_LIKE_POST_END,
  COMPANY_FEED_CHANGE_AVATAR
}                                   from '../types';
import {
  RECEIVE_NEWS_AFTER_CHANGE_ACTION,
  DELETE_NEWS_FROM_NEWSFEED,
  RECEIVE_MY_NEWS,
  REQUEST_GET_NEWS_FULLTEXT,
  RECEIVE_GET_NEWS_FULLTEXT,
  ERROR_GET_NEWS_FULLTEXT
}                                   from '../actions/companyFeedActions';


const initialState = {
  myNewsLoaded:false,
  loading: false,
  loaded: false,
  myCompanyNews: [],
  anotherCompanyNews: [],
  data: [],
  likesInProgress: []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case COMPANY_FEED_REQUEST:
      return Immutable
        .fromJS(state)
        .setIn([ 'loaded' ], false)
        .setIn([ 'loading' ], true)
        .toJS();
    case COMPANY_FEED_RESPONSE:
      return Immutable
        .fromJS(state)
        .updateIn([ 'data' ], list => {
          return action.isFirstPage ? action.json : list.concat(action.json);
        })
        .setIn([ 'loaded' ], true)
        .setIn([ 'loading' ], false)
        .toJS();
    case RECEIVE_MY_NEWS:
      return Immutable
        .fromJS(state)
        .toJS();
    case RECEIVE_NEWS_AFTER_CHANGE_ACTION:
      return Immutable
        .fromJS(state)
        .toJS();
    case DELETE_NEWS_FROM_NEWSFEED:
      return Immutable
        .fromJS(state)
        .updateIn([ 'data' ], list => {
          return list.toJS().filter((news) => {
            return news._id !== action.id;
          });
        })
        .toJS();

    case REQUEST_GET_NEWS_FULLTEXT:
      return Immutable
        .fromJS(state)
        .toJS();
    case RECEIVE_GET_NEWS_FULLTEXT:
      return Immutable
        .fromJS(state)
        .updateIn([ 'data' ], list => {
          return list.toJS().map((news) => {
            if (news.sourceId) {
              return news.sourceId._id === action.id ?
                Immutable
                  .fromJS(news)
                  .setIn(['sourceId', 'description'], action.text)
                  .setIn(['sourceId', 'isFull'], true)
                  .toJS() : news;
            }
            return news._id === action.id ? Immutable
                .fromJS(news)
                .setIn([ 'description' ], action.text)
                .setIn([ 'isFull' ], true)
                .toJS() : news;
          });
        })
        .toJS();
    case ERROR_GET_NEWS_FULLTEXT:
      return Immutable
        .fromJS(state)
        .toJS();

    case COMPANY_FEED_LIKE_POST_REQUEST:
      return Immutable
        .fromJS(state)
        .updateIn([ 'likesInProgress' ], list => {
          return list.push(action.id);
        })
        .toJS();

    case COMPANY_FEED_LIKE_POST_END:
      return Immutable
        .fromJS(state)
        .updateIn([ 'likesInProgress' ], list => {
          return list.remove(list.indexOf(action.id));
        })
        .toJS();

    /* eslint-disable no-case-declarations */
    case COMPANY_FEED_LIKE_POST_RESPONSE:
      const index = Immutable.fromJS(state).get('data').findIndex(listItem => {
        return listItem.get('_id') === action.id;
      });

      return Immutable
        .fromJS(state)
        .setIn(['data', index, 'likesCount'], action.json.likesCount)
        .setIn(['data', index, 'likes'], action.json.likes)
        .updateIn([ 'likesInProgress' ], list => {
          return list.remove(list.indexOf(action.id));
        })
        .toJS();
    /* eslint-enable no-case-declarations */
    case COMPANY_FEED_CHANGE_AVATAR:
      return Immutable
        .fromJS(state)
        .updateIn([ 'data' ], (obj) => {
          return obj.map(item => {
            return item.setIn(['owner', 'avatar'], action.url);
          });
        })
        .toJS();
    default:
      return state;
  }
}
