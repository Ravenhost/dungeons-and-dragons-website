import { combineReducers }                  from 'redux';
import { reducer as reduxAsyncConnect }     from 'redux-connect';
import newsReducer                          from './newsReducer';
import signUpReducer                        from './signUpReducer';
import signInReducer                        from './signInReducer';
import companyProfileReducer                from './companyProfileReducer';
import companyFeedReducer                   from './companyFeedReducer';
import modalReducer                         from './modalReducer';
import partnersReducer                      from './partnersReducer';
import partnersRequestReducer               from './partnersRequestReducer';
import followReducer                        from './followReducer';

export default combineReducers({
  reduxAsyncConnect,
  news: newsReducer,
  signUp: signUpReducer,
  signIn: signInReducer,
  companyProfile: companyProfileReducer,
  companyFeed: companyFeedReducer,
  partners: partnersReducer,
  partnersRequest: partnersRequestReducer,
  follows: followReducer,
  modal: modalReducer
});
