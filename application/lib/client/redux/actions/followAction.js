import fetch              from 'isomorphic-fetch';
import pathAPI            from '../pathAPI';
import fetchDecorator     from './common/fetchDecorator';
import {
  getToken
}                         from '../../redux/utils/helpers';
import decoratorAuthCheck from './common/protectedAuth';


export const REQUEST_USER_SUBSCRIPTIONS = 'REQUEST_USER_SUBSCRIPTIONS';
export const RECEIVE_USER_SUBSCRIPTIONS = 'RECEIVE_USER_SUBSCRIPTIONS';
export const ERROR_USER_SUBSCRIPTIONS   = 'ERROR_USER_SUBSCRIPTIONS';

function requestUserSubscriptions() {
  return {
    type: REQUEST_USER_SUBSCRIPTIONS
  };
}

function errorUserSubscriptions(error) {
  return {
    type: ERROR_USER_SUBSCRIPTIONS,
    error
  };
}

function receiveUserSubscriptions(subscriptions, page, search) {
  return {
    type: RECEIVE_USER_SUBSCRIPTIONS,
    subscriptions,
    page,
    search
  };
}

export function fetchUserSubscriptions(params) {
  return (dispatch, getState) => {
    if (shouldFetchUserSubscriptions(getState(), params)) {
      return dispatch(fetchUserSubscriptionsDo(getState(), params));
    }
  };
}

function shouldFetchUserSubscriptions(state, params) {
  if (state.follows.subscriptions.loading) {
    return false;
  }
  const subscriptions = state.follows.subscriptions;

  if (subscriptions.search !== params.search) {
    return true;
  }
  return params.page > subscriptions.page && !subscriptions.isLast;
}

function fetchUserSubscriptionsDo(state, params) {
  return dispatch => {
    const page = params.page;
    const perPage = params.perPage;
    const search = params.search || '';

    dispatch(requestUserSubscriptions());
    console.log('Fetch: User Subscriptions');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/subscribe?page=${page}&perPage=${perPage}&search=${search}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      })
        .then(response => {
          const json = response.json();

          if (response.status !== 200) {
            return Promise.reject(response.statusText);
          }
          return json;
        })
        .then(json => dispatch(receiveUserSubscriptions(json, page, search)))
        .catch(err => {
          dispatch(errorUserSubscriptions(err));
        })
    );
  };
}

export const REQUEST_DELETE_SUBSCRIPTIONS = 'REQUEST_DELETE_SUBSCRIPTIONS';
export const RECEIVE_DELETE_SUBSCRIPTIONS = 'RECEIVE_DELETE_SUBSCRIPTIONS';
export const ERROR_DELETE_SUBSCRIPTIONS   = 'ERROR_DELETE_SUBSCRIPTIONS';

function requestDeleteSubscriptions() {
  return {
    type: REQUEST_DELETE_SUBSCRIPTIONS
  };
}

function errorDeleteSubscriptions(error) {
  return {
    type: ERROR_DELETE_SUBSCRIPTIONS,
    error
  };
}

function receiveDeleteSubscriptions(json, subscriptionId) {
  return {
    type: RECEIVE_DELETE_SUBSCRIPTIONS,
    json,
    subscriptionId
  };
}

export function fetchDeleteSubscriptions(subscriptionId) {
  return (dispatch, getState) => {
    if (shouldFetchDeleteSubscriptions(getState())) {
      return dispatch(fetchDeleteSubscriptionsDo(getState(), subscriptionId));
    }
  };
}

function shouldFetchDeleteSubscriptions(state) {
  return !state.follows.subscriptions.loading;
}

function fetchDeleteSubscriptionsDo(state, subscriptionId) {
  return dispatch => {
    console.log('Fetch: Delete Subscriptions');
    dispatch(requestDeleteSubscriptions());
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/subscribe/${subscriptionId}`, {
        method: 'DELETE',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveDeleteSubscriptions(json, subscriptionId)))
      .catch(err => {
        dispatch(errorDeleteSubscriptions(err));
      });
  };
}


export const REQUEST_USER_FOLLOWERS = 'REQUEST_USER_FOLLOWERS';
export const RECEIVE_USER_FOLLOWERS = 'RECEIVE_USER_FOLLOWERS';
export const ERROR_USER_FOLLOWERS   = 'ERROR_USER_FOLLOWERS';

function requestUserFollowers() {
  return {
    type: REQUEST_USER_FOLLOWERS
  };
}

function errorUserFollowers(error) {
  return {
    type: ERROR_USER_FOLLOWERS,
    error
  };
}

function receiveUserFollowers(followers, page, search) {
  return {
    type: RECEIVE_USER_FOLLOWERS,
    followers,
    page,
    search
  };
}

export function fetchUserFollowers(params) {
  return (dispatch, getState) => {
    if (shouldFetchUserFollowers(getState(), params)) {
      return dispatch(fetchUserFollowersDo(getState(), params));
    }
  };
}

function shouldFetchUserFollowers(state, params) {
  if (state.follows.followers.loading) {
    return false;
  }
  const followers = state.follows.followers;

  if (followers.search !== params.search) {
    return true;
  }
  return params.page > followers.page && !followers.isLast;
}

function fetchUserFollowersDo(state, params) {
  return dispatch => {
    const page = params.page;
    const perPage = params.perPage;
    const search = params.search || '';

    dispatch(requestUserFollowers());
    console.log('Fetch: User Followers');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/subscribe/followers?page=${page}&perPage=${perPage}&search=${search}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      })
        .then(response => {
          const json = response.json();

          if (response.status !== 200) {
            return Promise.reject(response.statusText);
          }
          return json;
        })
        .then(json => dispatch(receiveUserFollowers(json, page, search)))
        .catch(err => {
          dispatch(errorUserFollowers(err));
        })
    );
  };
}
