import fetch from 'isomorphic-fetch';


import {
  CHECK_BANK_DETAILS_REQUEST,
  CHECK_BANK_DETAILS_RESPONSE,
  SIGN_UP_REQUEST,
  SIGN_UP_RESPONSE
} from '../types';

import * as Constants from '../../config/constants';
import pathAPI from '../pathAPI';

function checkBankDetailsRequest(userType, email, password, ogrn) {
  return {
    type: CHECK_BANK_DETAILS_REQUEST,
    userType,
    email,
    password,
    ogrn
  };
}
function checkBankDetailsResponse(json) {
  return {
    type: CHECK_BANK_DETAILS_RESPONSE,
    json
  };
}

function shouldCheckBankDetails(state) {
  if (state.signUp.bankDetails.isFetching) {
    return false;
  }

  return true;
}

function checkBankDetailsDo(userType, email, password, ogrn) {
  return dispatch => {
    dispatch(checkBankDetailsRequest(userType, email, password, ogrn));
    const data =  { type: getApiUserType(userType), email, password, ogrn };

    return fetch(`${pathAPI}/auth/check`, {
      method: 'POST',
      headers: new Headers({ 'content-type': 'application/json' }),
      body: JSON.stringify(data),
      credentials: 'same-origin'
    })
      .then(response => response.json())
      .then(json => dispatch(checkBankDetailsResponse(json)));
  };
}

export function checkBankDetails(userType, email, password, ogrn) {
  return (dispatch, getState) => {
    if (shouldCheckBankDetails(getState())) {
      return dispatch(checkBankDetailsDo(userType, email, password, ogrn));
    }
  };
}

//---------------------------------------------------------------------------

export function signUp(data) {
  return (dispatch) => {
    return dispatch(signUpDo(data));
  };
}

function signUpDo(data) {
  return dispatch => {
    dispatch(signUpRequest(data));

    let apiIn = {};

    if (data.userType === Constants.USER_TYPE_PERSON) {
      apiIn = {
        type: getApiUserType(data.userType),
        email: data.email,
        password: data.password,
        firstName: data.firstName,
        middleName: data.middleName,
        lastName: data.lastName
      };
    } else {
      apiIn = {
        type: getApiUserType(data.userType),
        email: data.email,
        password: data.password,
        ogrn: data.ogrn,
        brand: data.brand
      };
    }

    return fetch(`${pathAPI}/auth/signup`, {
      method: 'POST',
      headers: new Headers({ 'content-type': 'application/json' }),
      body: JSON.stringify(apiIn),
      credentials: 'same-origin'
    })
      .then(response => response.json())
      .then(json => dispatch(signUpResponse(json)));
  };
}

function signUpRequest(data) {
  return {
    type: SIGN_UP_REQUEST,
    userType: data.userType,
    email: data.email,
    password: data.password,
    ogrn: data.ogrn
  };
}

function signUpResponse(json) {
  return {
    type: SIGN_UP_RESPONSE,
    json
  };
}

function getApiUserType(userType) {
  switch (userType) {
    case 'company':
      return 0;
    case 'entrepreneur':
      return 1;
    case 'person':
      return 2;
    default:
      return 0;
  }
}
