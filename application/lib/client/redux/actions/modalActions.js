export const HIDE_MAIN_MODAL = 'HIDE_MAIN_MODAL';
export const SHOW_MAIN_MODAL = 'SHOW_MAIN_MODAL';

function showMainModalDo(modalProps, modalType) {
  return {
    type: SHOW_MAIN_MODAL,
    modalProps,
    modalType
  };
}

function hideMainModalDo() {
  return {
    type: HIDE_MAIN_MODAL

  };
}

export function showMainModal(modalProps, modalType) {
  return (dispatch) => {
    dispatch(showMainModalDo(modalProps, modalType));
  };
}

export function hideMainModal() {
  return (dispatch) => {
    dispatch(hideMainModalDo());
  };
}

export const NONE_MODAL_TYPE = 'NONE_MODAL_TYPE';
export const DELETE_NEWS_MODAL_TYPE = 'DELETE_NEWS_MODAL_TYPE';
export const REPOST_NEWS_MODAL_TYPE = 'REPOST_NEWS_MODAL_TYPE';
export const DETAIL_NEWS_MODAL_TYPE = 'DETAIL_NEWS_MODAL_TYPE';
