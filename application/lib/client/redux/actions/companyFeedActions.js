// import config from '../../../config';
import fetch                   from 'isomorphic-fetch';
import pathAPI                 from '../pathAPI';
import { getToken, getUserId } from '../../redux/utils/helpers';
import decoratorAuthCheck      from './common/protectedAuth';
import fetchDecorator          from './common/fetchDecorator';

import {
  COMPANY_FEED_REQUEST,
  COMPANY_FEED_RESPONSE,
  COMPANY_FEED_LIKE_POST_REQUEST,
  COMPANY_FEED_LIKE_POST_RESPONSE,
  COMPANY_FEED_LIKE_POST_END,
  COMPANY_FEED_CHANGE_AVATAR
} from '../types';

function requestFeed() {
  return {
    type: COMPANY_FEED_REQUEST
  };
}

function receiveFeed(json, isFirstPage) {
  return {
    type: COMPANY_FEED_RESPONSE,
    json,
    isFirstPage
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchFeed(state, userId) {
  const feed = state.companyFeed;

  // console.log(!feed.loading && (getUserId() !== userId && !state.companyFeed.myNewsLoaded));
  return !feed.loading;
}
/* eslint-enable no-unused-vars */

function fetchFeedDo(state, fetchInfo) {
  return dispatch => {
    console.log('fetchFeedDo');
    let date = '';
    const userId = fetchInfo.userId;
    const perPage = fetchInfo.perPage;
    const search = fetchInfo.search;

    // console.log('search ', search);

    if (!fetchInfo.isFirst) {
      const currentFeed = state.companyFeed.data;
      const lastNews = currentFeed[currentFeed.length - 1];

      date = (typeof lastNews === 'undefined') ? '' : lastNews.datePublished;
    }


    dispatch(requestFeed());
    return fetch(`${pathAPI}/social-news/user/${userId}?datePublished=${date}&perPage=${perPage}&search=${search}`, {
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(json => dispatch(receiveFeed(json, date.length === 0)))
      .catch(err => {
        console.log(err);
      });
  };
}

export function fetchCompanyFeed(isFirst, perPage, userId, search = '') {
  // console.log(search);
  return (dispatch, getState) => {
    if (shouldFetchFeed(getState(), userId)) {
      const fetchInfo = {
        perPage,
        userId,
        isFirst,
        search
      };

      return dispatch(fetchFeedDo(getState(), fetchInfo));
    }
  };
}

/* Delete New from NewsFeed */

export const DELETE_NEWS_FROM_NEWSFEED = 'DELETE_NEWS_FROM_NEWSFEED';

export function deleteNewsFromNewsFeed(id) {
  return {
    type: DELETE_NEWS_FROM_NEWSFEED,
    id
  };
}

/* Update Feed then create, update news/repost */

export const RECEIVE_NEWS_AFTER_CHANGE_ACTION = 'RECEIVE_NEWS_AFTER_CHANGE_ACTION';

export function updateFeedList(news) {
  return {
    type: RECEIVE_NEWS_AFTER_CHANGE_ACTION,
    news
  };
}

/* Check async request my News */

export const RECEIVE_MY_NEWS = 'RECEIVE_CHECK_IS_MY_NEWS';

function shouldCheckMyNews(state) {
  const feed = state.companyFeed;

  return !feed.myNewsLoaded;
}

function receiveCheckIsMyNews(result) {
  return {
    type: RECEIVE_MY_NEWS,
    result
  };
}

function checkIsMyNewsDo(id) {
  console.log('checkIsMyNewsDo');
  return dispatch => {
    dispatch(receiveCheckIsMyNews(getUserId() === id));
  };
}

export function checkIsMyNews(id) {
  return (dispatch, getState) => {
    if (shouldCheckMyNews(getState())) {
      return dispatch(checkIsMyNewsDo(id));
    }
  };
}
// ------------------------------------------------------------------------
export function likePost(id) {
  return (dispatch, getState) => {
    if (shouldLikePost(getState(), id)) {
      return dispatch(likePostDo(getState(), id));
    }
  };
}

function shouldLikePost(state, id) {
  const likesInProgress = state.companyFeed.likesInProgress;

  return !(likesInProgress && likesInProgress.includes(id));
}

function likePostDo(state, id) {
  return dispatch => {
    let status = 0;

    dispatch(setLikeInProgress(id));

    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/social-news/${id}/like`, {
        method: 'PUT',
        headers: new Headers({ 'content-type': 'application/json', 'Authorization': getToken() }),
        credentials: 'same-origin'
      }))
      .then(response => {
        status = response.status;
        return response.json();
      })
      .then(json => {
        if (status === 200) {
          dispatch(endLikingProcess(id));
          dispatch(successLike(id, json));
        } else {
          dispatch(endLikingProcess(id, json));
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

function setLikeInProgress(id) {
  return {
    type: COMPANY_FEED_LIKE_POST_REQUEST,
    id
  };
}

function successLike(id, json) {
  return {
    type: COMPANY_FEED_LIKE_POST_RESPONSE,
    id,
    json
  };
}

function endLikingProcess(id) {
  return {
    type: COMPANY_FEED_LIKE_POST_END,
    id
  };
}
// ------------------------------------------------------------------------
export function changeAvatarInFeed(url) {
  return {
    type: COMPANY_FEED_CHANGE_AVATAR,
    url
  };
}

/* Get news fulltext */

export const REQUEST_GET_NEWS_FULLTEXT = 'REQUEST_GET_NEWS_FULLTEXT';
export const RECEIVE_GET_NEWS_FULLTEXT = 'RECEIVE_GET_NEWS_FULLTEXT';
export const ERROR_GET_NEWS_FULLTEXT   = 'ERROR_GET_NEWS_FULLTEXT';


function requestGetNewsFullText() {
  return {
    type: REQUEST_GET_NEWS_FULLTEXT
  };
}

function errorGetNewsFullText(error) {
  return {
    type: ERROR_GET_NEWS_FULLTEXT,
    error
  };
}

function receiveGetNewsFullText(json, id) {
  return {
    type: RECEIVE_GET_NEWS_FULLTEXT,
    text: json.text,
    id
  };
}

export function fetchGetNewsFullText(id) {
  return (dispatch, getState) => {
    if (shouldFetchGetNewsFullText(getState())) {
      return dispatch(fetchGetNewsFullTextDo(getState(), id));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchGetNewsFullText(state) {
  return true;
}

function fetchGetNewsFullTextDo(state, id) {
  return dispatch => {
    dispatch(requestGetNewsFullText());
    console.log('Fetch: Get FullText News');
    return fetch(`${pathAPI}/social-news/${id}/text`, {
      method: 'get',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveGetNewsFullText(json, id)))
      .catch(err => {
        dispatch(errorGetNewsFullText(err));
      });
  };
}
