import fetch              from 'isomorphic-fetch';
import pathAPI            from '../pathAPI';
import fetchDecorator     from './common/fetchDecorator';
import {
  getToken,
  getUserId
}                         from '../../redux/utils/helpers';
import decoratorAuthCheck from './common/protectedAuth';


export const REQUEST_USER_PARTNERS = 'REQUEST_USER_PARTNERS';
export const RECEIVE_USER_PARTNERS = 'RECEIVE_USER_PARTNERS';
export const ERROR_USER_PARTNERS   = 'ERROR_USER_PARTNERS';

function requestUserPartners() {
  return {
    type: REQUEST_USER_PARTNERS
  };
}

function errorUserPartners(error) {
  return {
    type: ERROR_USER_PARTNERS,
    error
  };
}

function receiveUserPartners(partners, page, userId, search) {
  return {
    type: RECEIVE_USER_PARTNERS,
    partners,
    page,
    userId,
    search
  };
}

export function fetchUserPartners(params) {
  return (dispatch, getState) => {
    if (shouldFetchUserPartners(getState(), params.userId, params.page, params.search)) {
      return dispatch(fetchUserPartnersDo(getState(), params));
    }
  };
}

function shouldFetchUserPartners(state, userId, page, search) {
  if (state.partners.loading) {
    return false;
  }

  if (state.partners.usersPartners.hasOwnProperty(userId)) {
    const partners = state.partners.usersPartners[userId];

    if (partners.search !== search) {
      return true;
    }
    return page > partners.page && !partners.isLast;
  }
  return true;
}

function fetchUserPartnersDo(state, params) {
  return dispatch => {
    const userId = params.userId;
    const page = params.page;
    const perPage = params.perPage;
    const search = params.search || '';

    dispatch(requestUserPartners());
    console.log('Fetch: User Partners');
    return fetch(`${pathAPI}/partners/${userId}?page=${page}&perPage=${perPage}&search=${search}`, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }

    })
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveUserPartners(json, page, userId, search)))
      .catch(err => {
        dispatch(errorUserPartners(err));
      });
  };
}

export const REQUEST_DELETE_PARTNERS = 'REQUEST_DELETE_PARTNERS';
export const RECEIVE_DELETE_PARTNERS = 'RECEIVE_DELETE_PARTNERS';
export const ERROR_DELETE_PARTNERS   = 'ERROR_DELETE_PARTNERS';

function requestDeletePartners() {
  return {
    type: REQUEST_DELETE_PARTNERS
  };
}

function errorDeletePartners(error) {
  return {
    type: ERROR_DELETE_PARTNERS,
    error
  };
}

function receiveDeletePartners(json, userId, partnerId) {
  return {
    type: RECEIVE_DELETE_PARTNERS,
    json,
    userId,
    partnerId
  };
}

export function fetchDeletePartners(partnerId) {
  return (dispatch, getState) => {
    const userId = getUserId();

    if (shouldFetchDeletePartners(getState(), userId)) {
      return dispatch(fetchDeletePartnersDo(getState(), partnerId, userId));
    }
  };
}

function shouldFetchDeletePartners(state, userId) {
  return !state.partners.loading && userId !== '';
}

function fetchDeletePartnersDo(state, partnerId, userId) {
  return dispatch => {
    console.log('Fetch: Delete Partners');
    dispatch(requestDeletePartners());
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/partners/${partnerId}`, {
        method: 'DELETE',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveDeletePartners(json, userId, partnerId)))
      .catch(err => {
        dispatch(errorDeletePartners(err));
      });
  };
}


export const REQUEST_MUTUAL_PARTNERS = 'REQUEST_MUTUAL_PARTNERS';
export const RECEIVE_MUTUAL_PARTNERS = 'RECEIVE_MUTUAL_PARTNERS';
export const ERROR_MUTUAL_PARTNERS   = 'ERROR_MUTUAL_PARTNERS';

function requestMutualPartners() {
  return {
    type: REQUEST_MUTUAL_PARTNERS
  };
}

function errorMutualPartners(error) {
  return {
    type: ERROR_MUTUAL_PARTNERS,
    error
  };
}

function receiveMutualPartners(partners, partnerId, page, search) {
  return {
    type: RECEIVE_MUTUAL_PARTNERS,
    partners,
    partnerId,
    page,
    search
  };
}

export function fetchMutualPartners(params) {
  return (dispatch, getState) => {
    if (shouldFetchMutualPartners(getState(), params)) {
      return dispatch(fetchMutualPartnersDo(getState(), params));
    }
  };
}

function shouldFetchMutualPartners(state, params) {
  if (state.partners.loading) {
    return false;
  }

  if (state.partners.mutualPartners.hasOwnProperty(params.partnerId)) {
    const mutualPartners = state.partners.mutualPartners[params.partnerId];

    if (mutualPartners.search !== params.search) {
      return true;
    }
    return params.page > mutualPartners.page && !mutualPartners.isLast;
  }
  return true;
}

function fetchMutualPartnersDo(state, params) {
  return dispatch => {
    const partnerId = params.partnerId;
    const page = params.page;
    const perPage = params.perPage;
    const search = params.search || '';

    console.log('Fetch: Mutual Partners');
    dispatch(requestMutualPartners());
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/partners/mutual/${partnerId}?page=${page}&perPage=${perPage}&search=${search}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveMutualPartners(json, partnerId, page, search)))
      .catch(err => {
        dispatch(errorMutualPartners(err));
      });
  };
}

export const REQUEST_ADD_PARTNERS = 'REQUEST_ADD_PARTNERS';
export const RECEIVE_ADD_PARTNERS = 'RECEIVE_ADD_PARTNERS';
export const ERROR_ADD_PARTNERS   = 'ERROR_ADD_PARTNERS';

function requestAddPartners() {
  return {
    type: REQUEST_ADD_PARTNERS
  };
}

function errorAddPartners(error) {
  return {
    type: ERROR_ADD_PARTNERS,
    error
  };
}

function receiveAddPartners(json, partnerId) {
  return {
    type: RECEIVE_ADD_PARTNERS,
    json,
    partnerId
  };
}

export function fetchAddPartners(partnerId) {
  return (dispatch, getState) => {
    if (shouldFetchAddPartners(getState(), partnerId)) {
      return dispatch(fetchAddPartnersDo(getState(), partnerId));
    }
  };
}

function shouldFetchAddPartners(state, partnerId) {
  return !state.partners.loading && partnerId !== '';
}

function fetchAddPartnersDo(state, partnerId) {
  return dispatch => {
    console.log('Fetch: Add Partners');
    dispatch(requestAddPartners());
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/requests/${partnerId}`, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveAddPartners(json, partnerId)))
      .catch(err => {
        dispatch(errorAddPartners(err));
      });
  };
}
