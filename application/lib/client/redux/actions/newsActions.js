import fetch              from 'isomorphic-fetch';
import pathAPI            from '../pathAPI';
import { browserHistory } from 'react-router';
import getUserId          from '../utils/helpers/getUserId';
import {
  updateFeedList,
  deleteNewsFromNewsFeed
}                         from './companyFeedActions';
import {
  showMainModal,
  REPOST_NEWS_MODAL_TYPE
}                         from '../actions/modalActions';

import {
  REQUEST_NEWS,
  RECEIVE_NEWS
}                         from '../types';

import decoratorAuthCheck from './common/protectedAuth';
import fetchDecorator     from './common/fetchDecorator';
import { getToken }       from '../../redux/utils/helpers';

function requestNews(category) {
  return {
    type: REQUEST_NEWS,
    category
  };
}

function receiveNews(json, category, page) {
  console.log('Recieve News ', category);
  return {
    type: RECEIVE_NEWS,
    news: json.news,
    category,
    page
  };
}

function shouldFetchNews(state, category, page) {
  const news = state.news;

  console.log('shouldFetchNews', news[category]);

  // TODO: return !(news[category].loading || news[category].page >= page);

  if (news[category].loading ||
      news[category].page >= page
  ) {
    return false;
  }

  return true;
}

function fetchNewsDo(state, category, page, perPage) {
  return dispatch => {
    dispatch(requestNews(category));
    console.log('pathAPI:', pathAPI);
    return fetch(`${pathAPI}/news?category=${category}&page=${page}&perPage=${perPage}`, {
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then(json => dispatch(receiveNews(json, category, page)))
      .catch(err => {
        console.error(err);
      });
  };
}

export function fetchNews(category, page, perPage) {
  return (dispatch, getState) => {
    if (shouldFetchNews(getState(), category, page)) {
      return dispatch(fetchNewsDo(getState(), category, page, perPage));
    }
  };
}

/* News GET Edited */

export const REQUEST_GET_EDITED_NEWS = 'REQUEST_GET_EDITED_NEWS';
export const RECEIVE_GET_EDITED_NEWS = 'RECEIVE_GET_EDITED_NEWS';
export const ERROR_GET_EDITED_NEWS   = 'ERROR_GET_EDITED_NEWS';


function requestGetEditedNews() {
  return {
    type: REQUEST_GET_EDITED_NEWS
  };
}

function errorGetEditedNews(error) {
  return {
    type: ERROR_GET_EDITED_NEWS,
    error
  };
}

function receiveGetEditedNews(news) {
  return {
    type: RECEIVE_GET_EDITED_NEWS,
    news
  };
}

export function fetchGetEditedNews(id) {
  return (dispatch, getState) => {
    if (shouldFetchGetEditedNews(getState())) {
      return dispatch(fetchGetEditedNewsDo(getState(), id));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchGetEditedNews(state) {
  return !state.news.singleNews.loading;
}

function fetchGetEditedNewsDo(state, id) {
  return dispatch => {
    dispatch(requestGetEditedNews());
    console.log('Fetch: Get Edited News');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/social-news/${id}/edited`, {
        method: 'get',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `${getToken()}`
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveGetEditedNews(json)))
      .catch(err => {
        dispatch(errorGetEditedNews(err));
      });
  };
}


/* /News Create */

export const REQUEST_CREATE_NEWS = 'REQUEST_CREATE_NEWS';
export const RECEIVE_CREATE_NEWS = 'RECEIVE_CREATE_NEWS';
export const ERROR_CREATE_NEWS   = 'ERROR_CREATE_NEWS';

function requestCreateNews() {
  return {
    type: REQUEST_CREATE_NEWS
  };
}

function errorCreateNews(error) {
  return {
    type: ERROR_CREATE_NEWS,
    error
  };
}

function receiveCreateNews(id) {
  browserHistory.push(`/news-edit/${id}`);
  return {
    type: RECEIVE_CREATE_NEWS,
    id
  };
}

export function fetchCreateNews() {
  return (dispatch, getState) => {
    if (shouldFetchCreateNews(getState())) {
      return dispatch(fetchCreateNewsDo(getState()));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchCreateNews(state) {
  return !state.news.singleNews.loading;
}

/* eslint-disable no-unused-vars */
function fetchCreateNewsDo(state, sourceId = null) {
  return dispatch => {
    if (sourceId === null) {
      dispatch(requestCreateNews());
      console.log('Fetch: Create News');
    } else {
      dispatch(requestRepostNews());
      console.log('Fetch: Repost News');
    }

    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/social-news/`, {
        method: 'post',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `${getToken()}`
        },
        body: JSON.stringify({
          sourceId
        })

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json =>
        sourceId === null ? dispatch(receiveCreateNews(json)) : dispatch(receiveRepostNews(json))
       )
      .catch(err => {
        if (sourceId === null) {
          dispatch(errorCreateNews(err));
        } else {
          dispatch(errorRepostNews(err));
        }
      });
  };
}

/* /News Repost */


export const REQUEST_REPOST_NEWS = 'REQUEST_REPOST_NEWS';
export const RECEIVE_REPOST_NEWS = 'RECEIVE_REPOST_NEWS';
export const ERROR_REPOST_NEWS   = 'ERROR_REPOST_NEWS';

function requestRepostNews() {
  return {
    type: REQUEST_REPOST_NEWS
  };
}

function errorRepostNews(error) {
  return {
    type: ERROR_REPOST_NEWS,
    error
  };
}

function receiveRepostNews(id) {
  return {
    type: RECEIVE_REPOST_NEWS,
    id
  };
}

export function fetchRepostNews(sourceId, isFromModal = false) {
  return (dispatch, getState) => {
    if (shouldFetchRepostNews(getState())) {
      return dispatch(fetchCreateNewsDo(getState(), sourceId))
        .then(json => dispatch(showMainModal({ sourceId, isFromModal }, REPOST_NEWS_MODAL_TYPE)));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchRepostNews(state) {
  return !state.news.singleNews.loading;
}


/* News change */

export const REQUEST_CHANGE_NEWS = 'REQUEST_CHANGE_NEWS';
export const RECEIVE_CHANGE_NEWS = 'RECEIVE_CHANGE_NEWS';
export const ERROR_CHANGE_NEWS   = 'ERROR_CHANGE_NEWS';


function requestChangeNews() {
  return {
    type: REQUEST_CHANGE_NEWS
  };
}

function errorChangeNews(error) {
  return {
    type: ERROR_CHANGE_NEWS,
    error
  };
}

function receiveChangeNews(news) {
  browserHistory.push(`/id${getUserId()}`);
  return {
    type: RECEIVE_CHANGE_NEWS,
    news
  };
}

export function fetchChangeNews(news) {
  return (dispatch, getState) => {
    if (shouldFetchChangeNews(getState())) {
      return dispatch(fetchChangeNewsDo(getState(), news));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchChangeNews(state) {
  return !state.news.singleNews.loading;
}

function fetchChangeNewsDo(state, news) {
  return dispatch => {
    const token = localStorage.getItem('token');


    dispatch(requestChangeNews());
    console.log('Fetch: Change News');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/social-news/${news.id}`, {
        method: 'put',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `${getToken()}`
        },
        body: JSON.stringify({
          title: news.title,
          text: news.text,
          images: news.images,
          files: news.files,
          hashtag: news.hashtag,
          inMaking: news.inMaking
        })
      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(updateFeedList(json)))
      .then(obj => dispatch(receiveChangeNews(obj)))
      .catch(err => {
        dispatch(errorChangeNews(err));
      });
  };
}

/* News add attachment/image to news */

export const REQUEST_DELETE_ATTACHMENT_NEWS = 'REQUEST_DELETE_ATTACHMENT_NEWS';
export const RECEIVE_DELETE_ATTACHMENT_NEWS = 'RECEIVE_DELETE_ATTACHMENT_NEWS';
export const ERROR_DELETE_ATTACHMENT_NEWS   = 'ERROR_DELETE_ATTACHMENT_NEWS';


function requestDeleteAttachmentNews() {
  return {
    type: REQUEST_DELETE_ATTACHMENT_NEWS
  };
}

function errorDeleteAttachmentNews(error) {
  return {
    type: ERROR_DELETE_ATTACHMENT_NEWS,
    error
  };
}

function receiveDeleteAttachmentNews(type, name) {
  return {
    type: RECEIVE_DELETE_ATTACHMENT_NEWS,
    typeDelete: type,
    name
  };
}

export function fetchDeleteAttachmentNews(type, name) {
  return (dispatch, getState) => {
    if (shouldFetchDeleteAttachmentNews(getState())) {
      return dispatch(fetchDeleteAttachmentNewsDo(getState(), type, name));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchDeleteAttachmentNews(state) {
  return !state.news.singleNews.loading;
}

function fetchDeleteAttachmentNewsDo(state, type, name) {
  return dispatch => {
    const token = localStorage.getItem('token');

    console.log('Fetch: Delete Attachment News');
    dispatch(requestDeleteAttachmentNews());
    dispatch(receiveDeleteAttachmentNews(type, name));
    // return fetch(`${pathAPI}/social-news/attachment`, {
    //   method: 'delete',
    //   credentials: 'same-origin',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json',
    //     'Authorization': `${token}`
    //   },
    //   body: JSON.stringify({
    //     type,
    //     name
    //   })
    // })
    //   .then(response => response.json())
    //   .then(json => dispatch(receiveDeleteAttachmentNews(json)))
    //   .catch(err => {
    //     dispatch(errorDeleteAttachmentNews(err));
    //   });
  };
}


/* News add news attachment/image */

export const REQUEST_ADD_ATTACHMENT_NEWS = 'REQUEST_ADD_ATTACHMENT_NEWS';
export const RECEIVE_ADD_ATTACHMENT_NEWS = 'RECEIVE_ADD_ATTACHMENT_NEWS';
export const ERROR_ADD_ATTACHMENT_NEWS   = 'ERROR_ADD_ATTACHMENT_NEWS';


function requestAddAttachmentNews() {
  return {
    type: REQUEST_ADD_ATTACHMENT_NEWS
  };
}

function errorAddAttachmentNews(error) {
  return {
    type: ERROR_ADD_ATTACHMENT_NEWS,
    error
  };
}

function receiveAddAttachmentNews(json) {
  return {
    type: RECEIVE_ADD_ATTACHMENT_NEWS,
    files: json.files,
    images: json.images,
    errors: json.errors || null
  };
}

export function fetchAddAttachmentNews(id, images, attachments) {
  return (dispatch, getState) => {
    if (shouldFetchAddAttachmentNews(getState())) {
      return dispatch(fetchAddAttachmentNewsDo(getState(), id, images, attachments));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchAddAttachmentNews(state) {
  return !state.news.singleNews.loading;
}

function fetchAddAttachmentNewsDo(state, id, images, attachments) {
  return dispatch => {
    const data = new FormData();

    for (let i = 0; i < images.length; i++) {
      data.append('images', images[i]);
    }
    for (let i = 0; i < attachments.length; i++) {
      data.append('attachments', attachments[i]);
    }

    dispatch(requestAddAttachmentNews());
    console.log('Fetch: Add Attachment News');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/social-news/${id}/files`, {
        method: 'put',
        credentials: 'same-origin',
        headers: {
          'Authorization': `${getToken()}`
        },
        body: data
      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveAddAttachmentNews(json)))
      .catch(err => {
        dispatch(errorAddAttachmentNews(err));
      });
  };
}

/* News Delete */

export const REQUEST_DELETE_NEWS = 'REQUEST_DELETE_NEWS';
export const RECEIVE_DELETE_NEWS = 'RECEIVE_DELETE_NEWS';
export const ERROR_DELETE_NEWS   = 'ERROR_DELETE_NEWS';


function requestDeleteNews() {
  return {
    type: REQUEST_DELETE_NEWS
  };
}

function errorDeleteNews(error) {
  return {
    type: ERROR_DELETE_NEWS,
    error
  };
}

function receiveDeleteNews() {
  return {
    type: RECEIVE_DELETE_NEWS
  };
}

export function fetchDeleteNews(id) {
  return (dispatch, getState) => {
    if (shouldFetchDeleteNews(getState())) {
      return dispatch(fetchDeleteNewsDo(getState(), id));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchDeleteNews(state) {
  return !state.news.singleNews.loading;
}

function fetchDeleteNewsDo(state, id) {
  return dispatch => {
    const token = localStorage.getItem('token');

    dispatch(requestDeleteNews());
    console.log('Fetch: Delete News');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/social-news/${id}/`, {
        method: 'delete',
        credentials: 'same-origin',
        headers: {
          'Authorization': `${getToken()}`
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveDeleteNews(json)))
      .then(obj => dispatch(deleteNewsFromNewsFeed(id)))
      .catch(err => {
        dispatch(errorDeleteNews(err));
      });
  };
}

/* Get News */

export const REQUEST_GET_SINGLE_NEWS = 'REQUEST_GET_SINGLE_NEWS';
export const RECEIVE_GET_SINGLE_NEWS = 'RECEIVE_GET_SINGLE_NEWS';
export const ERROR_GET_SINGLE_NEWS   = 'ERROR_GET_SINGLE_NEWS';


function requestGetSingleNews() {
  return {
    type: REQUEST_GET_SINGLE_NEWS
  };
}

function errorGetSingleNews(error) {
  return {
    type: ERROR_GET_SINGLE_NEWS,
    error
  };
}

function receiveGetSingleNews(news) {
  return {
    type: RECEIVE_GET_SINGLE_NEWS,
    news
  };
}

export function fetchGetSingleNews(id) {
  return (dispatch, getState) => {
    if (shouldFetchGetSingleNews(getState())) {
      return dispatch(fetchGetSingleNewsDo(getState(), id));
    }
  };
}

/* eslint-disable no-unused-vars */
function shouldFetchGetSingleNews(state) {
  return true;
}

function fetchGetSingleNewsDo(state, id) {
  return dispatch => {
    dispatch(requestGetSingleNews());
    console.log('Fetch: Get Single News');
    return fetch(`${pathAPI}/social-news/${id}`, {
      method: 'get',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      }

    })
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveGetSingleNews(json)))
      .catch(err => {
        dispatch(errorGetSingleNews(err));
      });
  };
}


/* /News Publish */

// export const REQUEST_PUBLISH_NEWS = 'REQUEST_PUBLISH_NEWS';
// export const RECEIVE_PUBLISH_NEWS = 'RECEIVE_PUBLISH_NEWS';
// export const ERROR_PUBLISH_NEWS   = 'ERROR_PUBLISH_NEWS';
//
// function requestPublishNews() {
//   return {
//     type: REQUEST_PUBLISH_NEWS
//   };
// }
//
// function errorPublishNews(error) {
//   return {
//     type: ERROR_PUBLISH_NEWS,
//     error
//   };
// }
//
// function receivePublishNews(id) {
//   return {
//     type: RECEIVE_PUBLISH_NEWS,
//     id
//   };
// }
//
// export function fetchPublishNews(newsId) {
//   return (dispatch, getState) => {
//     if (shouldFetchPublishNews(getState())) {
//       return dispatch(fetchPublishNewsDo(getState(), newsId));
//     }
//   };
// }
//
// /* eslint-disable no-unused-vars */
// function shouldFetchPublishNews(state) {
//   return true;
// }
//
// function fetchPublishNewsDo(state, newsId) {
//   return dispatch => {
//     const token = localStorage.getItem('token');
//
//     dispatch(requestPublishNews());
//     console.log('Fetch: Publish News');
//     return fetch(`${pathAPI}/social-news/${newsId}/publish`, {
//       method: 'put',
//       credentials: 'same-origin',
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json',
//         'Authorization': `${token}`
//       }
//
//     })
//       .then(response => {
//         const json = response.json();
//
//         if (response.status !== 200) {
//           return Promise.reject(response.statusText);
//         }
//         return json;
//       })
//       .then(json => dispatch(receivePublishNews(json)))
//       .catch(err => {
//         dispatch(errorPublishNews(err));
//       });
//   };
// }

