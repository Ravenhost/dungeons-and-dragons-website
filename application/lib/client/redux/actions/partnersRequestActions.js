import fetch              from 'isomorphic-fetch';
import pathAPI            from '../pathAPI';
import fetchDecorator     from './common/fetchDecorator';
import {
  getToken,
  getUserId
}                         from '../../redux/utils/helpers';
import decoratorAuthCheck from './common/protectedAuth';


export const REQUEST_INCOMING_REQUEST_PARTNERS = 'REQUEST_INCOMING_REQUEST_PARTNERS';
export const RECEIVE_INCOMING_REQUEST_PARTNERS = 'RECEIVE_INCOMING_REQUEST_PARTNERS';
export const ERROR_INCOMING_REQUEST_PARTNERS   = 'ERROR_INCOMING_REQUEST_PARTNERS';

function requestIncomingRequestPartners() {
  return {
    type: REQUEST_INCOMING_REQUEST_PARTNERS
  };
}

function errorIncomingRequestPartners(error) {
  return {
    type: ERROR_INCOMING_REQUEST_PARTNERS,
    error
  };
}

function receiveIncomingRequestPartners(fetchedRequest, page, search) {
  return {
    type: RECEIVE_INCOMING_REQUEST_PARTNERS,
    fetchedRequest,
    page,
    search
  };
}

export function fetchIncomingRequestPartners(params) {
  return (dispatch, getState) => {
    if (shouldFetchIncomingRequestPartners(getState(), params)) {
      return dispatch(fetchIncomingRequestPartnersDo(getState(), params));
    }
  };
}

function shouldFetchIncomingRequestPartners(state, params) {
  if (state.partnersRequest.incomingRequests.loading) {
    return false;
  }
  const incoming = state.partnersRequest.incomingRequests;

  if (incoming.search !== params.search) {
    return true;
  }
  return params.page > incoming.page && !incoming.isLast;
}

function fetchIncomingRequestPartnersDo(state, params) {
  return dispatch => {
    const page = params.page;
    const perPage = params.perPage;
    const search = params.search || '';

    dispatch(requestIncomingRequestPartners());
    console.log('Fetch: Incoming Request Partners');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/requests/incoming?perPage=${perPage}&page=${page}&search=${search}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveIncomingRequestPartners(json, page, search)))
      .catch(err => {
        dispatch(errorIncomingRequestPartners(err));
      });
  };
}

export const REQUEST_ACCEPT_REQUEST_PARTNERS = 'REQUEST_ACCEPT_REQUEST_PARTNERS';
export const RECEIVE_ACCEPT_REQUEST_PARTNERS = 'RECEIVE_ACCEPT_REQUEST_PARTNERS';
export const ERROR_ACCEPT_REQUEST_PARTNERS   = 'ERROR_ACCEPT_REQUEST_PARTNERS';

function requestAcceptRequestPartners() {
  return {
    type: REQUEST_ACCEPT_REQUEST_PARTNERS
  };
}

function errorAcceptRequestPartners(error) {
  return {
    type: ERROR_ACCEPT_REQUEST_PARTNERS,
    error
  };
}

function receiveAcceptRequestPartners(json, partner, userId) {
  return {
    type: RECEIVE_ACCEPT_REQUEST_PARTNERS,
    json,
    partner,
    userId
  };
}

export function fetchAcceptRequestPartners(obj) {
  return (dispatch, getState) => {
    if (shouldFetchAcceptRequestPartners(getState())) {
      return dispatch(fetchAcceptRequestPartnersDo(getState(), obj));
    }
  };
}

function shouldFetchAcceptRequestPartners(state) {
  return !state.partnersRequest.incomingRequests.loading;
}

function fetchAcceptRequestPartnersDo(state, obj) {
  return dispatch => {
    dispatch(requestAcceptRequestPartners());
    console.log('Fetch: Accept Request Partners');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/requests/accept/${obj._id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveAcceptRequestPartners(json, obj, getUserId())))
      .catch(err => {
        dispatch(errorAcceptRequestPartners(err));
      });
  };
}

export const REQUEST_REJECT_REQUEST_PARTNERS = 'REQUEST_REJECT_REQUEST_PARTNERS';
export const RECEIVE_REJECT_REQUEST_PARTNERS = 'RECEIVE_REJECT_REQUEST_PARTNERS';
export const ERROR_REJECT_REQUEST_PARTNERS   = 'ERROR_REJECT_REQUEST_PARTNERS';

function requestRejectRequestPartners() {
  return {
    type: REQUEST_REJECT_REQUEST_PARTNERS
  };
}

function errorRejectRequestPartners(error) {
  return {
    type: ERROR_REJECT_REQUEST_PARTNERS,
    error
  };
}

function receiveRejectRequestPartners(json, partner, userId) {
  return {
    type: RECEIVE_REJECT_REQUEST_PARTNERS,
    json,
    partner,
    userId
  };
}

export function fetchRejectRequestPartners(obj) {
  return (dispatch, getState) => {
    if (shouldFetchRejectRequestPartners(getState())) {
      return dispatch(fetchRejectRequestPartnersDo(getState(), obj));
    }
  };
}

function shouldFetchRejectRequestPartners(state) {
  return !state.partnersRequest.incomingRequests.loading;
}

function fetchRejectRequestPartnersDo(state, obj) {
  return dispatch => {
    dispatch(requestRejectRequestPartners());
    // dispatch(receiveRejectRequestPartners('', obj, getUserId()));
    // dispatch(errorRejectRequestPartners(''));
    console.log('Fetch: Reject Request Partners');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/requests/reject/${obj._id}`, {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveRejectRequestPartners(json, obj, getUserId())))
      .catch(err => {
        dispatch(errorRejectRequestPartners(err));
      });
  };
}

export const REQUEST_OUTGOING_REQUEST_PARTNERS = 'REQUEST_OUTGOING_REQUEST_PARTNERS';
export const RECEIVE_OUTGOING_REQUEST_PARTNERS = 'RECEIVE_OUTCOMING_REQUEST_PARTNERS';
export const ERROR_OUTGOING_REQUEST_PARTNERS   = 'ERROR_OUTCOMING_REQUEST_PARTNERS';

function requestOutgoingRequestPartners() {
  return {
    type: REQUEST_OUTGOING_REQUEST_PARTNERS
  };
}

function errorOutgoingRequestPartners(error) {
  return {
    type: ERROR_OUTGOING_REQUEST_PARTNERS,
    error
  };
}

function receiveOutgoingRequestPartners(fetchedRequest, page, search) {
  return {
    type: RECEIVE_OUTGOING_REQUEST_PARTNERS,
    fetchedRequest,
    page,
    search
  };
}

export function fetchOutgoingRequestPartners(params) {
  return (dispatch, getState) => {
    if (shouldFetchOutgoingRequestPartners(getState(), params)) {
      return dispatch(fetchOutgoingRequestPartnersDo(getState(), params));
    }
  };
}

function shouldFetchOutgoingRequestPartners(state, params) {
  if (state.partnersRequest.outgoingRequests.loading) {
    return false;
  }
  const incoming = state.partnersRequest.outgoingRequests;

  if (incoming.search !== params.search) {
    return true;
  }
  return params.page > incoming.page && !incoming.isLast;
}

function fetchOutgoingRequestPartnersDo(state, params) {
  return dispatch => {
    const page = params.page;
    const perPage = params.perPage;
    const search = params.search || '';

    dispatch(requestOutgoingRequestPartners());
    console.log('Fetch: Outgoing Request Partners');
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/requests?perPage=${perPage}&page=${page}&search=${search}`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveOutgoingRequestPartners(json, page, search)))
      .catch(err => {
        dispatch(errorOutgoingRequestPartners(err));
      });
  };
}

export const REQUEST_DELETE_REQUEST_PARTNERS = 'REQUEST_DELETE_REQUEST_PARTNERS';
export const RECEIVE_DELETE_REQUEST_PARTNERS = 'RECEIVE_DELETE_REQUEST_PARTNERS';
export const ERROR_DELETE_REQUEST_PARTNERS   = 'ERROR_DELETE_REQUEST_PARTNERS';

function requestDeleteRequestPartners() {
  return {
    type: REQUEST_DELETE_REQUEST_PARTNERS
  };
}

function errorDeleteRequestPartners(error) {
  return {
    type: ERROR_DELETE_REQUEST_PARTNERS,
    error
  };
}

function receiveDeleteRequestPartners(json, partnerId) {
  return {
    type: RECEIVE_DELETE_REQUEST_PARTNERS,
    json,
    partnerId
  };
}

export function fetchDeleteRequestPartners(partnerId) {
  return (dispatch, getState) => {
    if (shouldFetchDeleteRequestPartners(getState())) {
      return dispatch(fetchDeleteRequestPartnersDo(getState(), partnerId));
    }
  };
}

function shouldFetchDeleteRequestPartners(state) {
  return !state.partnersRequest.outgoingRequests.loading;
}

function fetchDeleteRequestPartnersDo(state, partnerId) {
  return dispatch => {
    console.log('Fetch: Delete Request Partners');
    dispatch(requestDeleteRequestPartners());
    dispatch(receiveDeleteRequestPartners('', partnerId));
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/requests/${partnerId}`, {
        method: 'DELETE',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveDeleteRequestPartners(json, partnerId)))
      .catch(err => {
        dispatch(errorDeleteRequestPartners(err));
      });
  };
}
