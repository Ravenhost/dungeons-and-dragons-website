import fetch from 'isomorphic-fetch';
import { browserHistory } from 'react-router';

export const SIGN_IN_ERROR_IS_NOT_CONFIRMED = 'SIGN_IN_ERROR_IS_NOT_CONFIRMED';
export const SIGN_IN_ERROR_OTHER = 'SIGN_IN_ERROR_OTHER';

import { setToken, setUserId } from '../utils/helpers';

import {
  SIGN_IN_REQUEST,
  SIGN_IN_FAIL,
  CONFIRM_EMAIL_REQUEST,
  CONFIRM_EMAIL_RESPONSE,
  SIGN_IN_RESET_STORE
} from '../types';

import pathAPI from '../pathAPI';

export function signIn(email, password) {
  return (dispatch) => {
    dispatch(signInDo(email, password));
  };
}

function signInRequest(email, password) {
  return {
    type: SIGN_IN_REQUEST,
    email,
    password
  };
}

function signInDo(email, password) {
  return (dispatch) => {
    dispatch(signInRequest(email, password));
    const data = { email, password };
    let status = 0;

    return fetch(`${pathAPI}/auth/`, {
      method: 'POST',
      headers: new Headers({ 'content-type': 'application/json' }),
      body: JSON.stringify(data),
      credentials: 'same-origin'
    }).then(response => {
      status = response.status;
      return response.json();
    })
      .then((json) => {
        if (status === 200) {
          dispatch(signInSuccess(json.token, json.id));
        } else {
          let errorCode = SIGN_IN_ERROR_OTHER;
          const errorMessage = json.message;

          if (errorMessage === 'Пользователь не подтвердил e-mail.') {
            errorCode = SIGN_IN_ERROR_IS_NOT_CONFIRMED;
          }
          dispatch(signInFail(json, status, errorCode, errorMessage));
        }
      });
  };
}

export function signInSuccess(token, userId) {
  return () => {
    setToken(token);
    setUserId(userId);
    const url = `/id${userId}`;

    browserHistory.push(url);
  };
}

function signInFail(json, status, errorCode, errorMessage) {
  return {
    type: SIGN_IN_FAIL,
    json,
    status,
    errorCode,
    errorMessage
  };
}

export function signInResetStore() {
  return {
    type: SIGN_IN_RESET_STORE
  };
}

// ------------------------------------------------------------
export function resendConfirmEmail(email) {
  return (dispatch) => {
    dispatch(resendConfirmEmailDo(email));
  };
}

function resendConfirmEmailDo(email) {
  return (dispatch) => {
    dispatch(resendConfirmEmailRequest(email));
    const data = { email };
    let status = 0;

    return fetch(`${pathAPI}/auth/confirm`, {
      method: 'POST',
      headers: new Headers({ 'content-type': 'application/json' }),
      body: JSON.stringify(data),
      credentials: 'same-origin'
    }).then(response => {
      status = response.status;
      return response.json();
    })
      .then((json) => {
        if (status === 200) {
          dispatch(resendConfirmEmailSuccess(json));
        } else {
          dispatch(resendConfirmEmailFail(json, status));
        }
      });
  };
}

function resendConfirmEmailRequest(email) {
  return {
    type: CONFIRM_EMAIL_REQUEST,
    email
  };
}


export function resendConfirmEmailSuccess(json) {
  return {
    type: CONFIRM_EMAIL_RESPONSE,
    hasError: false,
    json
  };
}

function resendConfirmEmailFail(json, status) {
  return {
    type: CONFIRM_EMAIL_RESPONSE,
    hasError: true,
    json,
    status
  };
}
