import { browserHistory } from 'react-router';
import { logout }         from '../../utils/helpers';

export default function decoratorAuthCheck(dispatch) {
  return this
    .then(res => {
      if (res.status === 401) {
        logout();
        if (dispatch) {
          dispatch(browserHistory.push('/sign-in'));
        } else {
          browserHistory.push('/sign-in');
        }
        return Promise.reject('Unauthorized');
      }
      return res;
    });
}
