export default function fetchDecorator(decorators, dispatch, fetchPromise) {
  let decoratedFunc = fetchPromise;

  decorators.forEach((element) => {
    decoratedFunc = element.call(decoratedFunc, dispatch);
  });
  return decoratedFunc;
}
