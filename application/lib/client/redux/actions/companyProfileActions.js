import fetch                              from 'isomorphic-fetch';
import {
  COMPANY_PROFILE_GENERAL_REQUEST,
  COMPANY_PROFILE_GENERAL_RESPONSE,
  COMPANY_PROFILE_SET_BRAND_LOCAL,
  COMPANY_PROFILE_SET_DESCRIPTION_LOCAL,
  COMPANY_PROFILE_SET_AVATAR_LOCAL
}                                         from '../types';

import pathAPI                            from '../pathAPI';
import {
  getToken,
  transformObjectWithHtmlEntities
}                                         from '../../redux/utils/helpers';
import decoratorAuthCheck                 from './common/protectedAuth';
import fetchDecorator                     from './common/fetchDecorator';

// Получить базовую информацию для карточки компании.
//---------------------------------------------------------------------------

function companyProfileGeneralRequest(id) {
  return {
    type: COMPANY_PROFILE_GENERAL_REQUEST,
    id
  };
}
function companyProfileGeneralResponse(json, hasError) {
  return {
    type: COMPANY_PROFILE_GENERAL_RESPONSE,
    json: transformObjectWithHtmlEntities(json),
    hasError
  };
}

function shouldGetProfileGeneral(state) {
  return !state.companyProfile.profile.isFetching;
}

function getProfileGeneralDo(id) {
  return dispatch => {
    let status = 0;

    dispatch(companyProfileGeneralRequest(id));
    return fetch(`${pathAPI}/users/${id}`, {
      method: 'GET',
      headers: new Headers({ 'content-type': 'application/json' }),
      credentials: 'same-origin'
    })
      .then(response => {
        status = response.status;
        return response.json();
      })
      .then(json => {
        let hasError = false;

        if (status !== 200)  hasError = true;
        dispatch(companyProfileGeneralResponse(json, hasError));
      });
  };
}

export function getProfileGeneral(id) {
  return (dispatch, getState) => {
    if (shouldGetProfileGeneral(getState())) {
      return dispatch(getProfileGeneralDo(id));
    }
  };
}

// Получить ленту новостей для компании.
//---------------------------------------------------------------------------

export function setNewAvatar(url) {
  return (dispatch) => {
    return dispatch(NewAvatar(url));
  };
}

function NewAvatar(url) {
  return {
    type: COMPANY_PROFILE_SET_AVATAR_LOCAL,
    url
  };
}

//---------------------------------------------------------------------------
export function setNewBrand(brand) {
  return {
    type: COMPANY_PROFILE_SET_BRAND_LOCAL,
    brand
  };
}

export function setNewDescription(description) {
  return {
    type: COMPANY_PROFILE_SET_DESCRIPTION_LOCAL,
    description
  };
}
//---------------------------------------------------------------------------
export function removeAvatar() {
  return (dispatch) => {
    return dispatch(removeAvatarDo());
  };
}

function removeAvatarDo() {
  return dispatch => {
    let status = 0;

    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/users/avatar/`, {
        method: 'DELETE',
        headers: new Headers({ 'content-type': 'application/json', 'Authorization': getToken() }),
        credentials: 'same-origin'
      }))
      .then(response => {
        status = response.status;
        return response.json();
      })
      .then(json => {
        if (status === 200) {
          dispatch(setNewAvatar(json.avatar));
        }
      });
  };
}


export const REQUEST_PROFILE_COUNTERS = 'REQUEST_PROFILE_COUNTERS';
export const RECEIVE_PROFILE_COUNTERS = 'RECEIVE_PROFILE_COUNTERS';
export const ERROR_PROFILE_COUNTERS   = 'ERROR_PROFILE_COUNTERS';

function requestProfileCounters() {
  return {
    type: REQUEST_PROFILE_COUNTERS
  };
}

function receiveProfileCounters(counters) {
  return {
    type: RECEIVE_PROFILE_COUNTERS,
    counters
  };
}

function errorProfileCounters(error) {
  return {
    type: ERROR_PROFILE_COUNTERS,
    error
  };
}

export function fetchProfileCounters() {
  return (dispatch, getState) => {
    if (shouldFetchProfileCounters(getState())) {
      return dispatch(fetchProfileCountersDo(getState()));
    }
  };
}

function shouldFetchProfileCounters() {
  return true;
}

function fetchProfileCountersDo() {
  return dispatch => {
    console.log('Fetch: Profile Counters');
    dispatch(requestProfileCounters());
    return fetchDecorator([ decoratorAuthCheck ], dispatch,
      fetch(`${pathAPI}/users/counters`, {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': getToken()
        }

      }))
      .then(response => {
        const json = response.json();

        if (response.status !== 200) {
          return Promise.reject(response.statusText);
        }
        return json;
      })
      .then(json => dispatch(receiveProfileCounters(json)))
      .catch(err => {
        dispatch(errorProfileCounters(err));
      });
  };
}

