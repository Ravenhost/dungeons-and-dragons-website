import { AllHtmlEntities }                from 'html-entities';

const transformer = new AllHtmlEntities();

export function transformObjectWithHtmlEntities(convertObj) {
  const convertedObj = convertObj;

  for (const property in convertedObj) {
    if (convertedObj.hasOwnProperty(property)) {
      if (typeof (convertedObj[property]) === 'object') {
        transformObjectWithHtmlEntities(convertedObj[property]);
      }
      if (typeof (convertObj[property]) === 'string') {
        convertedObj[property] = decodeStringWithHtmlEntities(convertObj[property]);
      }
    }
  }
  return convertObj;
}


export function decodeStringWithHtmlEntities(string) {
  return transformer.decode(string);
}
