import { AllHtmlEntities }                from 'html-entities';

const transformer = new AllHtmlEntities();

export default function (string) {
  return transformer.decode(string);
}
