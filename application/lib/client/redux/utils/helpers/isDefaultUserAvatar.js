export default function (avatarUrl) {
  const splitedUrl = avatarUrl.split('/');
  const imageName = splitedUrl[splitedUrl.length - 1];

  return imageName.indexOf('default-avatar') !== -1;
}
