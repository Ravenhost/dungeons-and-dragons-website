import isBrowser                              from './isBrowser';
import isLoaded                               from './isLoaded';
import getToken                               from './getToken';
import setToken                               from './setToken';
import getUserId                              from './getUserId';
import setUserId                              from './setUserId';
import logout                                 from './logout';
import getAvatarUrl                           from './getAvatarUrl';
import clearSingleNews                        from './clearSingleNews';
import {
  transformObjectWithHtmlEntities,
  decodeStringWithHtmlEntities
}                                             from './transformObjectWithHtmlEntities';
import isDefaultUserAvatar                    from './isDefaultUserAvatar';

export {
  isBrowser,
  isLoaded,
  getToken,
  setToken,
  getUserId,
  setUserId,
  logout,
  getAvatarUrl,
  decodeStringWithHtmlEntities,
  transformObjectWithHtmlEntities,
  clearSingleNews,
  isDefaultUserAvatar
};
