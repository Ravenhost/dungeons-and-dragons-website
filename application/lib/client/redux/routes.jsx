import React                        from 'react';
import {
  IndexRoute,
  Router,
  Route,
  browserHistory
}                                   from 'react-router';
import { ReduxAsyncConnect }        from 'redux-connect';
import App                          from '../components/App';
import NewsList                     from '../components/NewsList/NewsList';
import CompanyPage                  from '../components/CompanyPage/CompanyPage';
import SignUpPage                   from '../components/SignUpPage/SignUpPage';
import SignUpConfirmEmail           from '../components/SignUpConfirmEmail/SignUpConfirmEmail';
import SignInPage                   from '../components/SignInPage/SignInPage';
import RecoverPasswordPage          from '../components/RecoverPasswordPage/RecoverPasswordPage';
import ResetPasswordPage            from '../components/ResetPasswordPage/ResetPasswordPage';
import CompanyNewsEdit              from '../components/CompanyNews/CompanyNewsEdit';
import MyNewsList                   from '../components/CompanyNews/MyNewsList';
import {
  CompanyPartners,
  Partners,
  IncomingProposal,
  OutgoingProposal,
  Followers,
  Subscriptions,
  Recommended,
  MutualPartners
}                                   from '../components/Partners';
import checkLogin                   from './utils/routeGuard/isAuth';
import SearchPage                   from  '../components/SearchPage/SearchPage';

export default (
  <Router render={(props) => <ReduxAsyncConnect{...props} />} history={browserHistory}>
    <Route path='/' component={App}>
      <IndexRoute component={NewsList} />
      <Route path='/sign-up' component={SignUpPage} />
      <Route path='/sign-up/confirm-email/:token' component={SignUpConfirmEmail} />
      <Route path='/sign-in' component={SignInPage} />
      <Route path='/password' component={RecoverPasswordPage} />
      <Route path='/reset-password/:token' component={ResetPasswordPage} />
      <Route path='/id:id' component={CompanyPage} />
      <Route path='/news-edit/:newsId' component={CompanyNewsEdit} onEnter={checkLogin} />
      <Route path='/news-list/:userId' component={MyNewsList} />
      <Route path='/search-page' component={SearchPage} />
      <Route components={Partners}>
        <Route path='/partners/:companyId/partners' component={CompanyPartners}/>
        <Route path='/partners/:companyId/all_requests' component={IncomingProposal}/>
        <Route path='/partners/:companyId/out_requests' component={OutgoingProposal}/>
        <Route path='/partners/:companyId/following' component={Subscriptions}/>
        <Route path='/partners/:companyId/followers' component={Followers}/>
        <Route path='/partners/:companyId/recommended' component={Recommended}/>
        <Route path='/partners/:companyId/mutual' component={MutualPartners}/>
      </Route>
    </Route>
  </Router>
);
