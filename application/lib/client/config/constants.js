export const USER_TYPE_COMPANY = 'company';
export const USER_TYPE_ENTREPRENEUR = 'entrepreneur';
export const USER_TYPE_PERSON = 'person';

export const API_PATH  = 'http://localhost:8080/';
