import React, { Component, PropTypes } from 'react';
import { connect }                     from 'react-redux';
import {
  Post,
  RePost
}                                      from '../Post';
import {
  fetchCreateNews
}                                      from '../../redux/actions/newsActions';

class CompanyFeed extends Component {
  constructor(props) {
    super(props);

    this.handleWindowScroll = this.handleWindowScroll.bind(this);
    this.handleAddNews = this.handleAddNews.bind(this);
  }

  componentDidMount() {
    if (typeof document !== 'undefined') {
      document.addEventListener('scroll', this.handleWindowScroll);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleWindowScroll);
  }

  handleAddNews() {
    this.props.dispatch(fetchCreateNews());
  }

  handleWindowScroll() {
    const footerPositionBottom = document.getElementsByClassName('uou-block-4a')[0].getBoundingClientRect().bottom;
    const windowHeight = document.documentElement.clientHeight;

    if (parseInt(footerPositionBottom - windowHeight, 10) === 0) {
      this.props.onHandleNeedMore();
    }
  }

  getFeed(feed) {
    return feed.map((value) => {
      return this.renderNewsFromObject(value);
    });
  }

  haveFeed() {
    return this.props.companyFeed.length > 0;
  }

  renderNewsFromObject(obj) {
    return (obj.sourceId) ?
        (
          <RePost
            key={obj._id}
            rePostInfo={obj}
            isMyCompany={this.props.isMyCompany}
            isReadOnly={this.props.isReadOnly}
          />
        ) :
        (
          <Post
            key={obj._id}
            postInfo={obj}
            isMyCompany={this.props.isMyCompany}
            isReadOnly={this.props.isReadOnly}
          />
        );
  }

  render() {
    let feed = <p>Загружаем ленту новостей...</p>;

    if (this.haveFeed()) {
      feed = this.getFeed(this.props.companyFeed);
    } else if (this.props.isLoaded) {
      feed = <p>У вас пока нет новостей.</p>;
    }
    return (
      <div className='profile-block'>
        <h6>{this.props.title}</h6>
        <button
          className={`btn btn-transparent-primary width-100proc ${this.props.isReadOnly ? 'visuallyhidden' : '' }`}
          onClick={this.handleAddNews}
        >Добавить новость</button>
        {this.props.children}
        <div className='profile-in profile-serv mt15'>
          {feed}
        </div>
      </div>);
  }
}

const propTypes = {
  title: PropTypes.string,
  companyFeed: PropTypes.array,
  onHandleNeedMore: PropTypes.func,
  isReadOnly: PropTypes.bool,
  isLoaded: PropTypes.bool,
  children: PropTypes.node,
  dispatch: PropTypes.func,
  isMyCompany: PropTypes.bool
};

const defaultProps = {
  title: '',
  companyFeed: [],
  onHandleNeedMore: () => {},
  isReadOnly: true,
  isLoaded: false,
  dispatch: () => {},
  isMyCompany: true
};

CompanyFeed.propTypes = propTypes;
CompanyFeed.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  return {

  };
}
/* eslint-enable no-unused-vars */


export default connect(mapStateToProps)(CompanyFeed);
