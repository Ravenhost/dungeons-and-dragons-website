import CompanyPage from './CompanyPage';
import CompanyPagePerson from './CompanyPage';

export default CompanyPage;
export {
  CompanyPage,
  CompanyPagePerson
};
