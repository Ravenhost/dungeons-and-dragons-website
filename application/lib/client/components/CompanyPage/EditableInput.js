import React, { Component, PropTypes }  from 'react';
import ReactDOM                         from 'react-dom';
import pathAPI                          from '../../redux/pathAPI';
import { getToken }                     from '../../redux/utils/helpers';
import decoratorAuthCheck               from '../../redux/actions/common/protectedAuth';
import fetchDecorator                   from '../../redux/actions/common/fetchDecorator';

export default class EditableInput extends Component {

  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleStartEditing = this.handleStartEditing.bind(this);
    this.handleSaveClick = this.handleSaveClick.bind(this);
    this.handleCancelClick = this.handleCancelClick.bind(this);
    this.saveOk = this.saveOk.bind(this);
    this.saveFail = this.saveFail.bind(this);

    this.state = {
      isEditing: false,
      isSaving: false,
      isSaveOk: false,
      isSaveFail: false,
      newText: ''
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const inputElem = ReactDOM.findDOMNode(this.editor);

    if (this.state.isEditing && !prevState.isEditing) {
      inputElem.focus();
      // this.selectInputText(inputElem);
    }
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleStartEditing = () => {
    this.setState({ isEditing: true });
    this.setState({ newText: this.props.text });
  };

  handleSaveClick = () => {
    this.setState({ isSaving: true });
    let status = 0;

    const inJson = { [this.props.fieldName]: this.state.newText };

    return fetchDecorator([ decoratorAuthCheck ], null,
      fetch(`${pathAPI}/users`, {
        method: 'PUT',
        headers: new Headers({ 'content-type': 'application/json', 'Authorization': getToken() }),
        body: JSON.stringify(inJson),
        credentials: 'same-origin'
      })).then(response => {
        status = response.status;
        return response.json();
      }).then((json) => {
        if (status === 200) {
          this.saveOk();
        } else {
          this.saveFail(json);
        }
      }).catch(() => {
        this.saveFail();
      });
  };

  handleCancelClick = () => {
    this.setState(
      {
        isSaving: false,
        isEditing: false,
        isSaveFail: false,
        isSaveOk: false
      });
  };

  handleKeyDown = (event) => {
    if (event.keyCode === 13) {
      this.handleSaveClick();
    } else if (event.keyCode === 27) {
      this.handleCancelClick();
    }
  };

  saveOk = () => {
    this.setState(
      {
        isSaving: false,
        isEditing: false,
        isSaveOk: true,
        isSaveFail: false
      });
    this.props.onChange(this.state.newText);
  };

  saveFail = () => {
    this.setState(
      {
        isSaving: false,
        isEditing: true,
        isSaveOk: false,
        isSaveFail: true
      });
  };

  editorPencil() {
    return (
      <i
        className='fa fa-pencil fa-fw ml15 fontIconSize20 clickable'
        aria-hidden='true'
        tabIndex='0'
        onClick={this.handleStartEditing}
      />
    );
  }

  renderTextArea() {
    return (
      <textarea
        className='editable-field__textarea'
        name='newText'
        ref={(c) => {
          this.editor = c;
        }}
        defaultValue={this.props.text}
        disabled={this.state.isSaving}
        rows='3'
        onChange={this.handleInputChange}
        onKeyDown={this.handleKeyDown}
      />
    );
  }

  renderError() {
    return (
      <p className='error'>Ошибка при сохранении</p>
    );
  }

  renderInput() {
    return (
      <input
        className='editable-field__input'
        name='newText'
        ref={(c) => {
          this.editor = c;
        }}
        disabled={this.state.isSaving}
        defaultValue={this.props.text}
        onChange={this.handleInputChange}
        onKeyDown={this.handleKeyDown}
      />);
  }

  renderEditingComponent() {
    return (
      <div className='editable-field'>
        {this.state.isSaveFail && this.renderError()}

        {this.props.isTextArea ? this.renderTextArea() : this.renderInput()}

        <i
          className='fa fa-check fontIconSize20 ml15 clickable'
          aria-hidden='true'
          tabIndex='0'
          onClick={this.handleSaveClick}
        />

        <i
          className='fa fa-times fontIconSize20 ml15 clickable'
          aria-hidden='true'
          tabIndex='0'
          onClick={this.handleCancelClick}
        />
      </div>

    );
  }

  renderNormalComponent() {
    return (
      <div className='info-field info-field--description'>
        <span>{this.props.text || this.props.emptyText}</span>
        {!this.props.readOnly && this.editorPencil()}
      </div>
    );
  }

  render() {
    if (this.state.isEditing) {
      return this.renderEditingComponent();
    }
    return this.renderNormalComponent();
  }
}

const defaultProps = {
  text: '',
  emptyText: 'Не указано',
  onChange: () => {
  },
  isTextArea: false,
  fieldName: 'brand',
  readOnly: true
};

const propTypes = {
  text: PropTypes.string,
  emptyText: PropTypes.string,
  onChange: PropTypes.func,
  isTextArea: PropTypes.bool,
  fieldName: PropTypes.string,
  readOnly: PropTypes.bool
};

EditableInput.propTypes = propTypes;
EditableInput.defaultProps = defaultProps;
