import React, { Component, PropTypes }  from 'react';
//
// const defaultProps = {
//   dispatch: () => {}
// };
//
// const propTypes = {
//   dispatch: PropTypes.func
// };

export default class CompanyPagePerson extends Component {

  componentDidMount() {

  }

  showInfo(event){

    event.preventDefault();

    var $accordion = $(event.currentTarget),
      $li = $accordion.find('.info-slide__item'),
      $div = $accordion.find('.info-slide__content');

    if (!$li.hasClass('active')) {
      $div.slideDown(250, function () {
        $li.addClass('active');
      });
    } else {
      $div.slideUp(250, function () {
        $li.removeClass('active');
      });
    }
  }

  render() {
    return (
      <div>
        <div className='compny-profile'>
          <div className='profile-company-content'>
            <div className='container'>
              <div className='row'>
                { /* <!-- SIDEBAR -->*/ }
                <div className='col-md-4 col-md-push-8'>
                  { /* <!-- Company Information --> */ }
                  <div className='sidebar profile-block text-center'>
                    <div className='img-profile img-rounded img-rounded--equal width-80proc mt25 mb15 '>
                      <img className='media-object img-rounded__image'  src='/images/news3.png' alt=''/>
                    </div>
                    <div className='sidebar-information'>
                      <table className='contacts-table'>
                        <tr>
                          <th>Телефон:</th>
                          <td>8-800-382-33-23</td>
                        </tr>
                        <tr>
                          <th>E-mail:</th>
                          <td>pochta@mail.ru</td>
                        </tr>
                        <tr>
                          <th>Сайт:</th>
                          <td>website.ru</td>
                        </tr>
                      </table>
                    </div>
                    <button type='button' className='btn btn-transparent-primary mb15 width-100proc' data-dismiss='modal'>Подать жалобу
                    </button>
                  </div>
                </div>

                { /* <!-- Content --> */ }
                <div className='col-md-8 col-md-pull-4'>
                  <div className='tab-content'>
                    <div id='profile' className='tab-pane fade in active'>

                      { /* About */ }
                      <div className='profile-block'>
                        <div className='profile-in'>
                          <h5>Константин Константинович Константинопольский</h5>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                            Proin sodales pulvinar tempor.
                            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Proin sodales pulvinar tempor.
                            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

                          <table className='contacts-table width-auto'>
                            <tr>
                              <th>Компания:</th>
                              <td>АстраМедиа</td>
                            </tr>
                            <tr>
                              <th>Должность:</th>
                              <td>Генеральный директор</td>
                            </tr>
                          </table>
                        </div>
                      </div>
                      { /* About END */ }

                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// CompanyPagePerson.propTypes = propTypes;
// CompanyPagePerson.defaultProps = defaultProps;
