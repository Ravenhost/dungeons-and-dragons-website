import React, { Component, PropTypes }      from 'react';
import { asyncConnect }                     from 'redux-connect';
import { isBrowser, isLoaded }              from '../../redux/utils/helpers';
import {
  getProfileGeneral,
  setNewBrand,
  setNewDescription,
  setNewAvatar,
  removeAvatar
}                                           from '../../redux/actions/companyProfileActions';

import {
  fetchCompanyFeed,
  checkIsMyNews,
  likePost,
  changeAvatarInFeed
}                                           from '../../redux/actions/companyFeedActions';
import EditableInput                        from './EditableInput';
import CompanyFeed                          from './CompanyFeed';
import {
  getUserId,
  getToken,
  getAvatarUrl,
  isDefaultUserAvatar
}                                           from '../../redux/utils/helpers';
import pathAPI                              from '../../redux/pathAPI';
import Cropper                              from 'cropperjs';


const asyncPromises = [
  {
    key: 'profile',
    promise: ({ store, params }) => {
      const id = params.id;

      if (!isBrowser()) {
        const state = store.getState();

        if (!isLoaded(state, 'companyProfile')) {
          return store.dispatch(getProfileGeneral(id));
        }
      }

      return null;
    }
  },
  {
    key: 'feed',
    promise: ({ store, params }) => {
      const id = params.id;

      if (!isBrowser()) {
        console.log('fetchCompanyFeed');
        return store.dispatch(fetchCompanyFeed(true, PER_PAGE, id));
      }

      return null;
    }
  }
];

@asyncConnect(asyncPromises, state => ({ companyProfile: state.companyProfile, companyFeed: state.companyFeed }))
export default class CompanyPage extends Component {

  constructor(props) {
    super(props);

    this.handleBrandSave = this.handleBrandSave.bind(this);
    this.handleDescriptionSave = this.handleDescriptionSave.bind(this);
    this.handleNeedMoreFeed = this.handleNeedMoreFeed.bind(this);
    this.handleSaveAvatar = this.handleSaveAvatar.bind(this);
    this.handleAvatarRemove = this.handleAvatarRemove.bind(this);
    this.renderPersonPage = this.renderPersonPage.bind(this);
    this.renderAvatarModal = this.renderAvatarModal.bind(this);
    this.handleLikeClick = this.handleLikeClick.bind(this);
  }

  componentDidMount() {
    $('#modalAvatar').on('shown.bs.modal', this.initCropper.bind(this)).on('hidden.bs.modal', this.endCrop.bind(this));
    this.props.dispatch(checkIsMyNews(this.props.params.id));
  }


  handleNeedMoreFeed() {
    const id = this.props.params.id;

    this.props.dispatch(fetchCompanyFeed(false, PER_PAGE, id));
  }

  handleInfoClick(event) {
    event.preventDefault();

    const $accordion = $('[data-slideinfo="company-info"]');
    const $item = $accordion.find('.info-slide__item');
    const $content = $accordion.find('.info-slide__content');

    if (!$item.hasClass('active')) {
      $content.slideDown(250, () => {
        $item.addClass('active');
      });
    } else {
      $content.slideUp(250, () => {
        $item.removeClass('active');
      });
    }
  }

  handleBrandSave = (brand) => {
    this.props.dispatch(setNewBrand(brand));
  };

  handleDescriptionSave = (description) => {
    this.props.dispatch(setNewDescription(description));
  };

  handleSaveAvatar() {
    const cropper = this.cropper;
    // const cropBoxData = cropper.getCropBoxData();
    // const canvasData = cropper.getCanvasData();
    const dispatch = this.props.dispatch;

    cropper.getCroppedCanvas().toBlob((blob) => {
      const formData = new FormData();

      formData.append('avatar', blob, `${Math.random().toString(36).slice(2)}.jpg`);
      formData.append('crop', JSON.stringify({}));

      // Use `jQuery.ajax` method
      $.ajax(`${pathAPI}/users/avatar`, {
        method: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        headers: { 'Authorization': getToken() },
        success: function ok(data) {
          dispatch(setNewAvatar(data.avatar));
          dispatch(changeAvatarInFeed(data.avatar));
          $('#modalAvatar').modal('hide');
        },
        error: function error(xhr) {
          $('#modalAvatar').find('#modalCroppperError').text(xhr.responseJSON.message);
        }
      });
    });
  }

  handleAvatarClick() {
    $('#avatarFile').trigger('click');
  }

  handleAvatarChange(e) {
    const image = document.getElementById('cropAvatar');

    image.src = window.URL.createObjectURL(e.target.files[0]);
    $('#modalAvatar').modal('show');
    // Fix for: If user pick the same file onChange will not trigger.
    /* eslint-disable no-param-reassign */
    e.target.value = null; // eslint-disable-line no-param-reassign
  }

  handleAvatarRemove() {
    this.props.dispatch(removeAvatar());
  }

  handleLikeClick(id) {
    this.props.dispatch(likePost(id));
  }

  getAnotherPhones(phones) {
    const result = [];

    phones.forEach((value, index) => {
      if (index !== 0) {
        result.push(
          <tr key={`phones${index}`}>
            <td />
            <td>{value}</td>
          </tr>
        );
      }
    });

    return result;
  }

  getAdditionalOkvd(okvds) {
    const result = [];

    okvds.forEach((value, index) => {
      result.push(
        <div key={`okvd${index}`}>{value}</div>
      );
    });

    return result;
  }

  getPartnersList(partners) {
    const result = [];

    partners.forEach((value, index) => {
      result.push(
        <div className='flex-col flex-col--lg4 flex-col--xs6' key={`partner${index}`}>
          <div className='uou-block-6a card'>
            <div className='card__image'>
              <img src={value.avatar} alt=''/>
            </div>
            <h6 className='uou-block-6a__header card__header'>{value.companyData.legalName}</h6>
          </div>
        </div>
      );
    });

    return result;
  }

  getProjectsList(projects) {
    const result = [];

    projects.forEach((value, index) => {
      result.push(
        <div className='flex-col flex-col--xs4 flex-col--md4' key={`project${index}`}>
          <a href='#' className='uou-block-7g card flex-column'>
            <div className='card__image img-ratio img-ratio--16-9'>
              <span className='img-ratio__spacer'/>
              <img className='img-ratio__image' src={value.image} alt=''/>
            </div>
            <div className='content flex-grow'>
              <h6 className='uou-block-7g__header card__header'>
                {value.name}</h6>
            </div>
          </a>
        </div>
      );
    });

    return result;
  }

  getFirstPhone(phones) {
    return phones[0];
  }

  initCropper() {
    const image = document.getElementById('cropAvatar');
    const cropBoxData = { width: 400, height: 200, x: 0, y: 0 };

    this.cropper = new Cropper(image, {
      viewMode: 1,
      background: false,
      zoomable: false,
      movable: false,
      ready: function ready() {
        // Strict mode: set crop box data first
        // this.cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
        const data = this.cropper.getImageData();

        if (data.naturalWidth < 400 || data.naturalHeight < 200) {
          /* eslint-disable no-alert */
          alert('Изображение менее 400 на 200.');
          /* eslint-enable no-alert */
          $('#modalAvatar').modal('hide');
          return;
        }
        this.cropper.setData(cropBoxData);
      },
      cropend: function cropend() {
        const data = this.cropper.getData(true);

        let width = data.width;
        let height = data.height;
        let isBreakLimitations = false;

        if (height > 1600) {
          height = 1600;
          isBreakLimitations = true;
        }

        if (width > 1600) {
          width = 1600;
          isBreakLimitations = true;
        }

        if (height < 200) {
          height = 200;
          isBreakLimitations = true;
        }

        if (width < 400) {
          width = 400;
          isBreakLimitations = true;
        }

        if (isBreakLimitations) this.cropper.setData({ width, height });
      }
    });
  }

  endCrop() {
    this.cropper.destroy();
    $('#modalAvatar').find('#modalCroppperError').text('');
  }

  isMyCompany() {
    return this.props.params.id === getUserId();
  }

  additionalMenu() {
    return (
      <div>
        <button type='button' className='btn btn-transparent-primary mb15 width-100proc' data-dismiss='modal'>
          Написать сообщение
        </button>
        <div className='custom-select-box'>
          <select name='additional' id='selectbox'>
            <option value='0'>Дополнительно</option>
            <option value='1'>Добавить в партнеры</option>
            <option value='2'>Подписаться на новости</option>
            <option value='3'>Добавить в черный список</option>
            <option value='4'>Подать жалобу</option>
          </select>
        </div>
      </div>
    );
  }

  editButtonsAvatar(isDefaultAvatar) {
    return (
      <div className='mt10'>
        <i
          className='fa fa-pencil fontIconSize20 clickable'
          aria-hidden='true'
          onClick={this.handleAvatarClick}
        />
        {!isDefaultAvatar ?
          <i
            className='fa fa-times fontIconSize20 ml15 clickable'
            aria-hidden='true'
            onClick={this.handleAvatarRemove}
          /> : ''}

      </div>
    );
  }

  renderPersonPage(user) {
    const fullName = `${user.personData.lastName} ${user.personData.firstName} ${user.personData.middleName || ''}`;

    return (
      <div className='compny-profile'>
        <div className='profile-company-content'>
          <div className='container'>
            <div className='row'>
              { /* <!-- SIDEBAR -->*/ }
              <div className='col-md-4 col-md-push-8'>
                { /* Full name on mobile */ }
                <div className='profile-block is-hidden-md-up'>
                  <div className='profile-in'>
                    <h5>{fullName}</h5>
                    <EditableInput
                      text={user.description}
                      emptyText='Описание не указано'
                      onChange={this.handleDescriptionSave}
                      isTextArea
                      fieldName='description'
                      readOnly={!this.isMyCompany()}
                    />
                  </div>
                </div>
                { /* Full name END */ }
                { /* <!-- Company Information --> */ }
                <div className='sidebar profile-block text-center'>
                  {this.renderAvatar(user.avatar, true)}
                  <div className='sidebar-information'>
                    <table className='contacts-table'>
                      <tbody>
                        <tr>
                          <th>Телефон:</th>
                          <td>{this.getFirstPhone(user.phones)}</td>
                        </tr>

                        {this.getAnotherPhones(user.phones)}
                        <tr>
                          <th>E-mail:</th>
                          <td>{user.email}</td>
                        </tr>
                        <tr>
                          <th>Сайт:</th>
                          <td>{user.personData.website || 'не указан'}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <button type='button' className='btn btn-transparent-primary mb15 width-100proc' data-dismiss='modal'>
                    Подать жалобу
                  </button>
                </div>
              </div>

              { /* <!-- Content --> */ }
              <div className='col-md-8 col-md-pull-4'>
                <div className='tab-content'>
                  <div id='profile' className='tab-pane fade in active'>

                    { /* About */ }
                    <div className='profile-block'>
                      <div className='profile-in'>
                        <div className='is-hidden-md-down'>
                          <h5>{fullName}</h5>
                          <EditableInput
                            text={user.description}
                            emptyText='Описание не указано'
                            onChange={this.handleDescriptionSave}
                            isTextArea
                            fieldName='description'
                            readOnly={!this.isMyCompany()}
                          />
                        </div>
                        <table className='contacts-table width-auto'>
                          <tbody>
                            <tr>
                              <th>Компания:</th>
                              <td>{user.personData.companyName}</td>
                            </tr>
                            <tr>
                              <th>Должность:</th>
                              <td>{user.personData.post}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    { /* About END */ }

                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
        {this.isMyCompany() && this.renderAvatarModal()}
      </div>
    );
  }

  renderAvatar(avatar, isCircle) {
    let divClassName = 'img-profile mt25 mb15';
    let imgClassName = 'img-profile__image media-object';

    if (isCircle) {
      divClassName = 'img-profile mt25 mb15 img-rounded img-rounded--equal width-80proc';
      imgClassName = 'img-profile__image media-object img-rounded__image';
    }

    return (
      <div>
        <div className={divClassName}>
          <input
            id='avatarFile'
            type='file'
            className='visuallyhidden'
            accept='image/*'
            onChange={this.handleAvatarChange}
          />
          <img
            id='avatarImg'
            className={imgClassName}
            src={getAvatarUrl(avatar, 'normal')}
          />
        </div>
        {this.isMyCompany() && this.editButtonsAvatar(isDefaultUserAvatar(avatar))}
      </div>
    );
  }

  renderAvatarModal() {
    return (
      <div
        className='modal fade cropper-avatar'
        id='modalAvatar'
        role='dialog'
        aria-labelledby='modalLabel'
        tabIndex='-1'
      >
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='modalLabel'>Выбор аватара</h5>
              <button type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Закрыть'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <div className='img-container'>
                <p id='modalCroppperError' className='error' />
                <img id='cropAvatar' src='' alt=''/>
              </div>
            </div>
            <div className='modal-footer'>
              <button type='button' className='btn btn-default' data-dismiss='modal'>Отмена</button>
              <button type='button' className='btn btn-transparent-primary' onClick={this.handleSaveAvatar}>Сохранить</button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const user = this.props.companyProfile.profile.data.user;
    const companyData = this.props.companyProfile.profile.data.user.companyData;
    const projects = this.props.companyProfile.profile.data.projects;
    const partners = this.props.companyProfile.profile.data.partners;

    // If user is a person, not a company or entrepreneur
    if (user.type === 2) {
      return this.renderPersonPage(user);
    }

    const hideBrand = !this.isMyCompany() && !companyData.brand;

    return (
      <div>
        <div className='compny-profile'>
          <div className='profile-company-content' data-bg-color='f5f5f5'>
            <div className='container'>
              <div className='row'>
                { /* <!-- SIDEBAR -->*/ }
                <div className='col-md-4 col-md-push-8'>
                  { /* Brand (show only on mobile) */ }
                  <div className='profile-block pb0 is-hidden-md-up'>
                    <div className='profile-in'>
                      <h5>
                        {!hideBrand &&
                        <EditableInput
                          text={companyData.brand}
                          emptyText='Укажите бренд'
                          onChange={this.handleBrandSave}
                          isTextArea={false}
                          fieldName='brand'
                          readOnly={!this.isMyCompany()}
                        />}
                      </h5>
                    </div>
                  </div>
                  { /* Brand END */ }
                  { /* <!-- Company Information --> */ }
                  <div className='sidebar profile-block'>
                    {this.renderAvatar(user.avatar, false)}
                    <div className='sidebar-information'>
                      <table className='contacts-table'>
                        <tbody>
                          <tr>
                            <th>Руководитель:</th>
                            <td>{companyData.director || 'не указан'}
                              <br /> {companyData.directorPost || 'должность не указана'}
                            </td>
                          </tr>

                          <tr>
                            <th>Телефон:</th>
                            <td>{this.getFirstPhone(user.phones)}</td>
                          </tr>

                          {this.getAnotherPhones(user.phones)}

                          <tr>
                            <th>E-mail:</th>
                            <td>{user.email}</td>
                          </tr>
                          <tr>
                            <th>Сайт:</th>
                            <td>{'не указан' || companyData.website}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    {!this.isMyCompany() && this.additionalMenu()}
                  </div>
                </div>

                { /* <!-- Content --> */ }
                <div className='col-md-8 col-md-pull-4'>
                  <div className='tab-content'>
                    <div id='profile' className='tab-pane fade in active'>
                      { /* About */ }
                      <div className='profile-block'>
                        <div className='profile-in'>
                          <h5 className='is-hidden-md-down'>
                            {!hideBrand &&
                            <EditableInput
                              text={companyData.brand}
                              emptyText='Укажите бренд'
                              onChange={this.handleBrandSave}
                              isTextArea={false}
                              fieldName='brand'
                              readOnly={!this.isMyCompany()}
                            />}
                          </h5>
                          <h5>{companyData.legalName || 'Не указано юридическое лицо'}</h5>
                          <EditableInput
                            text={user.description}
                            emptyText='Описание не указано'
                            onChange={this.handleDescriptionSave}
                            isTextArea
                            fieldName='description'
                            readOnly={!this.isMyCompany()}
                          />

                          <h6 className='info-slide__caption' onClick={this.handleInfoClick}>Реквизиты</h6>
                          <div className='info-slide'  data-slideinfo='company-info'>
                            <div className='info-slide__item'>
                              <div className='info-slide__content'>
                                <ul className='list-unstyled'>
                                  <li><strong>ИНН: </strong>{companyData.inn || 'не указан'}</li>
                                  <li><strong>КПП: </strong>{companyData.kpp || 'не указан'}</li>
                                  <li><strong>ОГРН: </strong>{companyData.ogrn || 'не указан'}</li>
                                  <li><strong>ОКВЭД: </strong>
                                    {companyData.okvd || 'Основной ОКВЭД не указан'}
                                    {this.getAdditionalOkvd(companyData.okvds)}</li>
                                </ul>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      { /* About END */ }

                      { /* Partners */ }
                      <div className='profile-block profile-block--no-pb'>
                        <div className='flex flex-between flex-vertical-center'>
                          <a className='btn btn-link btn-header'>
                            {this.isMyCompany() && <h3 className='btn-header__caption'>Мои партнеры</h3>}
                            {!this.isMyCompany() && <h3 className='btn-header__caption'>Партнеры</h3>}
                            <span className='btn-header__counter'>{user.partners || '0'}</span>
                          </a>
                        </div>
                        <section className='profile-in pro-mem'>
                          <div className='flex-grid'>
                            {this.getPartnersList(partners)}
                          </div>
                        </section>
                      </div>
                      {/* Partners END */}

                      {/* Projects */}
                      <div className='profile-block profile-block--no-pb'>
                        <div className='flex flex-between flex-vertical-center'>
                          <a className='btn btn-link btn-header'>
                            <h3 className='btn-header__caption'>Портфолио</h3>
                            <span className='btn-header__counter'>{user.projects || '0'}</span>
                          </a>
                        </div>
                        <div className='profile-in'>
                          <div className='flex-grid'>
                            {this.getProjectsList(projects)}
                          </div>
                        </div>
                      </div>
                      {/* Projects END */}
                      <CompanyFeed
                        title={'Новости'}
                        isReadOnly={!this.isMyCompany()}
                        isLoaded={this.props.companyFeed.loaded}
                        companyFeed={this.props.companyFeed.data}
                        onHandleNeedMore={this.handleNeedMoreFeed}
                        onLikeClick={this.handleLikeClick}
                        isMyCompany={this.isMyCompany()}
                        userId={getUserId()}
                      />
                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>

        {this.isMyCompany() && this.renderAvatarModal()}

      </div>
    );
  }
}

const PER_PAGE = 2;

const defaultProps = {
  params: {},
  companyProfile: {},
  companyFeed: { data:[] },
  dispatch: () => {
  }
};

const propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.object,
  companyProfile: PropTypes.object,
  companyFeed: PropTypes.object
};

CompanyPage.propTypes = propTypes;
CompanyPage.defaultProps = defaultProps;
