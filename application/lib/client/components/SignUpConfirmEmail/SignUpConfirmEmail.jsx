import React, { Component, PropTypes }  from 'react';
import fetch from 'isomorphic-fetch';
import pathAPI from '../../redux/pathAPI';
import { browserHistory } from 'react-router';
import { setToken, setUserId } from '../../redux/utils/helpers';

const defaultProps = {
  dispatch: () => {
  }
};

const propTypes = {
  params: PropTypes.object
};

export default class SignUpConfirmEmail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isConfirmed: false,
      isError: false
    };
  }

  componentDidMount() {
    const token = this.props.params.token;
    const apiIn = { token };
    let status = 0;

    fetch(`${pathAPI}/auth/confirm`, {
      method: 'PUT',
      headers: new Headers({ 'content-type': 'application/json' }),
      body: JSON.stringify(apiIn),
      credentials: 'same-origin'
    }).then(response => {
      status = response.status;
      return response.json();
    }).then(json => {
      if (status === 200) {
        setToken(json.token);
        setUserId(json.id);
        browserHistory.push(`/id${json.id}`);
      } else {
        this.setState({ isError: true });
      }
    });
  }

  render() {
    let text = '';

    if (this.state.isConfirmed) {
      text = <p>Вы успешно подтвердили свою учётную запись.</p>;
    }

    if (this.state.isError) {
      text = <p>Ошибка при подтверждении учётной записи.</p>;
    }

    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm-8 col-sm-offset-2'>
            <div className='panel panel--white mtb30'>
              <h1>Подтверждение e-mail</h1>
              {text}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SignUpConfirmEmail.propTypes = propTypes;
SignUpConfirmEmail.defaultProps = defaultProps;
