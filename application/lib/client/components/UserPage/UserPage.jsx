import React, { Component, PropTypes }  from 'react';
import { fetchCreateNews }              from '../../redux/actions/newsActions';

const defaultProps = {
  dispatch: () => {}
};

const propTypes = {
  dispatch: PropTypes.func
};

export default class UserPage extends Component {

  constructor(props) {
    super(props);
    this.handleCreateNews = this.handleCreateNews.bind(this);
  }

  componentDidMount() {

  }

  handleCreateNews(event) {
    event.preventDefault();
    this.props.dispatch(fetchCreateNews());
  }

  render() {
    return (
      <div>
        <div className='compny-profile'>
          <div className='profile-company-content' data-bg-color='f5f5f5'>
            <div className='container'>
              <div className='row'>
                { /* <!-- SIDEBAR -->*/ }
                <div className='col-md-4 col-md-push-8'>
                  { /* <!-- Company Information --> */ }
                  <div className='sidebar profile-block'>
                    <div className='img-profile mt25' style={{ marginBottom: '15px' }}>
                      <img className='media-object' src='/images/news3.png' alt=''/>
                    </div>
                    <div className='sidebar-information'>
                      <table className='contacts-table'>
                        <tr>
                          <th>Телефон:</th>
                          <td>8-800-382-33-23 (бухгалтерия)</td>
                        </tr>
                        <tr>
                          <td />
                          <td>8-800-382-33-23 (отдел продаж)</td>
                        </tr>
                        <tr>
                          <td />
                          <td>8-800-382-33-23 (отдел маркетинга)</td>
                        </tr>
                        <tr>
                          <th>E-mail:</th>
                          <td>pochta@mail.ru</td>
                        </tr>
                        <tr>
                          <th>Сайт:</th>
                          <td>website.ru</td>
                        </tr>
                      </table>
                    </div>
                  </div>

                  <div className='sidebar'>
                    <h5 className='main-title'>Рекомендованные партнеры</h5>
                    <div className='uou-block-4e'>
                      <ul className='photos-list'>
                        <li className='card'>
                          <div className='card__image'>
                            <img src='images/photostream4.jpg' alt=''/>
                          </div>
                        </li>
                        <li className='card'>
                          <div className='card__image'>
                            <img src='images/photostream4.jpg' alt=''/>
                          </div>
                        </li>
                        <li className='card'>
                          <div className='card__image'>
                            <img src='images/photostream4.jpg' alt=''/>
                          </div>
                        </li>
                        <li className='card'>
                          <div className='card__image'>
                            <img src='images/photostream4.jpg' alt=''/>
                          </div>
                        </li>
                        <li className='card'>
                          <div className='card__image'>
                            <img src='images/photostream4.jpg' alt=''/>
                          </div>
                        </li>
                        <li className='card'>
                          <div className='card__image'>
                            <img src='images/photostream4.jpg' alt=''/>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>

                { /* <!-- Content --> */ }
                <div className='col-md-8 col-md-pull-4'>
                  <div className='tab-content'>
                    <div id='profile' className='tab-pane fade in active'>

                      { /* About */ }
                      <div className='profile-block profile-block--border-bottom'>
                        <div className='profile-in'>
                          <h5>ГосТяжМаш</h5>
                          <h5>OOO 'ТяжМаш Сибирь'</h5>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                            Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                            Proin sodales pulvinar tempor.
                            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Proin sodales pulvinar tempor.
                            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

                          <h6 className='mb0'>
                            <a className='btn btn-link btn-simple' data-toggle='modal' data-target='#requisites'>
                              Реквизиты</a></h6>

                          <div
                            className='modal fade'
                            id='requisites' tabIndex='-1'
                            role='dialog'
                            aria-labelledby='myModalLabel'
                          >
                            <div className='modal-dialog' role='document'>
                              <div className='modal-content'>
                                <div className='modal-header'>
                                  <button
                                    type='button'
                                    className='close'
                                    data-dismiss='modal'
                                    aria-label='Close'
                                  >
                                    <span aria-hidden='true'>&times;
                                    </span>
                                  </button>
                                  <h4 className='modal-title' id='myModalLabel'>Реквизиты</h4>
                                </div>
                                <div className='modal-body'>
                                  <ul className='list-unstyled'>
                                    <li><strong>ИНН:</strong> 84248248204294209</li>
                                    <li><strong>КПП:</strong> 84224290</li>
                                    <li><strong>ОГРН:</strong> 0к2338109</li>
                                    <li><strong>ОКВЭД:</strong> Деятельность, связанная со строительством</li>
                                  </ul>
                                </div>
                                <div className='modal-footer'>
                                  <button type='button' className='btn btn-default' data-dismiss='modal'>Закрыть
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      { /* About END */ }

                      { /* Partners */ }
                      <div className='profile-block profile-block--border-bottom'>
                        <div className='flex flex-between flex-vertical-center'>
                          <a className='btn btn-link btn-header'>
                            <h3 className='btn-header__caption'>Мои партнеры</h3>
                            <span className='btn-header__counter'>8</span>
                          </a>
                        </div>
                        <section className='profile-in pro-mem'>
                          <div className='row'>
                            <div className='col-sm-3'>
                              <div className='uou-block-6a card'>
                                <div className='card__image'>
                                  <img src='images/member-1.png' alt=''/>
                                </div>
                                <h6 className='uou-block-6a__header card__header'>ООО Малинка </h6>
                              </div>
                            </div>
                            <div className='col-sm-3'>
                              <div className='uou-block-6a card'>
                                <div className='card__image'>
                                  <img src='images/member-1.png' alt=''/>
                                </div>
                                <h6 className='uou-block-6a__header card__header'>ЗАО Богатырь </h6>
                              </div>
                            </div>
                            <div className='col-sm-3'>
                              <div className='uou-block-6a card'>
                                <div className='card__image'>
                                  <img src='images/member-1.png' alt=''/>
                                </div>
                                <h6 className='uou-block-6a__header card__header'>OOO Инвест</h6>
                              </div>
                            </div>
                            <div className='col-sm-3'>
                              <div className='uou-block-6a card'>
                                <div className='card__image'>
                                  <img src='images/member-1.png' alt=''/>
                                </div>
                                <h6 className='uou-block-6a__header card__header'>ИП Карпов </h6>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                      {/* Partners END */}

                      {/* Projects */}
                      <div className='profile-block profile-block--border-bottom'>
                        <div className='flex flex-between flex-vertical-center'>
                          <a className='btn btn-link btn-header'>
                            <h3 className='btn-header__caption'>Проекты</h3>
                            <span className='btn-header__counter'>7</span>
                          </a>
                        </div>
                        <div className='profile-in'>
                          <div className='row'>
                            <div className='col-sm-4'>
                              <article className='uou-block-7g card'>
                                <div className='card__image'>
                                  <img className='image' src='images/member-1.png' alt=''/>
                                </div>
                                <div className='content'>
                                  <h6 className='uou-block-7g__header card__header'>
                                    Колеса для комбайнов для ООО 'Комбайнеры'</h6>
                                </div>
                              </article>
                            </div>
                            <div className='col-sm-4'>
                              <article className='uou-block-7g card'>
                                <div className='card__image'>
                                  <img className='image' src='images/member-1.png' alt=''/>
                                </div>
                                <div className='content'>
                                  <h6 className='uou-block-7g__header card__header'>
                                    Комплектующие для строительной техники</h6>
                                </div>
                              </article>
                            </div>
                            <div className='col-sm-4'>
                              <article className='uou-block-7g card'>
                                <div className='card__image'>
                                  <img className='image' src='images/member-1.png' alt=''/>
                                </div>
                                <div className='content'>
                                  <h6 className='uou-block-7g__header card__header'>
                                    Поставка деталей для вагонов по заказу РЖД</h6>
                                </div>
                              </article>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* Projects END */}

                      {/* Feed */}
                      <div className='profile-block'>
                        <h3>Моя лента</h3>
                        <button
                          className='btn btn-transparent-primary'
                          onClick={this.handleCreateNews}
                        >
                          Добавить новость
                        </button>
                        <div className='profile-in profile-serv'>

                          <article className='uou-block-7f blog-post-content feed'>
                            <div className='feed__header feed-header'>
                              <div className='feed-header__image'>
                                <div className='icon'>
                                  <img src='images/photostream3.jpg' alt=''/>
                                </div>
                              </div>
                              <div className='feed-header__body'>
                                <a href='#' className='feed-header__caption'>
                                  Мы получили награду на выставке КрасЭкспо 2016</a>
                                <a href='#' className='feed-header__subheader'>OOO 'Съем Слона'</a>
                                <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                              </div>
                            </div>

                            <img src='images/news5.png' alt='' style={{ marginBottom:'15px' }}/>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                              Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.
                              Cras vel lorem. Etiam pellentesque aliquet tellus.</p>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                              Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.
                              Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.
                            Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.
                            Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.</p>

                            <ul className='social-icons text-left list-unstyled' style={{ marginBottom:'0' }}>
                              <li>
                                <a href='#'>
                                  <i className='fa fa-star' style={{ marginRight: '10px' }} />Нравится
                                </a>
                                <span>21</span>
                              </li>
                            </ul>
                          </article>

                          <article className='uou-block-7f blog-post-content feed'>

                            <div className='feed__header feed-header'>
                              <div className='feed-header__image'>
                                <div className='icon'>
                                  <img src='images/photostream3.jpg' alt=''/>
                                </div>
                              </div>
                              <div className='feed-header__body'>
                                <h6 className='feed-header__caption'>Сохраненная запись</h6>
                                <a href='#' className='feed-header__subheader'>OOO 'Съем Слона'</a>
                                <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                              </div>
                            </div>

                            <div className='uou-block-7f blog-post-content feed--is-nested'>
                              <div className='feed__header feed-header'>
                                <div className='feed-header__image'>
                                  <div className='icon'>
                                    <img src='images/photostream3.jpg' alt=''/>
                                  </div>
                                </div>
                                <div className='feed-header__body'>
                                  <a href='#' className='feed-header__caption'>
                                    Мы получили награду на выставке КрасЭкспо 2016</a>
                                  <a href='#' className='feed-header__subheader'>OOO 'Съем Слона'</a>
                                  <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                                </div>
                              </div>

                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.
                                Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.
                              Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.
                              Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.</p>

                            </div>
                          </article>

                          <article className='uou-block-7f blog-post-content feed'>
                            <div className='feed__header feed-header'>
                              <div className='feed-header__image'>
                                <div className='icon'>
                                  <img src='images/photostream3.jpg' alt=''/>
                                </div>
                              </div>
                              <div className='feed-header__body'>
                                <a href='#' className='feed-header__caption'>
                                  Мы получили награду на выставке КрасЭкспо 2016
                                </a>
                                <a className='feed-header__subheader'>OOO 'Съем Слона'</a>
                                <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                              </div>
                            </div>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                              Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.
                              Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.
                            Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.
                            Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.</p>

                            <ul className='social-icons text-left list-unstyled' style={{ marginBottom: '0' }}>
                              <li><a href='#'><i className='fa fa-star' style={{ marginRight:'10px' }} />Нравится </a>
                                <span>42</span> </li>
                            </ul>
                          </article>

                        </div>
                      </div>
                      {/* Feed END */}

                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UserPage.propTypes = propTypes;
UserPage.defaultProps = defaultProps;
