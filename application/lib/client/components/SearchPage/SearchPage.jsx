import React, { Component, PropTypes }  from 'react';

const defaultProps = {
  dispatch: () => {}
};

const propTypes = {
  dispatch: PropTypes.func
};

export default class SearchPage extends Component {

  renderSearch() {
    return (
      <div className='flex-grid mt20'>
        <div className='flex-col flex-col--sm5'>
          <input
            className='mb0 mb15-sm'
            type='text'
            required
            placeholder='Поиск'
          />
        </div>
        <div className='flex-col flex-col--sm5 custom-select-box'>
          <select className='mb15-sm' name='name' id='selectbox'>
            <option value='0'>Во всех</option>
            <option value='1'>Не во всех</option>
            <option value='2'>Не искать</option>
          </select>
        </div>
        <div className='flex-col flex-col--xs6 flex-col--sm2 flex-to-right--sm'>
          <button
            className='btn btn-transparent-primary'
          >Найти</button>
        </div>
      </div>
    );
  }

  /* eslint-disable max-len */
  render() {
    return (
      <div className='container mt20'>
        <div className='profile-block'>
          {this.renderSearch()}
        </div>

        <div className='profile-block pt20'>
          <div className='card-row'>
            <div className='card-row__image'>
              <img src='/api/images/58e359b43c6b35001575ed9c' alt='' />
            </div>
            <div className='card-row__content'>
              <h2 className='card-row__header'>ОАО "СибТехноСервис"</h2>
              <h3 className='card-row__subheader'>Директор: Иванов Артемий Трофимович</h3>
              <div className='card-row__text'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra
                justo commodo
              </div>
            </div>
            <div className='card-row__controls'>
              <button
                className='btn btn-transparent-primary mb5'
              >Подписаться</button>
              <button
                className='btn btn-transparent-primary'
              >Добавить в партнеры</button>
            </div>
          </div>
          <div className='card-row'>
            <div className='card-row__image'>
              <img src='/api/images/58e359b43c6b35001575ed9c' alt='' />
            </div>
            <div className='card-row__content'>
              <h2 className='card-row__header'>ОАО "СибТехноСервис"</h2>
              <h3 className='card-row__subheader'>Директор: Иванов Артемий Трофимович</h3>
              <div className='card-row__text'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                Proin gravida dolor sit amet lacus accumsan et viverra justo commodo
              </div>
            </div>
            <div className='card-row__controls'>
              <button
                className='btn btn-transparent-primary mb5'
              >Подписаться</button>
              <button
                className='btn btn-transparent-primary'
              >Добавить в партнеры</button>
            </div>
          </div>
          <div className='card-row'>
            <div className='card-row__image'>
              <img src='/api/images/58e359b43c6b35001575ed9c' alt='' />
            </div>
            <div className='card-row__content'>
              <h2 className='card-row__header'>ОАО "СибТехноСервис"</h2>
              <h3 className='card-row__subheader'>Директор: Иванов Артемий Трофимович</h3>
              <div className='card-row__text'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra
                justo commodo
              </div>
            </div>
            <div className='card-row__controls'>
              <button
                className='btn btn-transparent-primary mb5'
              >Подписаться</button>
              <button
                className='btn btn-transparent-primary'
              >Добавить в партнеры</button>
            </div>
          </div>
          <div className='card-row'>
            <div className='card-row__image'>
              <img src='/api/images/58e359b43c6b35001575ed9c' alt='' />
            </div>
            <div className='card-row__content'>
              <h2 className='card-row__header'>ОАО "СибТехноСервис"</h2>
              <h3 className='card-row__subheader'>Директор: Иванов Артемий Трофимович</h3>
              <div className='card-row__text'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus
                accumsan et viverra justo commodo
              </div>
            </div>
            <div className='card-row__controls'>
              <button
                className='btn btn-transparent-primary mb5'
              >Подписаться</button>
              <button
                className='btn btn-transparent-primary'
              >Добавить в партнеры</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

/* eslint-enable max-len */


SearchPage.propTypes = propTypes;
SearchPage.defaultProps = defaultProps;
