import React, { Component, PropTypes }  from 'react';
import PostHeader                       from './PostHeader';
import PostBody                         from './PostBody';
import RePostBody                       from './RePostBody';

const defaultProps = {
  rePostInfo: {},
  isMyCompany: false,
  isReadOnly: false,
  isDetail: false,
  dispatch: () => {}
};

const propTypes = {
  rePostInfo: PropTypes.object,
  isMyCompany: PropTypes.bool,
  isReadOnly: PropTypes.bool,
  isDetail: PropTypes.bool,
  dispatch: PropTypes.func
};

export default class RePost extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const repost = this.props.rePostInfo;
    const headerInfo =  {
      datePublished: repost.datePublished,
      owner: repost.owner,
      title: this.props.isDetail ? repost.text : repost.description,
      newsId: repost._id
    };
    const bodyInfo = {
      images: repost.sourceId.images,
      title: repost.sourceId.title,
      description: this.props.isDetail ? repost.sourceId.text : repost.sourceId.description,
      newsId: repost._id,
      sourceId: repost.sourceId,
      hashtag: repost.sourceId.hashtag,
      isFull: repost.sourceId.isFull,
      isExistFullText: repost.sourceId.isExistFullText
    };

    return (
      <article className='uou-block-7f blog-post-content feed'>
        <PostHeader
          headerInfo={headerInfo}
          isReadOnly={this.props.isReadOnly}
          isDetail={this.props.isDetail}
          isRepost
        >
          {headerInfo.title}
        </PostHeader>
        <RePostBody sourcePost={repost.sourceId}>
          <PostBody
            bodyInfo={bodyInfo}
            isReadOnly={this.props.isReadOnly}
            isMyCompany={this.props.isMyCompany}
            isDetail={this.props.isDetail}
          />
        </RePostBody>
      </article>
    );
  }
}

RePost.propTypes = propTypes;
RePost.defaultProps = defaultProps;
