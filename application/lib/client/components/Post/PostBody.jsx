import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import ImageGallery                     from 'react-image-gallery';

import {
  showMainModal,
  DETAIL_NEWS_MODAL_TYPE
}                                       from '../../redux/actions/modalActions';
import {
  fetchGetNewsFullText
}                                       from '../../redux/actions/companyFeedActions';

const defaultProps = {
  bodyInfo: {
    images: [],
    description: '',
    title:'',
    newsId:'',
    sourceId: null,
    isFull: false,
    isExistFullText: false
  },
  isReadOnly: true,
  isMyCompany: true,
  isDetail: false,
  dispatch: () => {}
};

const propTypes = {
  bodyInfo: PropTypes.object,
  isReadOnly: PropTypes.bool,
  isMyCompany: PropTypes.bool,
  isDetail: PropTypes.bool,
  dispatch: PropTypes.func
};

class PostBody extends Component {

  constructor(props) {
    super(props);

    this.handleOpenDetail = this.handleOpenDetail.bind(this);
    this.handleGetFullText = this.handleGetFullText.bind(this);
  }

  handleOpenDetail(event) {
    event.preventDefault();
    this.props.dispatch(showMainModal(
      { newsId: this.props.bodyInfo.newsId,
        isReadOnly: this.props.isReadOnly,
        isMyCompany: this.props.isMyCompany }, DETAIL_NEWS_MODAL_TYPE));
  }

  handleGetFullText(event) {
    event.preventDefault();
    const id = this.props.bodyInfo.sourceId ? this.props.bodyInfo.sourceId._id : this.props.bodyInfo.newsId;

    this.props.dispatch(fetchGetNewsFullText(id));
  }

  isImagesExist() {
    return this.props.bodyInfo.images && this.props.bodyInfo.images.length > 0;
  }

  isCanGetFullText() {
    return (!this.props.isDetail && !this.props.bodyInfo.isFull && this.props.bodyInfo.isExistFullText);
  }

  isPostHasTags() {
    return this.props.bodyInfo.hashtag;
  }


  renderImages() {
    if (!this.isImagesExist()) return null;
    if (this.props.isDetail) {
      return (
        <ImageGallery
          items={this.props.bodyInfo.images.map((element) => {
            return {
              original: element,
              thumbnail: element
            };
          })}
          showPlayButton={false}
          infinite={false}
          showFullscreenButton={false}
          lazyLoad
          showBullets
          defaultImage={'images/news5.png'}
        />
      );
    }
    return (<img src={this.props.bodyInfo.images[0]} alt='' className='mb15' />);
  }

  renderText() {
    const text = this.props.bodyInfo.description;

    return (
      <div>
        <div className={`${this.props.isDetail ? 'disabled' : 'clickable'}`} onClick={this.handleOpenDetail}>
          <h6 className='feed-header__caption clickable'>{this.props.bodyInfo.title}</h6>
          <p>{text}</p>
        </div>
        {this.renderButtonMore()}
      </div>
    );
  }

  renderTags(tags) {
    return (
      <div>
        {tags.map((value, index) => {
          return (
            <a key={index}>
              #{value}&nbsp;
            </a>
          );
        })}
      </div>
    );
  }

  renderButtonMore() {
    if (this.isCanGetFullText()) {
      return (
        <button onClick={this.handleGetFullText} className='read-more-btn mb10 clickable'>Читать дальше</button>
      );
    }
  }

  render() {
    let tags = '';

    if (this.isPostHasTags()) {
      tags =  this.props.bodyInfo.hashtag.split('#');
      tags = this.renderTags(tags);
    }
    return (
      <div>
        {this.renderImages()}
        {this.renderText()}
        {tags}
      </div>
    );
  }
}

PostBody.propTypes = propTypes;
PostBody.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  return {

  };
}
/* eslint-enable no-unused-vars */

export default connect(mapStateToProps)(PostBody);
