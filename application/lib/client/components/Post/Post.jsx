import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import PostHeader                       from './PostHeader';
import PostBody                         from './PostBody';
import PostAction                       from './PostAction';

const defaultProps = {
  postInfo: {},
  isMyCompany: false,
  isReadOnly: false,
  isDetail: false,
  dispatch: () => {}
};

const propTypes = {
  postInfo: PropTypes.object,
  isMyCompany: PropTypes.bool,
  isReadOnly: PropTypes.bool,
  isDetail: PropTypes.bool,
  dispatch: PropTypes.func
};


class Post extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const post = this.props.postInfo;
    const headerInfo =  {
      datePublished: post.datePublished,
      owner: post.owner,
      newsId: post._id
    };
    const bodyInfo = {
      images: post.images,
      description: this.props.isDetail ? post.text : post.description,
      newsId: post._id,
      isFull: post.isFull,
      isExistFullText: post.isExistFullText,
      hashtag: post.hashtag,
      title: post.title
    };
    const actionInfo = {
      postId: post.id,
      likes: post.likes,
      repostsCount: post.repostsCount,
      reposts: post.reposts,
      likesCount: post.likesCount
    };

    return (
      <article className='uou-block-7f blog-post-content feed'>
        <PostHeader
          headerInfo={headerInfo}
          isReadOnly={this.props.isReadOnly}
          isMyCompany={this.props.isMyCompany}
          isRepost={false}
          isDetail={this.props.isDetail}
        />
        <PostBody
          bodyInfo={bodyInfo}
          isReadOnly={this.props.isReadOnly}
          isMyCompany={this.props.isMyCompany}
          isDetail={this.props.isDetail}
        />
        <PostAction
          actionInfo={actionInfo}
          isMyCompany={this.props.isMyCompany}
          newsId={post._id}
          isDetail={this.props.isDetail}
        />
      </article>
    );
  }
}

Post.propTypes = propTypes;
Post.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  return {

  };
}
/* eslint-enable no-unused-vars */

export default connect(mapStateToProps)(Post);
