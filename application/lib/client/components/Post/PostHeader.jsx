import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import {
  browserHistory,
  Link
}                                       from 'react-router';
import { getAvatarUrl }                 from '../../redux/utils/helpers';
import {
  showMainModal,
  hideMainModal,
  DELETE_NEWS_MODAL_TYPE,
  REPOST_NEWS_MODAL_TYPE,
  DETAIL_NEWS_MODAL_TYPE
}                                       from '../../redux/actions/modalActions';


const defaultProps = {
  headerInfo: {
    datePublished: '',
    owner: null,
    newsId: ''
  },
  isRepost: false,
  isReadOnly: true,
  isMyCompany: true,
  isDetail: false,
  dispatch: () => {}
};

const propTypes = {
  headerInfo: PropTypes.object,
  isRepost: PropTypes.bool,
  isReadOnly: PropTypes.bool,
  isMyCompany: PropTypes.bool,
  isDetail: PropTypes.bool,
  children: PropTypes.node,
  dispatch: PropTypes.func
};

class PostHeader extends Component {

  constructor(props) {
    super(props);
    this.handleOpenDeleteNews = this.handleOpenDeleteNews.bind(this);
    this.handleEditRepost = this.handleEditRepost.bind(this);
    this.handleEditNews = this.handleEditNews.bind(this);
    this.handleOpenDetail = this.handleOpenDetail.bind(this);
  }

  handleOpenDetail(event) {
    event.preventDefault();
    this.props.dispatch(showMainModal(
      { newsId: this.props.headerInfo.newsId,
        isReadOnly: this.props.isReadOnly,
        isMyCompany: this.props.isMyCompany }, DETAIL_NEWS_MODAL_TYPE));
  }

  handleEditNews() {
    this.props.dispatch(hideMainModal());
    browserHistory.push(`/news-edit/${this.props.headerInfo.newsId}`);
  }

  handleEditRepost() {
    this.props.dispatch(showMainModal({
      newsId: this.props.headerInfo.newsId,
      sourceId: this.props.headerInfo.newsId,
      isReadOnly: false,
      isFromModal: this.props.isDetail }, REPOST_NEWS_MODAL_TYPE));
  }

  handleOpenDeleteNews() {
    this.props.dispatch(showMainModal({
      newsId: this.props.headerInfo.newsId,
      isFromModal: this.props.isDetail }, DELETE_NEWS_MODAL_TYPE));
  }

  editButtonsPost() {
    return (
      <div className='flex-align-self-baseline'>
        <i
          className='fa fa-pencil clickable fontIconSize20'
          aria-hidden='true'
          onClick={(event) => {
            event.preventDefault();
            if (this.props.isRepost) {
              this.handleEditRepost();
            } else {
              this.handleEditNews();
            }
          }}
        />
        <i
          className='fa fa-times clickable fontIconSize20 ml15'
          aria-hidden='true'
          onClick={this.handleOpenDeleteNews}
        />
      </div>
    );
  }

  render() {
    const pubDate = new Date(this.props.headerInfo.datePublished).toLocaleString();
    const owner = this.props.headerInfo.owner;

    return (
      <div>
        <div className='feed__header feed-header'>
          <div className='feed-header__image'>
            <div className='icon'>
              <img src={owner && getAvatarUrl(owner.avatar, 'mini')} alt=''/>
            </div>
          </div>
          <div className='feed-header__body'>
            <Link
              className='feed-header__subheader'
              to={owner ? `/id${owner._id}` : '/'}
            >
              {owner && owner.name}
            </Link>
            <time className='feed-header__time' dateTime=''>{pubDate}</time>
          </div>
          <div className='flex-spacer'/>
          {
            !this.props.isReadOnly ? this.editButtonsPost() : ''
          }
        </div>
        {this.props.children}
      </div>
    );
  }
}

PostHeader.propTypes = propTypes;
PostHeader.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  return {

  };
}
/* eslint-enable no-unused-vars */

export default connect(mapStateToProps)(PostHeader);
