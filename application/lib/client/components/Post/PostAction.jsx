import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { getUserId }                    from '../../redux/utils/helpers';
import {
  fetchRepostNews
}                                       from '../../redux/actions/newsActions';
import {
  likePost
}                                       from '../../redux/actions/companyFeedActions';
import PostActionUsersPreview           from './PostActionUsersPreview';


const defaultProps = {
  actionInfo: {
    likes:[],
    likesCount: 0,
    reposts:[],
    repostsCount: 0
  },
  newsId: '',
  isMyCompany: false,
  isDetail: false,
  dispatch: () => {}
};

const propTypes = {
  actionInfo: PropTypes.object,
  userId: PropTypes.string,
  newsId: PropTypes.string,
  isMyCompany: PropTypes.bool,
  isDetail: PropTypes.bool,
  dispatch: PropTypes.func
};

class PostAction extends Component {

  constructor(props) {
    super(props);

    this.isLikedByMyself = this.isLikedByMyself.bind(this);
    this.isRepostedByMyself = this.isRepostedByMyself.bind(this);
    this.handleRepostClick = this.handleRepostClick.bind(this);
    this.handleLikeClick = this.handleLikeClick.bind(this);
  }

  handleRepostClick(event) {
    event.preventDefault();

    this.props.dispatch(fetchRepostNews(this.props.newsId, this.props.isDetail));
  }

  handleLikeClick() {
    event.preventDefault();
    this.props.dispatch(likePost(this.props.newsId));
  }

  isLikedByMyself() {
    const r = this.props.actionInfo.likes.findIndex(element => element._id === getUserId());

    return r !== -1;
  }

  isRepostedByMyself() {
    const r = this.props.actionInfo.reposts.findIndex(element => element._id === getUserId());

    return r !== -1;
  }

  isClickable() {
    return !this.props.isMyCompany && getUserId() !== '';
  }

  renderLike() {
    const starClassName = `${!this.isLikedByMyself() ? 'black-star' : '' } socials-share__icon fa fa-star`;

    return (
      <li className='socials-share__item'>
        <a className={`${this.isClickable()  ? 'clickable' : 'disabled'} socials-share__link`}
          onClick={this.handleLikeClick}
        >
          <i className={starClassName}/>Нравится
        </a>
        <PostActionUsersPreview
          title={`Понравилось ${this.props.actionInfo.likesCount}`}
          previewUsers={this.props.actionInfo.likes}
          popoverId={`likes-post-${this.props.actionInfo.postId}`}
        >
          <span
            className={`${this.props.actionInfo.likes.length > 0 ? 'clickable' : 'disabled'} socials-share__counter`}
          >{this.props.actionInfo.likesCount}</span>
        </PostActionUsersPreview>
      </li>
    );
  }

  renderRepost() {
    return (
      <li className='socials-share__item'>
        <a className={`${this.isClickable() && !this.isRepostedByMyself() ? 'clickable' : 'disabled'} socials-share__link`}
          onClick={this.handleRepostClick}
        >
          <i className='socials-share__icon fa fa-share-alt'/>Репост
        </a>
        <PostActionUsersPreview
          title={`Поделились ${this.props.actionInfo.repostsCount}`}
          previewUsers={this.props.actionInfo.reposts}
          popoverId={`reposts-post-${this.props.actionInfo.postId}`}
        >
          <span
            className={`${this.props.actionInfo.reposts.length > 0 ? 'clickable' : 'disabled'} socials-share__counter`}
          >{this.props.actionInfo.repostsCount}</span>
        </PostActionUsersPreview>
      </li>
    );
  }


  render() {
    return (
      <ul className='socials-share'>
        {this.renderLike()}
        {this.renderRepost()}
      </ul>
    );
  }
}

PostAction.propTypes = propTypes;
PostAction.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  return {

  };
}
/* eslint-enable no-unused-vars */


export default connect(mapStateToProps)(PostAction);
