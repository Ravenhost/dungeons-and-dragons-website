import React, { Component, PropTypes }  from 'react';
import { getAvatarUrl }                 from '../../redux/utils/helpers';

const defaultProps = {
  sourcePost:{},
  dispatch: () => {}
};

const propTypes = {
  sourcePost: PropTypes.object,
  dispatch: PropTypes.func,
  children: PropTypes.node
};

export default class RePostBody extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const sourcePubDate = new Date(this.props.sourcePost.datePublished).toLocaleString();

    return (
      <div className='uou-block-7f blog-post-content feed--is-nested'>
        <div className='feed__header feed-header'>
          <div className='feed-header__image'>
            <div className='icon'>
              <img src={getAvatarUrl(this.props.sourcePost.owner.avatar, 'mini')} alt=''/>
            </div>
          </div>
          <div className='feed-header__body'>
            <a href='#' className='feed-header__subheader'>{this.props.sourcePost.owner.name}</a>
            <time className='feed-header__time' dateTime=''>{sourcePubDate}</time>
          </div>
        </div>
        { this.props.children }
      </div>
    );
  }
}

RePostBody.propTypes = propTypes;
RePostBody.defaultProps = defaultProps;
