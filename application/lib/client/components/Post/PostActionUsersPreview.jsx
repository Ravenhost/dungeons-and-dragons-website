import React, { Component, PropTypes }  from 'react';
import {
  OverlayTrigger,
  Popover
}                                       from 'react-bootstrap';

const defaultProps = {
  title: '',
  previewUsers: [],
  popoverId: '',
  dispatch: () => {}
};

const propTypes = {
  title: PropTypes.string,
  previewUsers: PropTypes.array,
  popoverId: PropTypes.string,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


export default class PostActionUsersPreview extends Component {

  constructor(props) {
    super(props);
  }

  renderElement(user, index) {
    return (
      <div key={index} className='popup-persons'>
        <a className='popup-persons__wrapper-link' href={user._id}>
          <img src={user.avatar} className='popup-persons__avatar'/>
          <div className='popup-persons__info'>
            <div className='popup-persons__name'>{user.name}</div>
            <div className='popup-persons__date'>{user.dateCreated}</div>
          </div>
        </a>
      </div>
    );
  }

  renderOverlayContent() {
    return (
      <Popover
        id={this.props.popoverId}
        title={this.props.title}
      >
        { this.props.previewUsers.map((user, index) => {
          return this.renderElement(user, index);
        })}
      </Popover>
    );
  }

  render() {
    return (
      <OverlayTrigger
        trigger={'click'}
        rootClose
        placement='top'
        overlay={this.renderOverlayContent()}
      >
        {this.props.children}
      </OverlayTrigger>
    );
  }
}

PostActionUsersPreview.propTypes = propTypes;
PostActionUsersPreview.defaultProps = defaultProps;
