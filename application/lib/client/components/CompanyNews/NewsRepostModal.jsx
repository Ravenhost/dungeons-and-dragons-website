import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import {
  fetchChangeNews,
  fetchGetEditedNews
}                                       from '../../redux/actions/newsActions';
import {
  showMainModal,
  DETAIL_NEWS_MODAL_TYPE
}                                       from '../../redux/actions/modalActions';

const defaultProps = {
  newsId: null,
  isFromModal: false,
  isReadOnly: true,
  sourceId: null,
  singleNews: {
    id: null,
    text: '',
    loaded: false,
    loading: false,
    errors: {
      others:[]
    }
  },
  onRequestCloseModal:() => {},
  dispatch: () => {}
};

const propTypes = {
  newsId: PropTypes.string,
  isFromModal: PropTypes.bool,
  isReadOnly: PropTypes.bool,
  sourceId: PropTypes.string,
  singleNews: PropTypes.object,
  onRequestCloseModal:PropTypes.func,
  dispatch: PropTypes.func
};


class NewsRepostModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      text: '',
      newsId: ''
    };


    this.handleRepost = this.handleRepost.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleBack = this.handleBack.bind(this);
  }

  componentDidMount() {
    let newsId = this.props.singleNews._id;

    if (this.props.newsId) {
      newsId = this.props.newsId;
    }
    this.setState({ newsId }, () => {
      this.props.dispatch(fetchGetEditedNews(newsId));
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.newsId === this.state.newsId) {
      this.setState({ text: nextProps.singleNews.text });
    }
  }

  handleRepost() {
    this.props.dispatch(fetchChangeNews({
      id: this.state.newsId,
      text: this.state.text,
      inMaking: false
    }))
      .then(this.handleCloseModal());
  }

  handleCloseModal() {
    this.props.onRequestCloseModal();
  }

  handleBack() {
    this.props.dispatch(showMainModal(
      { newsId: this.props.sourceId,
        isReadOnly: this.props.isReadOnly,
        isMyCompany: false }, DETAIL_NEWS_MODAL_TYPE));
  }

  renderActionButtons() {
    return (
      <div>
        <button
          type='button'
          className={`${this.props.isFromModal ? '' : 'visuallyhidden'} btn btn-default`}
          onClick={this.handleBack}
        >Назад</button>
        <button
          type='button'
          className='btn btn-default'
          onClick={this.handleCloseModal}
        >Закрыть</button>
        <button
          type='button'
          className='btn btn-transparent-primary'
          onClick={this.handleRepost}
        >{ this.props.singleNews.inMaking ? 'Репост' : 'Сохранить'}</button>
      </div>
    );
  }

  renderContent() {
    const newsTitle = this.props.singleNews.title;

    return (
      <div>
        <h4>{newsTitle}</h4>
        <textarea
          value={this.state.text}
          onChange={(event) => {
            this.setState({ text: event.target.value });
          }}
          autoFocus
          required
          placeholder='Ваше дополнение'
          cols='30'
          rows='10'
        />
      </div>
    );
  }

  render() {
    const isLoaded = this.props.singleNews.loaded;

    return (
      <div className='modal-content modal-content-repost'>
        <div className='modal-header'>
          <button type='button' className='close' onClick={this.handleCloseModal}>
            <span aria-hidden='true'>&times;</span>
          </button>
          <h4 className='modal-title'>Репост новости</h4>
        </div>
        <div className='modal-body'>
          {isLoaded ? this.renderContent() : 'Загружаем новость...'}
        </div>
        <div className='modal-footer'>
          {this.renderActionButtons()}
        </div>
      </div>
    );
  }
}

NewsRepostModal.propTypes = propTypes;
NewsRepostModal.defaultProps = defaultProps;

function mapStateToProps(state) {
  const singleNews = state.news.singleNews;

  return {
    singleNews
  };
}

export default connect(mapStateToProps)(NewsRepostModal);
