import React, { Component, PropTypes }  from 'react';
import CompanyFeed                      from '../CompanyPage/CompanyFeed';
import { asyncConnect }                 from 'redux-connect';
import { isBrowser }                    from '../../redux/utils/helpers';
import { fetchCompanyFeed }             from '../../redux/actions/companyFeedActions';

const PER_PAGE = 2;

const defaultProps = {
  companyFeed: { data:[] },
  params: {},
  dispatch: () => {}
};

const propTypes = {
  companyFeed: PropTypes.object,
  params: PropTypes.object,
  dispatch: PropTypes.func
};


const asyncPromises = [

  {
    key: 'feed',
    promise: ({ store, params }) => {
      const id = params.userId;

      if (!isBrowser()) {
        return store.dispatch(fetchCompanyFeed(true, PER_PAGE, id));
      }

      return null;
    }
  }
];

class MyNewsList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      search: '',
      currentSearch: ''
    };

    this.handleNeedMoreFeed = this.handleNeedMoreFeed.bind(this);
    this.handleSearchNews = this.handleSearchNews.bind(this);
  }

  handleNeedMoreFeed() {
    const id = this.props.params.userId;

    this.props.dispatch(fetchCompanyFeed(false, PER_PAGE, id, this.state.currentSearch));
  }

  handleSearchNews() {
    this.setState({
      currentSearch: this.state.search
    }, () => {
      const id = this.props.params.userId;

      this.props.dispatch(fetchCompanyFeed(true, PER_PAGE, id, this.state.currentSearch));
    });
  }

  renderSearch() {
    return (
      <div className='flex flex-vertical-center'>
        <input
          className='mb0'
          type='text'
          required
          placeholder='Поиск'
          value={this.state.search}
          onKeyPress={event => {
            if (event.key === 'Enter') {
              this.handleSearchNews();
            }
          }}
          onChange={(event) => {
            this.setState({ search : event.target.value });
          }}
        />
        <button
          className='btn btn-transparent-primary ml20'
          onClick={this.handleSearchNews}
        >Поиск</button>
      </div>
    );
  }

  render() {
    return (
      <div className='profile-company-content' data-bg-color='f5f5f5'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-8 col-md-offset-2'>
              <div className='tab-content'>
                <div className='tab-pane fade in active'>

                  <CompanyFeed
                    isReadOnly
                    isLoaded={this.props.companyFeed.loaded}
                    title={'Мои новости'}
                    companyFeed={this.props.companyFeed.data}
                    onHandleNeedMore={this.handleNeedMoreFeed}
                  >{this.renderSearch()}</CompanyFeed>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MyNewsList.propTypes = propTypes;
MyNewsList.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    companyFeed: state.companyFeed
  };
}

export default asyncConnect(asyncPromises, mapStateToProps)(MyNewsList);
