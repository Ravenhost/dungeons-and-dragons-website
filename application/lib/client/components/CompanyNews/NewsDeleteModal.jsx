import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { fetchDeleteNews }              from '../../redux/actions/newsActions';
import {
  showMainModal,
  DETAIL_NEWS_MODAL_TYPE
}                                       from '../../redux/actions/modalActions';

const defaultProps = {
  newsId: null,
  isFromModal: false,
  onRequestCloseModal:() => {},
  dispatch: () => {}
};

const propTypes = {
  newsId: PropTypes.string,
  isFromModal: PropTypes.bool,
  onRequestCloseModal:PropTypes.func,
  dispatch: PropTypes.func
};


class NewsDeleteModal extends Component {

  constructor(props) {
    super(props);

    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleDeleteNews = this.handleDeleteNews.bind(this);
    this.handleBack = this.handleBack.bind(this);
  }

  handleCloseModal() {
    this.props.onRequestCloseModal();
  }

  handleDeleteNews() {
    this.props.dispatch(fetchDeleteNews(this.props.newsId))
      .then(() => {
        this.handleCloseModal();
      });
  }

  handleBack() {
    this.props.dispatch(showMainModal(
      { newsId: this.props.newsId,
        isReadOnly: false
      }, DETAIL_NEWS_MODAL_TYPE));
  }

  renderActionButtons() {
    return (
      <div>
        <button
          type='button'
          className={`${this.props.isFromModal ? '' : 'visuallyhidden'} btn btn-default`}
          onClick={this.handleBack}
        >Назад</button>
        <button
          type='button'
          className='btn btn-default'
          onClick={this.handleCloseModal}
        >Отменить</button>
        <button
          type='button'
          className='btn btn-transparent-primary'
          onClick={this.handleDeleteNews}
        >Удалить</button>
      </div>
    );
  }

  render() {
    return (
      <div className='modal-content modal-content-repost'>
        <div className='modal-header'>
          <button type='button' className='close' onClick={this.handleCloseModal}>
            <span aria-hidden='true'>&times;</span>
          </button>
          <h4 className='modal-title'>Удаление новости</h4>
        </div>
        <div className='modal-body'>
          <h6>Вы действительно хотите удалить новость?</h6>
        </div>
        <div className='modal-footer'>
          {this.renderActionButtons()}
        </div>
      </div>
    );
  }
}

NewsDeleteModal.propTypes = propTypes;
NewsDeleteModal.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  return {

  };
}

export default connect(mapStateToProps)(NewsDeleteModal);
