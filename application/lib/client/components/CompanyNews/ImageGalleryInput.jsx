import React, { Component, PropTypes }  from 'react';
import ImageGallery                     from 'react-image-gallery';

const defaultProps = {
  images:[],
  maxImageInput: 10,
  disabled: false,
  onImageError: () => {},
  onAddImage:() => {},
  onDeleteImage:() => {},
  dispatch: () => {}
};

const propTypes = {
  images:PropTypes.array,
  maxImageInput: PropTypes.number,
  disabled: PropTypes.bool,
  onImageError: PropTypes.func,
  onAddImage: PropTypes.func,
  onDeleteImage: PropTypes.func,
  dispatch: PropTypes.func
};

export default class ImageGalleryInput extends Component {

  constructor(props) {
    super(props);

    this.state = { images: [ {
      isAddImage: true,
      original: '/images/download_pic.png',
      thumbnail: '/images/download_pic.png'
    } ] };


    this.renderItem = this.renderItem.bind(this);

    this.handleImageLoad = this.handleImageLoad.bind(this);
    this.handleImageClick = this.handleImageClick.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
    this.handleImageDelete = this.handleImageDelete.bind(this);
  }

  handleImageLoad() {
    const lastIndex = this.props.images.length;

    this.imageGallery.slideToIndex(lastIndex);
  }

  handleImageClick() {
    const currentIndex = this.imageGallery.getCurrentIndex();
    const currentImagesAmount = this.props.images.length;
    const maxImage = this.props.maxImageInput;
    const disabled = this.props.disabled;

    if (currentIndex === currentImagesAmount  && currentImagesAmount < maxImage && !disabled) {
      this.imageInput.click();
    }
  }

  handleImageChange(e) {
    const possibleImagesAmount = this.props.maxImageInput - this.props.images.length;
    const files = Array.prototype.slice.call(e.target.files, 0, possibleImagesAmount);

    /* eslint-disable no-param-reassign */
    e.target.value = null;
    /* eslint-enable no-param-reassign */
    this.props.onAddImage(files);
  }

  handleImageDelete(event) {
    event.preventDefault();
    event.stopPropagation();
    const currentIndex = this.imageGallery.getCurrentIndex();

    this.props.onDeleteImage(currentIndex);
  }


  renderItem(item) {
    const onImageError = this.props.onImageError || this._handleImageError;

    return (
      <div>
        <div className='image-gallery-image'>
          <img
            className='border-solid'
            src={item.original}
            alt={item.originalAlt}
            srcSet={item.srcSet}
            sizes={item.sizes}
            onLoad={this.handleImageLoad}
            onError={onImageError.bind(this)}
          />
          {
            !item.isAddImage &&
            <button
              onClick={this.handleImageDelete}
              className='btn btn-transparent-primary-second  width-auto button-delete-image'
            >
              Удалить
            </button>
          }
          {
            item.description &&
            <span className='image-gallery-description'>
              {item.description}
            </span>
          }
        </div>
      </div>
    );
  }

  render() {
    const currentImagesAmount = this.props.images.length;
    const maxImageAmount = this.props.maxImageInput;

    const uploadImages = currentImagesAmount !== maxImageAmount ? [...this.props.images, ...this.state.images]
      : this.props.images;

    return (
      <div>
        <ImageGallery
          ref={(imageGallery) => {
            this.imageGallery = imageGallery;
          }}
          items={uploadImages}
          showPlayButton={false}
          infinite={false}
          showFullscreenButton={false}
          lazyLoad
          showBullets
          defaultImage={'images/news5.png'}
          onClick={this.handleImageClick}
          renderItem={this.renderItem}
        />

        <input
          ref={(imageInput) => {
            this.imageInput = imageInput;
          }}
          multiple
          className='visuallyhidden'
          type='file'
          onChange={this.handleImageChange}
        />
      </div>
    );
  }
}

ImageGalleryInput.propTypes = propTypes;
ImageGalleryInput.defaultProps = defaultProps;
