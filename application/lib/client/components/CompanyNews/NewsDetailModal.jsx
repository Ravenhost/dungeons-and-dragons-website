import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { fetchGetSingleNews }           from '../../redux/actions/newsActions';
import {
  Post,
  RePost
}                                      from '../Post';

const defaultProps = {
  newsId: null,
  isMyCompany: true,
  isReadOnly: true,
  singleNews: {},
  onRequestCloseModal:() => {},
  dispatch: () => {}
};

const propTypes = {
  newsId: PropTypes.string,
  isMyCompany: PropTypes.bool,
  isReadOnly: PropTypes.bool,
  singleNews: PropTypes.object,
  onRequestCloseModal:PropTypes.func,
  dispatch: PropTypes.func
};


class NewsDetailModal extends Component {

  constructor(props) {
    super(props);

    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(fetchGetSingleNews(this.props.newsId));
  }

  handleCloseModal() {
    this.props.onRequestCloseModal();
  }

  renderNewsFromObject() {
    return (this.props.singleNews.sourceId) ?
      (
        <RePost
          key={this.props.singleNews._id}
          rePostInfo={this.props.singleNews}
          isMyCompany={this.props.isMyCompany}
          isReadOnly={this.props.isReadOnly}
          isDetail
        />
      ) :
      (
        <Post
          key={this.props.singleNews._id}
          postInfo={this.props.singleNews}
          isMyCompany={this.props.isMyCompany}
          isReadOnly={this.props.isReadOnly}
          isDetail
        />
      );
  }

  render() {
    return (
      <div className='modal-content modal-content-repost'>
        <div className='modal-header'>
          <button type='button' className='close' onClick={this.handleCloseModal}>
            <span aria-hidden='true'>&times;</span>
          </button>
          <h4 className='modal-title'>Новость</h4>
        </div>
        <div className='modal-body'>
          { this.props.singleNews.loaded ? this.renderNewsFromObject()
            : <h6>Загружаем новость...</h6> }
        </div>
        <div className='modal-footer'>
          <button type='button' className='btn btn-default' onClick={this.handleCloseModal}>Закрыть</button>
        </div>
      </div>
    );
  }
}

NewsDetailModal.propTypes = propTypes;
NewsDetailModal.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  const singleNews = state.news.singleNews;

  return {
    singleNews
  };
}

export default connect(mapStateToProps)(NewsDetailModal);
