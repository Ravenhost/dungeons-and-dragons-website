import React, { Component, PropTypes }        from 'react';
import { asyncConnect }                       from 'redux-connect';
import { browserHistory }                     from 'react-router';
import { isBrowser, isLoaded }                from '../../redux/utils/helpers';
import { getUserId }                          from '../../redux/utils/helpers';
import ImageGalleryInput                      from './ImageGalleryInput';
import AttachmentInput                        from  './AttachmentInput';
import { WithContext as ReactTags, Keys }     from 'react-tag-input';
import {
  fetchChangeNews,
  fetchAddAttachmentNews,
  fetchDeleteAttachmentNews,
  fetchGetEditedNews
}                                             from '../../redux/actions/newsActions';

const defaultProps = {
  singleNews: {
    _id: null,
    title: '',
    images: [],
    text: '',
    hashtag: '',
    files: [],
    loaded: false,
    loading: false,
    errors: {
      attachments:[],
      images:[],
      others:[]
    }
  },
  params: {},
  dispatch: () => {}
};

const propTypes = {
  singleNews: PropTypes.object,
  params: PropTypes.object,
  dispatch: PropTypes.func
};

class CompanyNewsEdit extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isNewsReceive: false,
      title: '',
      text: '',
      tags: []
    };

    this.handleOnSaveNews = this.handleOnSaveNews.bind(this);
    this.handleOnAddImage = this.handleOnAddImage.bind(this);
    this.handleOnDeleteImageWithIndex = this.handleOnDeleteImageWithIndex.bind(this);
    this.handleOnAddFile = this.handleOnAddFile.bind(this);
    this.handleOnDeleteFileWithIndex = this.handleOnDeleteFileWithIndex.bind(this);
    this.handleBackAction = this.handleBackAction.bind(this);
  }

  componentDidMount() {
    this.setupValidation();
    const newsId = this.props.params.newsId;

    this.props.dispatch(fetchGetEditedNews(newsId));
  }

  componentWillReceiveProps(nextProps) {
    const title = nextProps.singleNews.title === '' ? this.state.title : nextProps.singleNews.title;
    const text = nextProps.singleNews.text === '' ? this.state.text : nextProps.singleNews.text;
    const tags = nextProps.singleNews.hashtag === '' ? this.state.tags
      : nextProps.singleNews.hashtag.split('#').map((element, index) => {
        return {
          id: index,
          text: element
        };
      });

    this.setState({
      title,
      text,
      tags,
      isNewsReceive: true
    }, () => {
      this.setupValidation();
    });
  }

  handleOnAddImage(images) {
    const newsId = this.props.params.newsId;

    this.props.dispatch(fetchAddAttachmentNews(newsId, images, []));
  }

  handleOnDeleteImageWithIndex(index) {
    const type = 'images';
    const name = this.props.singleNews.images[index];

    this.props.dispatch(fetchDeleteAttachmentNews(type, name));
  }

  handleOnAddFile(files) {
    const newsId = this.props.params.newsId;

    this.props.dispatch(fetchAddAttachmentNews(newsId, [], files));
  }

  handleOnDeleteFileWithIndex(index) {
    const type = 'files';
    const name = this.props.singleNews.files[index];

    this.props.dispatch(fetchDeleteAttachmentNews(type, name));
  }

  handleOnSaveNews(event) {
    event.preventDefault();
    if (!$('#editNewsForm').valid()) {
      return;
    }
    if (this.props.singleNews.loading) return;
    this.saveNews();
  }

  handleBackAction(event) {
    event.preventDefault();
    browserHistory.push(`/id${getUserId()}`);
  }

  handleDelete(i) {
    this.setState({
      tags: this.state.tags.filter((tag, index) => index !== i)
    });
  }

  handleAddition(tag) {
    this.setState({
      tags: [
        ...this.state.tags,
        {
          id: this.state.tags.length + 1,
          text: tag.split(' ')[0].slice(0, 15)
        }
      ]
    });
  }

  handleDrag(tag, currPos, newPos) {
    const tags = [ ...this.state.tags ];

    tags.splice(currPos, 1);
    tags.splice(newPos, 0, tag);

    this.setState({ tags });
  }

  setupValidation() {
    $('#editNewsForm').validate({
      rules: {
        newsTitle: {
          required: true,
          maxlength: 150
        },
        newsText: {
          required: true,
          maxlength: 15895
        }
      },
      messages: {
        newsTitle: {
          required: 'Это обязательно поле к заполнению',
          maxlength:  'Заголовок новости не должен превышать 150 символов'
        },
        newsText: {
          required: 'Это обязательно поле к заполнению',
          maxlength:  'Описание новости не должно превышать 15895 символов'
        }
      },
      errorPlacement: function style(label, element) {
        label.css('color', 'red');
        label.insertAfter(element);
      }
    });
  }

  saveNews() {
    const news = this.props.singleNews;
    const hashtag = this.state.tags.map((element) => {
      return element.text;
    }).join('#');

    return this.props.dispatch(fetchChangeNews({
      id: this.props.params.newsId,
      title: this.state.title,
      text:this.state.text,
      files: news.files,
      hashtag,
      images: news.images,
      inMaking: false
    }));
  }

  renderEdit() {
    const news = this.props.singleNews;
    const images = news.images.map((element) => {
      return {
        original: element,
        thumbnail: element
      };
    });
    const title = this.state.title;
    const text  = this.state.text;
    const datePublished = new Date(news.datePublished).toLocaleString();

    return (
      <form name='editNewsForm' id='editNewsForm'>
        <div className='uou-block-7f blog-post-content mt20'>
          <input
            type='text'
            name='newsTitle'
            placeholder='Название проекта или услуги'
            required
            autoFocus
            value={title}
            onKeyPress={(event) => {
              if (event.key === 'Enter') {
                event.preventDefault();
              }
            }}
            onChange={(event) => {
              const newTitle = event.target.value;

              this.setState({ title: newTitle });
            }}
          />
          {
            news.datePublished &&
            <time className='feed-header__time' dateTime=''>{datePublished}</time>
          }
          <ReactTags tags={this.state.tags}
            placeholder='Добавьте тэг для Вашей новости'
            handleDelete={this.handleDelete.bind(this)}
            handleAddition={this.handleAddition.bind(this)}
            handleDrag={this.handleDrag.bind(this)}
            delimiters={[Keys.ENTER, Keys.TAB]}
            maxLength = '4'
          />

          <ImageGalleryInput
            images={images}
            disabled={news.loading}
            onAddImage={this.handleOnAddImage}
            onDeleteImage={this.handleOnDeleteImageWithIndex}
            maxImageInput={10}
          />

          {
            news.errors && news.errors.images && news.errors.images.map((element, index) => {
              return (<span className='error' key={index}>{element}</span>);
            })
          }

          <textarea
            className='height150'
            name='newsText'
            required
            value={text}
            onChange={(event) => {
              const newText = event.target.value;

              this.setState({ text: newText });

              const textarea = event.currentTarget;
              const lines = textarea.value.split('\n').length;

              textarea.style.height = `${140 + lines * 20  }px`;
            }}
            placeholder='Описание проекта или услуги'
            cols='30'
            rows='10'
          />

          <AttachmentInput
            files={news.files}
            disabled={news.loading}
            onAddFile={this.handleOnAddFile}
            onDeleteFile={this.handleOnDeleteFileWithIndex}
            maxFilesInput={10}
          />

          {
            news.errors && news.errors.attachments && news.errors.attachments.map((element, index) => {
              return (<span className='error' key={index}>{element}</span>);
            })
          }

          <div className='text-right flex-column flex-vertical-end'>
            <button
              onClick={this.handleOnSaveNews}
              className='btn btn-transparent-primary'
            >{ news.inMaking ? 'Опубликовать новость' : 'Сохранить редактирование'}
            </button>
            <button
              onClick={this.handleBackAction}
              className='btn btn-transparent-primary mt15'
            >
              { news.inMaking ? 'Отменить публикацию' : 'Отменить редактирование'}
            </button>
          </div>

          {
            news.errors && news.errors.others &&
            (<span className='error'>{news.errors.others}</span>)
          }
        </div>
      </form>
    );
  }

  renderLoad() {
    return (
      <div className='load-news'><p>Загружаем новость...</p></div>
    );
  }

  render() {
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm-6 col-sm-offset-3'>
            <div className='profile-block mt20'>
              <div className={`profile-in profile-serv ${!this.state.isNewsReceive ? 'empty' : ''}`}/>
              {
                this.state.isNewsReceive ? this.renderEdit() : this.renderLoad()
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CompanyNewsEdit.propTypes = propTypes;
CompanyNewsEdit.defaultProps = defaultProps;


function mapStateToProps(state) {
  const singleNews = state.news.singleNews;

  return {
    singleNews
  };
}

const asyncPromises = [
  {
    key: 'new',
    promise: ({ store }) => {
      if (!isBrowser()) {
        const state = store.getState();

        if (!isLoaded(state, 'news')) {
          return null;
        }
        return null;
      }
    }
  }
];

export default asyncConnect(asyncPromises, mapStateToProps)(CompanyNewsEdit);
