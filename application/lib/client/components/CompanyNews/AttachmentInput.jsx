import React, { Component, PropTypes }  from 'react';

const defaultProps = {
  files:[],
  maxFilesInput: 10,
  disabled: false,
  onAddFile:() => {},
  onDeleteFile:() => {},
  dispatch: () => {}
};

const propTypes = {
  files:PropTypes.array,
  maxFilesInput: PropTypes.number,
  disabled: PropTypes.bool,
  onAddFile:PropTypes.func,
  onDeleteFile:PropTypes.func,
  dispatch: PropTypes.func
};

export default class AttachmentInput extends Component {

  constructor(props) {
    super(props);

    this.handleFileChange = this.handleFileChange.bind(this);
    this.renderUploadedFile = this.renderUploadedFile.bind(this);
    this.handleFileDelete = this.handleFileDelete.bind(this);
  }

  handleFileChange(e) {
    const possibleFileAmount = this.props.maxFilesInput - this.props.files.length;
    const files = Array.prototype.slice.call(e.target.files, 0, possibleFileAmount);

    /* eslint-disable no-param-reassign */
    e.target.value = null;
    /* eslint-enable no-param-reassign */
    this.props.onAddFile(files);
  }

  handleFileDelete(event, id) {
    event.preventDefault();

    this.props.onDeleteFile(id);
  }


  renderUploadedFile(file, key) {
    const splitedFile = file.split('/');
    const fileName = splitedFile[splitedFile.length - 1];

    return (
      <div key={key} className='mb15'>
        <div className='flex-inline'>
          <span className='filename mb15'>{fileName}
          </span>
        </div>
        <button className='btn btn-transparent-primary-second  width-auto' onClick={(event) => {
          this.handleFileDelete(event, key);
        }}
        >Удалить</button>
      </div>
    );
  }

  render() {
    const files = this.props.files;
    const disabled = this.props.disabled || files.length === this.props.maxFilesInput;

    return (
      <div>
        <div className='flex flex-vertical-start flex-between mb15'>
          <div className='flex flex-direction-column'>
            {
              files.map((element, index) => {
                return this.renderUploadedFile(element, index);
              })
            }
          </div>
          <label className='btn btn-transparent-primary  width-auto' htmlFor='newsEditFile'>
            Прикрепить файл
          </label>
        </div>
        <input id='newsEditFile'
          className='visuallyhidden'
          type='file'
          multiple
          disabled={disabled}
          onChange={this.handleFileChange}
        />
      </div>
    );
  }
}

AttachmentInput.propTypes = propTypes;
AttachmentInput.defaultProps = defaultProps;
