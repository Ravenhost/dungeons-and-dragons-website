import React, { Component, PropTypes }  from 'react';
import {
  Header,
  Footer,
  MainModal
}                                       from './index';
import { StickyContainer }              from 'react-sticky';


const propTypes = {
  children: PropTypes.node
};

export default class App extends Component {
  render() {
    return (
      <div>
        <StickyContainer>
          <div id='main-wrapper' className=''>

            <Header />

            <div className='full-height-container'>
              {this.props.children}
            </div>

            <Footer />

          </div>

        </StickyContainer>
        <MainModal/>
      </div>
    );
  }
}

App.propTypes = propTypes;
