import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import Modal                            from 'react-modal';
import { hideMainModal }                from '../../redux/actions/modalActions';
import {
  NewsDeleteModal,
  NewsDetailModal,
  NewsRepostModal
}                                       from '../CompanyNews';
import {
  NONE_MODAL_TYPE,
  DELETE_NEWS_MODAL_TYPE,
  REPOST_NEWS_MODAL_TYPE,
  DETAIL_NEWS_MODAL_TYPE
}                                       from '../../redux/actions/modalActions';

const defaultProps = {
  modal: {},
  dispatch: () => {}
};

const propTypes = {
  modal: PropTypes.object,
  dispatch: PropTypes.func
};


class MainModal extends Component {

  constructor(props) {
    super(props);

    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleCloseModal() {
    this.props.dispatch(hideMainModal());
  }

  getStylesForType() {
    const type = this.props.modal.modalType;

    switch (type) {
      case NONE_MODAL_TYPE:
        return {
          content : {
            width: '0px',
            outline: 'none'
          }
        };
      case DELETE_NEWS_MODAL_TYPE:
        return {
          content : {
            width: '400px',
            outline: 'none'
          }
        };
      case REPOST_NEWS_MODAL_TYPE:
        return {
          content : {
            width: '400px',
            outline: 'none'
          }
        };
      case DETAIL_NEWS_MODAL_TYPE:
        return {
          content : {
            width: '800px',
            outline: 'none'
          }
        };
      default:
        return {};
    }
  }

  getModalContentForType() {
    const type = this.props.modal.modalType;

    switch (type) {
      case DELETE_NEWS_MODAL_TYPE:
        return this.renderDeleteNews();
      case REPOST_NEWS_MODAL_TYPE:
        return this.renderRepostNews();
      case DETAIL_NEWS_MODAL_TYPE:
        return this.renderDetailNews();
      case NONE_MODAL_TYPE:
      default:
        return this.renderEmpty();
    }
  }

  renderRepostNews() {
    return (
      <NewsRepostModal
        newsId={this.props.modal.modalProps.newsId}
        onRequestCloseModal={this.handleCloseModal}
        isFromModal={this.props.modal.modalProps.isFromModal}
        sourceId={this.props.modal.modalProps.sourceId}
        isReadOnly={this.props.modal.modalProps.isReadOnly}
      />
    );
  }

  renderDeleteNews() {
    return (
      <NewsDeleteModal
        newsId={this.props.modal.modalProps.newsId}
        isFromModal={this.props.modal.modalProps.isFromModal}
        onRequestCloseModal={this.handleCloseModal}
      />
    );
  }

  renderDetailNews() {
    return (
      <NewsDetailModal
        newsId={this.props.modal.modalProps.newsId}
        isMyCompany={this.props.modal.modalProps.isMyCompany}
        isReadOnly={this.props.modal.modalProps.isReadOnly}
        onRequestCloseModal={this.handleCloseModal}
      />
    );
  }

  renderEmpty() {
    return '';
  }


  render() {
    const isOpen = this.props.modal.show;
    const styles = this.getStylesForType();

    return (
      <Modal
        className='Modal__Bootstrap modal-dialog'
        isOpen={isOpen}
        onRequestClose={this.handleCloseModal}
        contentLabel='Modal'
        style={styles}
      >
        { this.getModalContentForType() }
      </Modal>
    );
  }
}

MainModal.propTypes = propTypes;
MainModal.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  const modal = state.modal;

  return {
    modal
  };
}

export default connect(mapStateToProps)(MainModal);
