import React, { Component } from 'react';

export default class Banner extends Component {
  render() {
    return (
      <div className='text-center mb20 mt20'>
        <img src='/images/news-banner.jpg' alt='' />
      </div>
    );
  }
}
