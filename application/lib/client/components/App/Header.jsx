import React, { Component }             from 'react';
import { Sticky }                       from 'react-sticky';
import { Link }                         from 'react-router';
import { getUserId, logout }            from '../../redux/utils/helpers';
import { browserHistory }               from 'react-router';

export default class Header extends Component {

  constructor(props) {
    super(props);
    this.handleWindowScroll = this.handleWindowScroll.bind(this);
    this.state = {
      lastScrollTop: 0
    };
  }

  componentDidMount() {
    if (typeof document !== 'undefined') {
      document.addEventListener('scroll', this.handleWindowScroll);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleWindowScroll);
  }

  handleWindowScroll() {
    const lastScrollTop = this.state.lastScrollTop;
    const st = window.pageYOffset || document.documentElement.scrollTop;
    const mainNav = document.querySelector('.jsMainFixedNav');

    if (mainNav === null) return;
    if (st > lastScrollTop) {
      // Down
      mainNav.classList.add('isStickyBottomHeader--hide');
    } else {
      // Up
      mainNav.classList.remove('isStickyBottomHeader--hide');
    }
    this.setState({
      lastScrollTop: st
    });
  }

  handleLogout(e) {
    e.preventDefault();
    logout();
    browserHistory.push('/');
  }

  handleNavbarsClick(e) {
    const mainNav = document.querySelector('.jsMainNav');
    const mainNavBars = e.currentTarget;

    mainNav.classList.toggle('nav-is-toggled');
    mainNavBars.classList.toggle('nav-wrapper__bars--is-toggled');
  }

  handleNavClick(e) {
    const mainNav = e.currentTarget;
    const mainNavBars = document.querySelector('.jsMainNavbars');

    mainNav.classList.toggle('nav-is-toggled');
    mainNavBars.classList.toggle('nav-wrapper__bars--is-toggled');
  }

  subMenu() {
    return (
      <Sticky stickyClassName={'isStickyBottomHeader'} className='jsMainFixedNav'>
        {/* Bottom bar */}
        <div className='main-header__bottom'>
          <div className='container'>
            <div className='nav-wrapper'>
              <div className='main-header__logo nav-wrapper__left'>
                <a href='#' className=''><img src='/images/sm-avatar.jpg' alt='logo'/></a>
              </div>
              <nav className='main-nav nav-wrapper__right'>
                <ul className='main-nav__list main-nav__list--hamburger jsMainNav' onClick={this.handleNavClick}>
                  <li className='main-nav__item'>
                    <Link className='main-nav__link' to={`/id${getUserId()}`}>Моя компания</Link>
                  </li>
                  <li className='main-nav__item'>
                    <Link className='main-nav__link' to={`/news-list/${getUserId()}`}>Мои новости</Link>
                  </li>
                  <li className='main-nav__item'>
                    <a href='#' className='main-nav__link'>Мои сообщения</a>
                  </li>
                  <li className='main-nav__item'>
                    <Link className='main-nav__link' to={`/partners/${getUserId()}/partners`}>Партнеры</Link>
                  </li>
                  <li className='main-nav__item'>
                    <a href='#' className='main-nav__link'>Портфолио</a>
                  </li>
                  <li className='main-nav__item'>
                    <a href='#' className='main-nav__link'>Настройки</a>
                  </li>
                  <li className='main-nav__item'>
                    <a href='#' className='main-nav__link'>Поиск</a>
                  </li>
                </ul>
              </nav>
              <div className='nav-wrapper__bars jsMainNavbars' onClick={this.handleNavbarsClick}>
                <span />
                <span />
                <span />
              </div>
            </div>
          </div>
        </div>
      </Sticky>
    );
  }

  mainMenu(isLoggedIn) {
    const signInLink =
      <Link className='main-nav__link' to='/sign-in'>Вход</Link>;

    const signOutLink =
      <a href className='main-nav__link' onClick={this.handleLogout}>Выход</a>;

    const signUpLink =
      <Link className='main-nav__link' to='/sign-up'>Регистрация</Link>;

    return (
      <div className='main-header__top'>
        <div className='container'>
          <div className='main-header__top-inner'>
            <nav className='main-nav main-nav--half-width'>
              <Link className='main-nav__logo' to='/'><img src='/images/logo-white.png' alt=''/></Link>
            </nav>

            <nav className='main-nav main-nav--half-width main-nav--alt-color'>
              <ul className='main-nav__list main-nav__list--nowrap flex-end'>
                <li className='main-nav__item'>
                  <a href='#' className='main-nav__link'>О проекте</a>
                </li>
                <li className='main-nav__item'>
                  {isLoggedIn ? signOutLink : signInLink }
                </li>
                {!isLoggedIn &&
                <li className='main-nav__item'>
                  {signUpLink}
                </li>
                }
              </ul>
            </nav>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const isLoggedIn = getUserId() !== '';

    return (
      <div className='main-header'>
        {this.mainMenu(isLoggedIn)}
        {isLoggedIn && this.subMenu()}
      </div>
    );
  }
}
