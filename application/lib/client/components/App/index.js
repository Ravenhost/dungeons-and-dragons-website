import App       from './App';
import Banner    from './Banner';
import Footer    from './Footer';
import Header    from './Header';
import MainModal from './MainModal';

export default App;
export {
  Banner,
  Footer,
  Header,
  MainModal
};
