if (process.env.NODE_ENV === 'dev') {
  module.exports = require('./App.dev');
} else {
  module.exports = require('./App.prod');
}
