import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return (
      <div className='uou-block-4a secondary light'>
        <div className='container text-center'>
          <p>&#9426; ProFeel, 2016</p>
        </div>
      </div>
    );
  }
}
