import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import {
  signIn,
  resendConfirmEmail,
  signInResetStore,
  SIGN_IN_ERROR_IS_NOT_CONFIRMED
} from '../../redux/actions/signInActions';
import { Link }             from 'react-router';

const defaultProps = {
  signIn: {
    signingIn: {
      isFetching: false,
      isFetched: false,
      hasError: false,
      errorCode: '',
      errorMessage: '',
      data: {}
    },
    resendConfirmEmail: {
      isFetching: false,
      isFetched: false,
      hasError: false,
      data: {}
    }
  },
  dispatch: () => {
  }
};

const propTypes = {
  signIn: PropTypes.object,
  dispatch: PropTypes.func
};


@connect(state => ({ signIn: state.signIn }))
export default class SignInPage extends Component {
  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSignInClick = this.handleSignInClick.bind(this);
    this.handleResendConfirmEmailClick = this.handleResendConfirmEmailClick.bind(this);

    this.state = {
      email: '',
      password: ''
    };
  }

  componentDidMount() {
    this.setupValidation();
    this.props.dispatch(signInResetStore());
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }


  /**
   * Обработчик кнопки 'Войти'
   */
  handleSignInClick(event) {
    event.preventDefault();
    if (!$('#signInForm').valid()) {
      return;
    }
    const email = this.state.email;
    const password = this.state.password;

    this.props.dispatch(signIn(email, password));
  }

  handleResendConfirmEmailClick(event) {
    event.preventDefault();
    const email = this.state.email;

    this.props.dispatch(resendConfirmEmail(email));
  }

  setupValidation() {
    $('#signInForm').validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true
        }
      },
      messages: {
        email: 'Укажите корректный e-mail, например, person@mail.ru',
        password: 'Это обязательно поле к заполнению'
      },
      errorPlacement: function style(label, element) {
        label.css('color', 'red');
        label.insertAfter(element);
      }
    });
  }


  resendLinks = () => {
    return (
      <div>
        <strong>Не получили письмо?</strong>
        <p><a href='#' onClick={this.handleResendConfirmEmailClick}>Нажмите здесь для повторной отправки</a></p>
      </div>);
  };

  renderErrors = (text) => {
    return (
      <div className='alert alert-error alert-dismissible' role='alert'>
        <button type='button' className='close'/>
        <strong>{text}</strong>
      </div>);
  };

  render() {
    const signingIn = this.props.signIn.signingIn;
    const resendConfirm = this.props.signIn.resendConfirmEmail;

    if (resendConfirm.isFetched && !resendConfirm.hasError) {
      return (
        <div className='container'>
          <div className='row'>
            <div className='col-sm-6 col-sm-offset-3'>
              <div className='panel panel--white mtb20'>
                <h1>Авторизация</h1>
                <p>Мы отправили на ваш e-mail письмо с инструкциями по активации учётной записи.</p>
              </div>
            </div>
          </div>
        </div>);
    }

    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm-6 col-sm-offset-3'>
            <div className='panel panel--white mtb20'>
              <h1>Авторизация</h1>
              {signingIn.hasError && this.renderErrors(signingIn.data.message)}
              {signingIn.hasError && signingIn.errorCode === SIGN_IN_ERROR_IS_NOT_CONFIRMED && this.resendLinks()}

              <form name='signInForm' id='signInForm'>

                <label>E-mail
                  <input
                    id='email'
                    type='email'
                    name='email'
                    required
                    value={this.state.email}
                    onChange={this.handleInputChange}
                  />
                </label>

                <label>Пароль
                  <input
                    id='password'
                    type='password'
                    name='password'
                    required
                    value={this.state.password}
                    onChange={this.handleInputChange}
                  />
                </label>

                <div className='text-right mb30'>
                  <button
                    className='btn btn-simple'
                    type='button'
                  >
                    <Link to='/password'>Забыли пароль?</Link>
                  </button>
                </div>

                <div className='text-right'>
                  <button
                    className='btn btn-transparent-primary'
                    onClick={this.handleSignInClick}
                  >
                    Вход
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SignInPage.propTypes = propTypes;
SignInPage.defaultProps = defaultProps;
