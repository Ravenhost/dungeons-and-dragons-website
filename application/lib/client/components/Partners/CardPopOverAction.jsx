import React, { Component, PropTypes }  from 'react';
import {
  OverlayTrigger
}                                       from 'react-bootstrap';

const defaultProps = {
  dispatch: () => {}
};

const propTypes = {
  children: PropTypes.node,
  dispatch: PropTypes.func
};


export default class CardPopOverAction extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <OverlayTrigger
        trigger={'click'}
        rootClose
        placement='top'
        overlay={this.props.children}
      >
        <button
          className='btn-popup btn-popup--vertical-dots'
        >
          <span className='btn-popup__dot'/>
          <span className='btn-popup__dot'/>
          <span className='btn-popup__dot'/>
        </button>
      </OverlayTrigger>
    );
  }
}

CardPopOverAction.propTypes = propTypes;
CardPopOverAction.defaultProps = defaultProps;
