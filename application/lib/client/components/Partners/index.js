import CompanyPartners         from './Tabs/CompanyPartners';
import Partners           from './Partners';
import IncomingProposal   from './Tabs/IncomingProposal';
import OutgoingProposal   from './Tabs/OutgoingProposal';
import Followers          from './Tabs/Followers';
import Subscriptions      from './Tabs/Subscriptions';
import Recommended        from './Tabs/Recommended';
import MutualPartners     from './Tabs/MutualPartners';

export {
  CompanyPartners,
  Partners,
  IncomingProposal,
  OutgoingProposal,
  Followers,
  Subscriptions,
  Recommended,
  MutualPartners
};
