import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { Link }                         from 'react-router';
import {
  fetchIncomingRequestPartners,
  fetchAcceptRequestPartners,
  fetchRejectRequestPartners
}                                       from '../../../redux/actions/partnersRequestActions';
import RootSearch                       from './RootSearch';
import ProfileCard                      from '../ProfileCard';
import {
  Popover
}                                       from 'react-bootstrap';

const defaultProps = {
  incomingRequests: {
    requests:null,
    page: 1,
    search: '',
    loaded: false,
    loading: false
  },
  params: {
    companyId: null
  },
  dispatch: () => {}
};

const propTypes = {
  incomingRequests: PropTypes.object,
  params: PropTypes.object,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class IncomingProposal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      perPage: 3
    };

    this.handleFetchIncomingRequest = this.handleFetchIncomingRequest.bind(this);
    this.renderRequestButtons = this.renderRequestButtons.bind(this);
    this.renderRequestActions = this.renderRequestActions.bind(this);
    this.handleClickAddPartner = this.handleClickAddPartner.bind(this);
    this.handleClickLeaveSubscriber = this.handleClickLeaveSubscriber.bind(this);
    this.handleRenderRequest = this.handleRenderRequest.bind(this);
  }


  handleClickAddPartner(request) {
    this.props.dispatch(fetchAcceptRequestPartners(request));
  }

  handleClickLeaveSubscriber(request) {
    this.props.dispatch(fetchRejectRequestPartners(request));
  }

  handleFetchIncomingRequest(page, search) {
    this.props.dispatch(fetchIncomingRequestPartners({
      page,
      perPage: this.state.perPage,
      search
    }));
  }

  handleRenderRequest(request, key) {
    return (<ProfileCard
      renderButtons={this.renderRequestButtons}
      renderActions={this.renderRequestActions}
      key={key}
      profile={request}
            />);
  }

  renderRequestButtons(request) {
    return (
      <div className='flex-column mtb10'>
        <button
          onClick={() => {
            this.handleClickAddPartner(request);
          }}
          className='btn btn-primary-second'
        >Добавить в партнеры</button>
        <button
          onClick={() => {
            this.handleClickLeaveSubscriber(request);
          }}
          className='btn btn-primary-second mt10'
        >Отклонить заявку</button>
      </div>
    );
  }

  renderRequestActions(request)  {
    return (
      <Popover
        id={request._id}
      >
        <div>
          <Link className='clickable' to={`/partners/${request._id}/partners`}>Посмотреть партнеров</Link>
        </div>
        <div>
          <a className='clickable'>Пожаловаться</a>
        </div>
      </Popover>
    );
  }

  render() {
    return (
      <RootSearch
        objects={this.props.incomingRequests.requests}
        companyId={this.props.params.companyId}
        search={this.props.incomingRequests.search}
        page={this.props.incomingRequests.page}
        onFetchContent={this.handleFetchIncomingRequest}
        onRenderResultObject={this.handleRenderRequest}
      />
    );
  }
}

IncomingProposal.propTypes = propTypes;
IncomingProposal.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    incomingRequests: state.partnersRequest.incomingRequests
  };
}

export default connect(mapStateToProps)(IncomingProposal);
