import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { Link }                         from 'react-router';
import {
  fetchMutualPartners,
  fetchDeletePartners
}                                       from '../../../redux/actions/partnersActions';
import RootSearch                       from './RootSearch';
import ProfileCard                      from '../ProfileCard';
import {
  Popover
}                                       from 'react-bootstrap';

const defaultProps = {
  mutualPartners: {
    partners:null,
    page: 1,
    search: ''
  },
  loaded: false,
  loading: false,
  params: {
    companyId: null
  },
  dispatch: () => {}
};

const propTypes = {
  mutualPartners: PropTypes.object,
  params: PropTypes.object,
  loaded: PropTypes.bool,
  loading: PropTypes.bool,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class MutualPartners extends Component {

  constructor(props) {
    super(props);

    this.state = {
      perPage: 3
    };

    this.handleFetchMutualPartners = this.handleFetchMutualPartners.bind(this);
    this.handleClickRemovePartner = this.handleClickRemovePartner.bind(this);
    this.handleRenderPartner = this.handleRenderPartner.bind(this);
  }

  handleFetchMutualPartners(page, search) {
    this.props.dispatch(fetchMutualPartners({
      partnerId: this.props.params.companyId,
      page,
      perPage: this.state.perPage,
      search
    }));
  }

  handleClickRemovePartner(id) {
    this.props.dispatch(fetchDeletePartners(id));
  }

  handleRenderPartner(partner, key) {
    return (<ProfileCard
      renderButtons={this.renderPartnerButtons}
      renderActions={this.renderPartnerActions}
      key={key}
      profile={partner}
            />);
  }

  renderPartnerButtons(partner) {
    return (
      <div className='flex-column mtb10'>
        <button
          onClick={() => {
            console.log(partner);
          }}
          className='btn btn-primary-second mt10'
        >Написать сообщение</button>
      </div>
    );
  }

  renderPartnerActions(partner)  {
    return (
      <Popover
        id={partner._id}
      >
        <div>
          <a className='clickable' onClick={() =>  this.handleClickRemovePartner(partner._id)}>
            Убрать из партнеров
          </a>
        </div>
        <div>
          <Link className='clickable' to={`/partners/${partner._id}/partners`}>Посмотреть партнеров</Link>
        </div>
        <div>
          <a className='clickable'>Пожаловаться</a>
        </div>
      </Popover>
    );
  }

  render() {
    return (
      <RootSearch
        objects={this.props.mutualPartners.partners}
        companyId={this.props.params.companyId}
        search={this.props.mutualPartners.search}
        page={this.props.mutualPartners.page}
        onFetchContent={this.handleFetchMutualPartners}
        onRenderResultObject={this.handleRenderPartner}
      />
    );
  }
}

MutualPartners.propTypes = propTypes;
MutualPartners.defaultProps = defaultProps;

function mapStateToProps(state, props) {
  const id = props.companyId;
  const mutualPartners = state.partners.mutualPartners[id];

  return {
    mutualPartners,
    loaded: state.partners.loaded,
    loading: state.partners.loading
  };
}

export default connect(mapStateToProps)(MutualPartners);
