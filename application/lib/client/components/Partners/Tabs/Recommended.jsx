import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';

const defaultProps = {
  dispatch: () => {}
};

const propTypes = {
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class Recommended extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>Recommended</div>
    );
  }
}

Recommended.propTypes = propTypes;
Recommended.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  return {

  };
}
/* eslint-enable no-unused-vars */

export default connect(mapStateToProps)(Recommended);
