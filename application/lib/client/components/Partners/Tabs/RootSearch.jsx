import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import Search                           from '../Search';

const defaultProps = {
  search: '',
  page: 1,
  companyId: null,
  objects: null,
  onFetchContent: () => {},
  onRenderResultObject: () => {},
  dispatch: () => {}
};

const propTypes = {
  search: PropTypes.string,
  page: PropTypes.number,
  companyId: PropTypes.string,
  objects: PropTypes.array,
  onFetchContent: PropTypes.func,
  onRenderResultObject: PropTypes.func,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class RootSearch extends Component {

  constructor(props) {
    super(props);

    this.handleWindowScroll = this.handleWindowScroll.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentDidMount() {
    this.props.onFetchContent(1, this.props.search);

    if (typeof document !== 'undefined') {
      document.addEventListener('scroll', this.handleWindowScroll);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.companyId !== this.props.companyId) {
      this.props.onFetchContent(1, this.props.search);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleWindowScroll);
  }

  handleWindowScroll() {
    const footerPositionBottom = document.getElementsByClassName('uou-block-4a')[0].getBoundingClientRect().bottom;
    const windowHeight = document.documentElement.clientHeight;

    if (parseInt(footerPositionBottom - windowHeight, 10) === 0) {
      this.props.onFetchContent(this.props.page + 1, this.props.search);
    }
  }

  handleSearch(searchString) {
    this.props.onFetchContent(1, searchString);
  }

  renderResultContent() {
    if (this.props.objects) {
      return (
        <div>
          {this.props.objects.map((partner, key) => {
            return this.props.onRenderResultObject(partner, key);
          })}
        </div>
      );
    }
    return (
      <div>Загружаем...</div>
    );
  }

  render() {
    return (
      <div>
        <Search
          search={this.props.search}
          onSearch={this.handleSearch}
        />
        {this.renderResultContent()}
        {this.props.children}
      </div>
    );
  }
}

RootSearch.propTypes = propTypes;
RootSearch.defaultProps = defaultProps;

export default connect()(RootSearch);
