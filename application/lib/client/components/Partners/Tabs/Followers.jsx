import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { Link }                         from 'react-router';
import {
  fetchUserFollowers
}                                       from '../../../redux/actions/followAction';
import RootSearch                       from './RootSearch';
import ProfileCard                      from '../ProfileCard';
import {
  Popover
}                                       from 'react-bootstrap';


const defaultProps = {
  followers: {
    objects:[],
    page: 0,
    search: '',
    loaded: false,
    loading: false
  },
  params: {
    companyId: null
  },
  dispatch: () => {}
};

const propTypes = {
  followers: PropTypes.object,
  params: PropTypes.object,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class Followers extends Component {

  constructor(props) {
    super(props);

    this.state = {
      perPage: 3
    };

    this.renderFollowerActions = this.renderFollowerActions.bind(this);
    this.renderFollowerButtons = this.renderFollowerButtons.bind(this);
    this.handleFetchUserFollowers = this.handleFetchUserFollowers.bind(this);
    this.handleRenderFollower = this.handleRenderFollower.bind(this);
  }

  handleFetchUserFollowers(page, search) {
    this.props.dispatch(fetchUserFollowers({
      page,
      perPage: this.state.perPage,
      search
    }));
  }

  handleRenderFollower(follower, key) {
    return (<ProfileCard
      renderButtons={this.renderFollowerButtons}
      renderActions={this.renderFollowerActions}
      key={key}
      profile={follower}
            />);
  }

  renderFollowerButtons() {
    return '';
  }

  renderFollowerActions(follower)  {
    return (
      <Popover
        id={follower._id}
      >
        <div>
          <Link className='clickable' to={`/partners/${follower._id}/partners`}>Посмотреть партнеров</Link>
        </div>
      </Popover>
    );
  }

  render() {
    return (
      <RootSearch
        companyId={this.props.params.companyId}
        objects={this.props.followers.objects}
        search={this.props.followers.search}
        page={this.props.followers.page}
        onFetchContent={this.handleFetchUserFollowers}
        onRenderResultObject={this.handleRenderFollower}
      />
    );
  }
}

Followers.propTypes = propTypes;
Followers.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    followers: state.follows.followers
  };
}

export default connect(mapStateToProps)(Followers);
