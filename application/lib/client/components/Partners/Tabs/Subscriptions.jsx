import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { Link }                         from 'react-router';
import {
  fetchUserSubscriptions,
  fetchDeleteSubscriptions
}                                       from '../../../redux/actions/followAction';
import RootSearch                       from './RootSearch';
import ProfileCard                      from '../ProfileCard';
import {
  Popover
}                                       from 'react-bootstrap';


const defaultProps = {
  subscriptions: {
    objects:[],
    page: 0,
    search: '',
    loaded: false,
    loading: false
  },
  params: {
    companyId: null
  },
  dispatch: () => {}
};

const propTypes = {
  subscriptions: PropTypes.object,
  params: PropTypes.object,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class Subscriptions extends Component {

  constructor(props) {
    super(props);

    this.state = {
      perPage: 3
    };

    this.renderSubscriptionActions = this.renderSubscriptionActions.bind(this);
    this.renderSubscriptionButtons = this.renderSubscriptionButtons.bind(this);
    this.handleFetchUserSubscriptions = this.handleFetchUserSubscriptions.bind(this);
    this.handleUnsubscribe = this.handleUnsubscribe.bind(this);
    this.handleRenderSubscription = this.handleRenderSubscription.bind(this);
  }

  handleFetchUserSubscriptions(page, search) {
    this.props.dispatch(fetchUserSubscriptions({
      page,
      perPage: this.state.perPage,
      search
    }));
  }

  handleUnsubscribe(subscriptionId) {
    this.props.dispatch(fetchDeleteSubscriptions(subscriptionId));
  }

  handleRenderSubscription(subscription, key) {
    return (<ProfileCard
      renderButtons={this.renderSubscriptionButtons}
      renderActions={this.renderSubscriptionActions}
      key={key}
      profile={subscription}
            />);
  }

  renderSubscriptionButtons(subscription) {
    return (
      <div className='flex-column mtb10'>
        <button
          onClick={() => this.handleUnsubscribe(subscription._id)}
          className='btn btn-primary-second'
        >Отписаться от обновлений</button>
      </div>
    );
  }

  renderSubscriptionActions(subscription)  {
    console.log(`/partners/${subscription._id}/partners`);
    return (
      <Popover
        id={subscription._id}
      >
        <div>
          <Link className='clickable' to={`/partners/${subscription._id}/partners`}>Посмотреть партнеров</Link>
        </div>
      </Popover>
    );
  }

  render() {
    return (
      <RootSearch
        objects={this.props.subscriptions.objects}
        companyId={this.props.params.companyId}
        search={this.props.subscriptions.search}
        page={this.props.subscriptions.page}
        onFetchContent={this.handleFetchUserSubscriptions}
        onRenderResultObject={this.handleRenderSubscription}
      />
    );
  }
}

Subscriptions.propTypes = propTypes;
Subscriptions.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    subscriptions: state.follows.subscriptions
  };
}

export default connect(mapStateToProps)(Subscriptions);
