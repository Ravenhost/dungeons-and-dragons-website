import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { Link }                         from 'react-router';
import {
  fetchOutgoingRequestPartners,
  fetchDeleteRequestPartners
}                                       from '../../../redux/actions/partnersRequestActions';
import RootSearch                       from './RootSearch';
import ProfileCard                      from '../ProfileCard';
import {
  Popover
}                                       from 'react-bootstrap';

const defaultProps = {
  outgoingRequests: {
    requests:null,
    page: 1,
    search: '',
    loaded: false,
    loading: false
  },
  params: {
    companyId: null
  },
  dispatch: () => {}
};

const propTypes = {
  outgoingRequests: PropTypes.object,
  params: PropTypes.object,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class OutgoingProposal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      perPage: 3
    };

    this.handleFetchOutgoingRequest = this.handleFetchOutgoingRequest.bind(this);
    this.renderRequestButtons = this.renderRequestButtons.bind(this);
    this.renderRequestActions = this.renderRequestActions.bind(this);
    this.handleDeleteOutgoingRequest = this.handleDeleteOutgoingRequest.bind(this);
    this.handleRenderRequest = this.handleRenderRequest.bind(this);
  }

  handleFetchOutgoingRequest(page, search) {
    this.props.dispatch(fetchOutgoingRequestPartners({
      page,
      perPage: this.state.perPage,
      search
    }));
  }

  handleDeleteOutgoingRequest(requestId) {
    this.props.dispatch(fetchDeleteRequestPartners(requestId));
  }

  handleRenderRequest(request, key) {
    return (<ProfileCard
      renderButtons={this.renderRequestButtons}
      renderActions={this.renderRequestActions}
      key={key}
      profile={request}
            />);
  }

  renderRequestButtons(request) {
    return (
      <div className='flex-column mtb10'>
        <button
          onClick={() => this.handleDeleteOutgoingRequest(request._id)}
          className='btn btn-primary-second'
        >Отозвать заявку</button>
      </div>
    );
  }

  renderRequestActions(request)  {
    return (
      <Popover
        id={request._id}
      >
        <div>
          <Link className='clickable' to={`/partners/${request._id}/partners`}>Посмотреть партнеров</Link>
        </div>
      </Popover>
    );
  }

  render() {
    return (
      <RootSearch
        objects={this.props.outgoingRequests.requests}
        companyId={this.props.params.companyId}
        search={this.props.outgoingRequests.search}
        page={this.props.outgoingRequests.page}
        onFetchContent={this.handleFetchOutgoingRequest}
        onRenderResultObject={this.handleRenderRequest}
      />
    );
  }
}

OutgoingProposal.propTypes = propTypes;
OutgoingProposal.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    outgoingRequests: state.partnersRequest.outgoingRequests
  };
}

export default connect(mapStateToProps)(OutgoingProposal);
