import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import ProfileCard                      from '../ProfileCard';
import { Link }                         from 'react-router';
import {
  fetchUserPartners,
  fetchAddPartners
}                                       from '../../../redux/actions/partnersActions';
import {
  Popover
}                                       from 'react-bootstrap';
import { fetchDeletePartners }          from '../../../redux/actions/partnersActions';
import RootSearch                       from './RootSearch';
import { getUserId }                    from '../../../redux/utils/helpers';


const defaultProps = {
  params: {
    companyId: null
  },
  partners:{
    page: 0,
    search: '',
    partners: null
  },
  loaded: false,
  loading: false,
  dispatch: () => {}
};

const propTypes = {
  params: PropTypes.object,
  partners: PropTypes.object,
  loaded: PropTypes.bool,
  loading: PropTypes.bool,
  children: PropTypes.node,
  dispatch: PropTypes.func
};

class CompanyPartners extends Component {

  constructor(props) {
    super(props);

    this.state = {
      perPage: 3
    };
    this.handleFetchPartners = this.handleFetchPartners.bind(this);
    this.renderAnotherPartnerActions = this.renderAnotherPartnerActions.bind(this);
    this.renderMyPartnerActions = this.renderMyPartnerActions.bind(this);
    this.renderAnotherPartnerButtons = this.renderAnotherPartnerButtons.bind(this);
    this.renderMyPartnerButtons = this.renderMyPartnerButtons.bind(this);
    this.handleClickRemovePartner = this.handleClickRemovePartner.bind(this);
    this.handleFetchAddPartners = this.handleFetchAddPartners.bind(this);
    this.renderPartnerButtons = this.renderPartnerButtons.bind(this);
    this.renderPartnerActions = this.renderPartnerActions.bind(this);
    this.handleRenderPartner = this.handleRenderPartner.bind(this);
  }

  handleClickRemovePartner(id) {
    this.props.dispatch(fetchDeletePartners(id));
  }

  handleFetchPartners(page, search) {
    this.props.dispatch(fetchUserPartners({
      userId: this.props.params.companyId,
      page,
      perPage: this.state.perPage,
      search
    }));
  }

  handleFetchAddPartners(partnerId) {
    this.props.dispatch(fetchAddPartners(
      partnerId
    ));
  }

  handleRenderPartner(partner, key) {
    return (<ProfileCard
      renderButtons={this.renderPartnerButtons}
      renderActions={this.renderPartnerActions}
      key={key}
      profile={partner}
            />);
  }

  isMyPartners(companyId) {
    return getUserId() === companyId;
  }

  renderPartnerButtons(partner) {
    return this.isMyPartners(this.props.params.companyId)
      ? this.renderMyPartnerButtons(partner) : this.renderAnotherPartnerButtons(partner);
  }

  renderPartnerActions(partner) {
    return this.isMyPartners(this.props.params.companyId)
      ? this.renderMyPartnerActions(partner) : this.renderAnotherPartnerActions(partner);
  }

  renderMyPartnerButtons(partner) {
    return (
      <button
        onClick={() => {
          console.log(partner._id);
        }}
        className='btn btn-primary-second'
      >Написать сообщение</button>
    );
  }

  renderAnotherPartnerButtons(partner) {
    return (
      <div className='flex-column mtb10'>
        <button
          onClick={() => this.handleFetchAddPartners(partner._id)}
          className='btn btn-primary-second'
        >Добавить в партнеры</button>
        <button
          onClick={() => {
            console.log(partner._id);
          }}
          className='btn btn-primary-second mt10'
        >Написать сообщение</button>
      </div>
    );
  }

  renderAnotherPartnerActions(partner) {
    return (
      <Popover
        id={partner._id}
      >
        <div>
          <a className='clickable'>Пожаловаться</a>
        </div>
      </Popover>
    );
  }

  renderMyPartnerActions(partner) {
    return (
      <Popover
        id={partner._id}
      >
        <div>
          <a className='clickable' onClick={() =>  this.handleClickRemovePartner(partner._id)}>
            Убрать из партнеров
          </a>
        </div>
        <div>
          <Link className='clickable' to={`/partners/${partner._id}/partners`}>Посмотреть партнеров</Link>
        </div>
        <div>
          <a className='clickable'>Пожаловаться</a>
        </div>
      </Popover>
    );
  }

  render() {
    return (
      <RootSearch
        companyId={this.props.params.companyId}
        objects={this.props.partners.partners}
        search={this.props.partners.search}
        page={this.props.partners.page}
        onFetchContent={this.handleFetchPartners}
        onRenderResultObject={this.handleRenderPartner}
      />
    );
  }
}

CompanyPartners.propTypes = propTypes;
CompanyPartners.defaultProps = defaultProps;

function mapStateToProps(state, props) {
  const id = props.companyId;
  const partners = state.partners.usersPartners[id];

  return {
    partners,
    loaded: state.partners.loaded,
    loading: state.partners.loading
  };
}

export default connect(mapStateToProps)(CompanyPartners);
