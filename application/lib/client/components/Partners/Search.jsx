import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';

const defaultProps = {
  search: '',
  onSearch: () => {},
  dispatch: () => {}
};

const propTypes = {
  search: PropTypes.string,
  onSearch: PropTypes.func,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class Search extends Component {

  constructor(props) {
    super(props);

    this.state = {
      search: this.props.search
    };

    this.handleSearch = this.handleSearch.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      search: nextProps.search
    });
  }

  handleSearch() {
    this.props.onSearch(this.state.search);
  }

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.props.onSearch(this.state.search);
    }
  };

  render() {
    return (
      <div className='flex-grid mt20 mb20'>
        <div className='flex-col flex-col--sm10'>
          <input
            className='mb0 mb15-sm'
            value={this.state.search}
            type='text'
            required
            autoFocus
            placeholder='Поиск'
            onKeyPress={this.handleKeyPress}
            onChange={event => {
              this.setState({
                search: event.target.value
              });
            }}
          />
        </div>
        <div className='flex-col flex-col--xs4 flex-col--sm2 flex-to-right--sm'>
          <button
            onClick={this.handleSearch}
            className='btn btn-primary'
          >Найти</button>
        </div>
      </div>
    );
  }
}

Search.propTypes = propTypes;
Search.defaultProps = defaultProps;

export default connect()(Search);
