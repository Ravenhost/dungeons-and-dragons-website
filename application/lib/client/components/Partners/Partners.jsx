import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { browserHistory }               from 'react-router';
import { getUserId }                    from '../../redux/utils/helpers';
import {
  fetchProfileCounters
}                                       from '../../redux/actions/companyProfileActions';
import {
  Tabs,
  Tab
}                                       from 'react-bootstrap';

const defaultProps = {
  counters: {
    followers: 0,
    subscriptions: 0,
    outgoing: 0,
    incoming: 0,
    partners: 0,
    mutual: 0,
    followersNew: 0
  },
  location: {},
  params: {},
  dispatch: () => {}
};

const propTypes = {
  counters: PropTypes.object,
  children: PropTypes.node,
  location: PropTypes.object,
  params: PropTypes.object,
  dispatch: PropTypes.func
};


class Partners extends Component {

  constructor(props) {
    super(props);

    this.state = {
      tabItems: this.getTabs(this.props.params.companyId)
    };

    this.handleSelect = this.handleSelect.bind(this);
    this.isMyPartners = this.isMyPartners.bind(this);
  }

  componentDidMount() {
    this.getCounters(this.props.params.companyId);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.companyId !== nextProps.params.companyId) {
      this.getCounters(nextProps.params.companyId);
      this.setState({
        tabItems: this.getTabs(nextProps.params.companyId)
      });
    }
  }

  handleSelect(event) {
    const splitedUrl = this.props.location.pathname.split('/');

    splitedUrl[splitedUrl.length - 1] = event;
    browserHistory.push(splitedUrl.join('/'));
  }

  getCounters(companyId) {
    if (this.isMyPartners(companyId)) {
      this.props.dispatch(fetchProfileCounters());
    } else {
      console.log('another counter');
    }
  }

  getTabs(companyId) {
    return this.isMyPartners(companyId) ? [
      {
        title:'Мои партнеры',
        url: 'partners',
        counterName: 'partners'
      },
      {
        title:'Входящие заявки',
        url: 'all_requests',
        counterName: 'incoming'
      },
      {
        title:'Исходящие заявки',
        url: 'out_requests',
        counterName: 'outgoing'
      },
      {
        title:'Мои подписки',
        url: 'following',
        counterName: 'subscriptions'
      },
      {
        title:'Подписчики',
        url: 'followers',
        counterName: 'followers'
      },
      {
        title:'Рекомендованные',
        url: 'recommended',
        counterName: ''
      }
    ] : [
      {
        title:'Партнеры',
        url: 'partners',
        counterName: 'partners'
      },
      {
        title:'Общие партнеры',
        url: 'mutual',
        counterName: 'mutual'
      }
    ];
  }

  isMyPartners(companyId) {
    return getUserId() === companyId;
  }

  renderCounter(tabCounterName) {
    const count = this.props.counters[tabCounterName];

    return (
      <span>
        { count !== 0 ? count : '' }
      </span>
    );
  }

  renderTabs() {
    const splitedUrl = this.props.location.pathname.split('/');
    const activeKey = splitedUrl[splitedUrl.length - 1];

    return (
      <div className='main-tabs main-tabs--fullwidth'>
        <Tabs
          id='partners'
          activeKey={activeKey}
          onSelect={this.handleSelect}
        >
          {
            this.state.tabItems.map((value, index) => {
              return (
                <Tab key={index}
                  eventKey={value.url}
                  title={(
                    <h5 className='main-tabs__header mtb5'>
                      {value.title}
                      {this.renderCounter(value.counterName)}
                    </h5>
                  )}
                  tabClassName='main-tabs__tab-item'
                />
              );
            })
          }
        </Tabs>
      </div>
    );
  }

  render() {
    return (
      <div className='container mt20'>
        {this.renderTabs()}
        <div className='profile-block mb20'>
          {React.cloneElement(this.props.children, { ...this.props.params })}
        </div>
      </div>
    );
  }
}

Partners.propTypes = propTypes;
Partners.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    counters: state.companyProfile.counters
  };
}

export default connect(mapStateToProps)(Partners);
