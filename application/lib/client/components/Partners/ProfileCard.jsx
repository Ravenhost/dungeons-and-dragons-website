import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { getAvatarUrl }                 from '../../redux/utils/helpers';
import CardPopOverAction                from './CardPopOverAction';
import { Link }                         from 'react-router';

const defaultProps = {
  profile:{},
  renderButtons: () => {
    return '';
  },
  renderActions: () => {
    return '';
  },
  dispatch: () => {}
};

const propTypes = {
  profile: PropTypes.object,
  renderButtons: PropTypes.func,
  renderActions: PropTypes.func,
  children: PropTypes.node,
  dispatch: PropTypes.func
};


class ProfileCard extends Component {

  constructor(props) {
    super(props);
  }

  isHasSubHeader(profile) {
    return profile.companyData.director || profile.companyData.directorPost;
  }

  renderSubHeader(profile) {
    if (this.isHasSubHeader(profile)) {
      return (
        <h3 className='card-row__subheader'>{profile.companyData.directorPost}:{profile.companyData.director}</h3>
      );
    }
    return '';
  }

  render() {
    const profile = this.props.profile;
    const actions = this.props.renderActions(profile);
    const buttons = this.props.renderButtons(profile);

    return (
      <div className='card-row'>
        <Link to={`/id${profile._id}`}>
          <div className='card-row__image'>
            <img src={profile.avatar && getAvatarUrl(profile.avatar, 'mini')} alt='' />
          </div>
        </Link>
        <div className='card-row__content'>
          <Link to={`/id${profile._id}`}>
            <h2 className='card-row__header'>{profile.companyData.legalName}</h2>
          </Link>
          {this.renderSubHeader(profile)}
          <div className='card-row__text'>
            {profile.description}
          </div>
        </div>
        <div className='card-row__controls card-row__controls--secondary flex-align-items-end--sm'>
          {
            actions !== '' ?
              <CardPopOverAction>
                {actions}
              </CardPopOverAction>
              : ''
          }
          {buttons}
        </div>
      </div>
    );
  }
}

ProfileCard.propTypes = propTypes;
ProfileCard.defaultProps = defaultProps;

/* eslint-disable no-unused-vars */
function mapStateToProps(state) {
  return {

  };
}
/* eslint-enable no-unused-vars */

export default connect(mapStateToProps)(ProfileCard);
