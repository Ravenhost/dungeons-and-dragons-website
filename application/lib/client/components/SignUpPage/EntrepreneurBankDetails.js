import React, { PropTypes }  from 'react';

export default function EntrepreneurBankDetails(props) {
  if (props.data.message) {
    let text = '';

    if (props.data.errors.email) {
      text = 'Пользователь с указанным e-mail уже зарегистрирован.';
    } else {
      text = props.data.message;
    }
    return (
      <div className='panel panel--body panel--small mb30'>
        <dl>
          <dt>Ошибка при проверке реквизитов ИП</dt>
          <dd className='panel-field'>{text}</dd>
        </dl>
      </div>
    );
  }

  const registerDate = new Date(props.data.registerDate).toLocaleString();

  return (
    <div className='panel panel--body panel--small mb30'>
      <dl>
        <dt>Официальное наименование ИП</dt>
        <dd className='panel-field'>{props.data.legalName}</dd>
      </dl>
      <dl>
        <dt>ИНН</dt>
        <dd className='panel-field'>{props.data.inn}</dd>
      </dl>
      <dl>
        <dt>Адрес</dt>
        <dd className='panel-field'>{props.data.legalAddress}</dd>
      </dl>
      <dl>
        <dt>Дата регистрации ИП</dt>
        <dd className='panel-field'>{registerDate}</dd>
      </dl>
      <dl>
        <dt>ОКВЭД</dt>
        <dd className='panel-field'>{props.data.okvd}</dd>
      </dl>
    </div>
  );
}

EntrepreneurBankDetails.propTypes = {
  data: PropTypes.object
};
