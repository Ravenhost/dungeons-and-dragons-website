import React, { Component, PropTypes }  from 'react';
import { connect }                      from 'react-redux';
import { checkBankDetails, signUp }     from '../../redux/actions/signUpActions';
import * as Constants                   from '../../config/constants';
import EntrepreneurBankDetails          from './EntrepreneurBankDetails';
import CompanyBankDetails               from './CompanyBankDetails';

const defaultProps = {
  signUp: {
    ogrn: '',
    bankDetails: {
      isFetching: false,
      fetchingError: '',
      isFetched: false,
      data: {}
    },
    signingUp: {
      isInProgress: false,
      isFetched: false,
      errorMsg: '',
      data: {}
    }
  },
  userType: Constants.USER_TYPE_COMPANY,
  dispatch: () => {
  }
};

const propTypes = {
  signUp: PropTypes.object,
  userType: PropTypes.string,
  dispatch: PropTypes.func
};

@connect(state => ({ signUp: state.signUp }))
export default class SignUpPage extends Component {
  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleCheckBankDetailsClick = this.handleCheckBankDetailsClick.bind(this);
    this.handleSignUpClick = this.handleSignUpClick.bind(this);

    this.state = {
      userType: Constants.USER_TYPE_COMPANY,
      email: '',
      password: '',
      repeatPassword: '',
      ogrn: '',
      isAgreed: false
    };
  }

  componentDidMount() {
    this.setupValidation();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  /**
   * Обработчик кнопки 'Проверить реквизиты'
   */
  handleCheckBankDetailsClick(e) {
    e.preventDefault();
    if (!$('#signUpForm').valid()) {
      return;
    }

    const userType = this.state.userType;
    const email = this.state.email;
    const password = this.state.password;
    const ogrn = this.state.ogrn;

    this.props.dispatch(checkBankDetails(userType, email, password, ogrn));
  }

  /**
   * Обработчик кнопки 'Зарегистрироваться'
   */
  handleSignUpClick(event) {
    event.preventDefault();
    if (!$('#signUpForm').valid()) {
      return;
    }

    const userType = this.state.userType;
    const email = this.state.email;
    const password = this.state.password;
    const firstName = this.state.firstName;
    const middleName = this.state.middleName;
    const lastName = this.state.lastName;
    const ogrn = this.state.ogrn;
    const brand = this.state.brandName;

    const data = {
      userType,
      email,
      password,
      firstName,
      middleName,
      lastName,
      ogrn,
      brand
    };

    this.props.dispatch(signUp(data));
  }

  setupValidation() {
    $('#signUpForm').validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          passwordCheck: true
        },
        repeatPassword: {
          equalTo: '#password'
        },
        ogrn: {
          required: true,
          minlength: function style() {
            if (this.state.userType === Constants.USER_TYPE_ENTREPRENEUR) {
              return 15;
            }
            return 13;
          }.bind(this),
          maxlength: function style() {
            if (this.state.userType === Constants.USER_TYPE_ENTREPRENEUR) {
              return 15;
            }
            return 13;
          }.bind(this)
        },
        lastName: {
          required: true
        },
        firstName: {
          required: true
        }
      },
      messages: {
        email: 'Укажите корректный e-mail, например, person@mail.ru',
        repeatPassword: {
          notEqualTo: '123'
        },
        ogrn: {
          minlength: function style(d) {
            return `ОГРН: ${d} знаков`;
          },
          maxlength: function style(d) {
            return `ОГРН: ${d} знаков`;
          }
        },
        password: {
          passwordCheck: function style() {
            return 'Пароль должен содержать не менее 8 символов, одну цифру, одну прописную и одну строчную буквы.';
          }
        }
      },
      errorPlacement: function style(label, element) {
        label.css('color', 'red');
        label.insertAfter(element);
      }
    });

    $.validator.addMethod('passwordCheck', (value) => {
      return /[a-z]/.test(value)
        && /\d/.test(value)
        && /[A-Z]/.test(value)
        && value.length >= 8;
    });
  }

  /**
   * Вывод полей специфичных только для пользователя типа: 'Физическое лицо'
   * @returns {XML}
   */
  personFields = () => {
    return (
      <div>
        <label>Фамилия*
          <input
            id='lastName'
            type='text'
            name='lastName'
            value={this.state.lastName}
            onChange={this.handleInputChange}
            data-msg-required='Это поле обязательно для заполнения'
          />
        </label>

        <label>Имя*
          <input
            type='text'
            name='firstName'
            value={this.state.firstName}
            onChange={this.handleInputChange}
            data-msg-required='Это поле обязательно для заполнения'
          />
        </label>

        <label>Отчество
          <input
            type='text'
            name='middleName'
            value={this.state.middleName}
            onChange={this.handleInputChange}
          />
        </label>
      </div>
    );
  };

  /**
   * Вывод поля 'ОГРН / ОГРНИП'
   * @returns {XML}
   */
  ogrnField = () => {
    const text = this.state.userType === Constants.USER_TYPE_COMPANY ? 'ОГРН' : 'ОГРНИП';

    return (
      <label>{text}
        <input
          id='ogrn'
          type='text'
          name='ogrn'
          value={this.state.ogrn}
          onChange={this.handleInputChange}
          data-msg-required='Это поле обязательно для заполнения'
        />
      </label>
    );
  };
  /**
   * Вывод поля 'Бренд'
   * @returns {XML}
   */
  brandNameFields = () => {
    return (
      <label>Бренд
        <input
          id='brandName'
          type='text'
          name='brandName'
          value={this.state.brandName}
          onChange={this.handleInputChange}
        />
      </label>
    );
  };

  checkBankDetailsButton = () => {
    return (
      <div className='text-right mb30'>
        <button
          className='btn btn-simple'
          type='button'
          onClick={this.handleCheckBankDetailsClick}
        >
          Проверить реквизиты
        </button>
      </div>);
  };

  renderErrors = (text) => {
    return (<div className='alert alert-error alert-dismissible' role='alert'>
      <button type='button' className='close'/>
      <strong>{text}</strong>
    </div>);
  };

  render() {
    let errors = '';
    let hasErrors = false;
    const type = this.state.userType;
    const isBankDetailsChecked = this.props.signUp.bankDetails.isFetched;
    const bankDetails = this.props.signUp.bankDetails.data;

    if (this.props.signUp.signingUp.isFetched) {
      if (this.props.signUp.signingUp.data.message ===
        'Регистрация прошла успешно. Вам отправлено письмо на подтверждения e-mail.') {
        return (<div className='row'>
          <div className='col-sm-8 col-sm-offset-2'>
            <div className='panel panel--white mtb20'>
              <h1>Регистрация</h1>
              <p>Успешная регистрация! Активируйте учётную запись используя ссылку в Вашей почте.</p>
            </div>
          </div>
        </div>);
      }

      if (this.props.signUp.signingUp.data.message) {
        hasErrors = true;
        errors = this.props.signUp.signingUp.data.message;
      }

      const signUpErrors = this.props.signUp.signingUp.data.errors;

      if (signUpErrors) {
        hasErrors = true;
        errors = '';
        for (const key in signUpErrors) {
          if (signUpErrors.hasOwnProperty(key)) {
            errors = errors + signUpErrors[key].msg;
          }
        }
      }
    }

    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm-8 col-sm-offset-2'>
            <div className='panel panel--white mtb20'>
              <h1>Регистрация</h1>

              {hasErrors && this.renderErrors(errors)}

              <form id='signUpForm' name='signUpForm'>
                <label>Тип пользователя
                  <select name='userType' value={this.state.type} onChange={this.handleInputChange}>
                    <option value={Constants.USER_TYPE_COMPANY}>Организация</option>
                    <option value={Constants.USER_TYPE_ENTREPRENEUR}>ИП</option>
                    <option value={Constants.USER_TYPE_PERSON}>Физическое лицо</option>
                  </select>
                </label>

                <label>E-mail*
                  <input
                    id='email'
                    type='email'
                    name='email'
                    value={this.state.email}
                    onChange={this.handleInputChange}
                    required
                  />
                </label>

                <label>Пароль*
                  <input
                    id='password'
                    type='password'
                    name='password'
                    value={this.state.password}
                    onChange={this.handleInputChange}
                    data-msg-required='Это поле обязательно для заполнения'
                    required
                  />
                </label>

                <label>Подтвердите пароль*
                  <input
                    id='repeatPassword'
                    type='password'
                    name='repeatPassword'
                    value={this.state.repeatPassword}
                    onChange={this.handleInputChange}
                    data-msg-required='Это поле обязательно для заполнения'
                    data-msg-equalTo='Пароли должны совпадать'
                    required
                  />
                </label>

                { type === Constants.USER_TYPE_PERSON && this.personFields() }

                { type !== Constants.USER_TYPE_PERSON && this.ogrnField() }


                { type !== Constants.USER_TYPE_PERSON && this.checkBankDetailsButton() }

                { type === Constants.USER_TYPE_COMPANY
                && isBankDetailsChecked
                && <CompanyBankDetails data={bankDetails}/> }

                { type === Constants.USER_TYPE_ENTREPRENEUR
                && isBankDetailsChecked
                && <EntrepreneurBankDetails data={bankDetails}/> }

                <div className='text-right'>
                  <div className='checkbox mb10'>
                    <input
                      id='isAgreed'
                      name='isAgreed'
                      type='checkbox'
                      checked={this.state.isAgreed}
                      onChange={this.handleInputChange}
                    />
                    <label className='width-auto' htmlFor='isAgreed'>Согласен с условиями <a href='#'>
                      пользовательского соглашения</a></label>
                  </div>

                  <button
                    disabled={!this.state.isAgreed}
                    className='btn btn-transparent-primary'
                    onClick={this.handleSignUpClick}
                  >
                    Зарегистрироваться
                  </button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SignUpPage.propTypes = propTypes;
SignUpPage.defaultProps = defaultProps;
