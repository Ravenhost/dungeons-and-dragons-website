import React, { Component, PropTypes }  from 'react';
import pathAPI from '../../redux/pathAPI';

const defaultProps = {
  dispatch: () => {
  }
};

const propTypes = {
  dispatch: PropTypes.func
};

export default class RecoverPasswordPage extends Component {
  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleRecoverClick = this.handleRecoverClick.bind(this);

    this.state = {
      isFetched: false,
      isFetching: false,
      hasError: false,
      email: ''
    };
  }

  componentDidMount() {
    $('#recoverForm').validate({
      rules: {
        email: {
          required: true,
          email: true
        }
      },
      messages: {
        email: 'Укажите корректный e-mail, например, person@mail.ru'
      }
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }


  /**
   * Обработчик кнопки "Войти"
   */
  handleRecoverClick(event) {
    event.preventDefault();
    if (!$('#recoverForm').valid()) {
      return;
    }
    let status = 0;
    const email = this.state.email;

    this.setState({ isFetching: true, hasError: false });

    return fetch(`${pathAPI}/auth/password/forgot`, {
      method: 'PUT',
      headers: new Headers({ 'content-type': 'application/json' }),
      body: JSON.stringify({ email }),
      credentials: 'same-origin'
    }).then(response => {
      status = response.status;
      return response.json();
    }).then((json) => {
      if (status === 200) {
        this.setState({ isFetched: true, hasError: false, isInProgress: false });
      } else {
        this.setState({ isFetched: true, hasError: true, errorMessage: json.message, isInProgress: false });
      }
    });
  }

  renderErrors = (text) => {
    return (<div className='alert alert-error alert-dismissible' role='alert'>
      <button type='button' className='close'/>
      <strong>{text}</strong>
    </div>);
  };

  render() {
    if (this.state.isFetched && !this.state.hasError) {
      return (
        <div className='container'>
          <div className='row'>
            <div className='col-sm-6 col-sm-offset-3'>
              <div className='panel panel--white mtb30'>
                <h1>Восстановление пароля</h1>
                <p>Мы отправили на ваш e-mail письмо с инструкциями по восстановлению пароля.</p>
              </div>
            </div>
          </div>
        </div>);
    }

    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm-6 col-sm-offset-3'>
            <div className='panel panel--white mtb30'>
              <h1>Восстановление пароля</h1>

              {this.state.hasError && this.renderErrors(this.state.errorMessage)}

              <p>Укажите ваш электронный адрес и мы вышлем на него код для восстановления пароля</p>

              <form id='recoverForm' name='recoverForm'>

                <label>E-mail
                  <input
                    type='email'
                    name='email'
                    required
                    value={this.state.email}
                    onChange={this.handleInputChange}
                  />
                </label>

                <div className='text-right'>
                  <button
                    className='btn btn-transparent-primary'
                    onClick={this.handleRecoverClick}
                  >
                    Отправить код для восстановления пароля
                  </button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

RecoverPasswordPage.propTypes = propTypes;
RecoverPasswordPage.defaultProps = defaultProps;
