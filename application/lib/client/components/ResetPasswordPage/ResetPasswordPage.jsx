import React, { Component, PropTypes }  from 'react';
import pathAPI from '../../redux/pathAPI';

const defaultProps = {
  dispatch: () => {
  }
};

const propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.object
};

export default class ResetPasswordPage extends Component {
  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleResetPasswordClick = this.handleResetPasswordClick.bind(this);

    this.state = {
      token: '',
      password1: '',
      password2: '',
      isFetched: false,
      hasError: false,
      errorMessage: ''
    };
  }

  componentDidMount() {
    this.setupValidation();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }


  /**
   * Обработчик кнопки "Войти"
   */
  handleResetPasswordClick(event) {
    event.preventDefault();
    if (!$('#resetForm').valid()) {
      return;
    }

    let status = 0;
    const token = this.props.params.token;
    const password = this.state.password1;
    const data = { token, password };

    this.setState({ isInProgress: true });

    return fetch(`${pathAPI}/auth/password/change`, {
      method: 'PUT',
      headers: new Headers({ 'content-type': 'application/json' }),
      body: JSON.stringify(data),
      credentials: 'same-origin'
    }).then(response => {
      status = response.status;
      return response.json();
    }).then((json) => {
      if (status === 200) {
        this.setState({ isFetched: true, hasError: false, isInProgress: false });
      } else {
        this.setState({ isFetched: true, hasError: true, errorMessage: json.message, isInProgress: false });
      }
    });
  }

  setupValidation() {
    $('#resetForm').validate({
      rules: {
        password1: {
          required: true,
          passwordCheck: true
        },
        password2: {
          equalTo: '#password1',
          required: true
        }
      },
      messages: {
        password1: {
          required: 'Это поле обязательно для заполнения'
        },
        password2: {
          required: 'Это поле обязательно для заполнения',
          equalTo: 'Пароли должны совпадать'
        }
      },
      errorPlacement: function style(label, element) {
        label.css('color', 'red');
        label.insertAfter(element);
      }
    });

    $.validator.addMethod('passwordCheck', (value) => {
      return /[a-z]/.test(value)
        && /\d/.test(value)
        && /[A-Z]/.test(value)
        && value.length >= 8;
    });
  }

  renderErrors = (text) => {
    return (<div className='alert alert-error alert-dismissible' role='alert'>
      <button type='button' className='close'/>
      <strong>{text}</strong>
    </div>);
  };

  render() {
    if (this.state.isFetched && !this.state.hasError) {
      return (
        <div className='container'>
          <div className='row'>
            <div className='col-sm-6 col-sm-offset-3'>
              <div className='panel panel--white mtb30'>
                <h1>Установка нового пароля</h1>
                <p>Пароль успешно изменён.</p>
              </div>
            </div>
          </div>
        </div>);
    }

    return (
      <div className='container'>
        <div className='row'>
          <div className='col-sm-6 col-sm-offset-3'>
            <div className='panel panel--white mtb30'>
              <h1>Установка нового пароля</h1>

              {this.state.hasError && this.renderErrors(this.state.errorMessage)}

              <form name='resetForm' id='resetForm'>

                <label>Пароль
                  <input
                    id='password1'
                    type='password'
                    name='password1'
                    required
                    value={this.state.password1}
                    onChange={this.handleInputChange}
                  />
                </label>

                <label>Повторите пароль
                  <input
                    id='password2'
                    type='password'
                    name='password2'
                    required
                    value={this.state.password2}
                    onChange={this.handleInputChange}
                  />
                </label>

                <div className='text-right'>
                  <button
                    className='btn btn-transparent-primary'
                    onClick={this.handleResetPasswordClick}
                  >
                    Установить новый пароль
                  </button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ResetPasswordPage.propTypes = propTypes;
ResetPasswordPage.defaultProps = defaultProps;
