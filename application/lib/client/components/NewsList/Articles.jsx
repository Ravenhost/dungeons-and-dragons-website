import React, { Component, PropTypes }  from 'react';
import { Article }                      from '../NewsList';

const propTypes = {
  category: PropTypes.string,
  list: PropTypes.array,
  page: PropTypes.number,
  loaded: PropTypes.bool,
  loading: PropTypes.bool
};

const defaultProps = {
  category: '',
  list: [],
  page: 1,
  loaded: false,
  loading: false
};

export default class Articles extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className='content'>
        <div className='active' id={`news-${this.props.category}`}>
          {this.props.list.map((article, i) => {
            return (
              <Article key={i} {...article} />
            );
          })}
        </div>
      </div>
    );
  }
}

Articles.propTypes = propTypes;
Articles.defaultProps = defaultProps;
