import React, { Component, PropTypes }  from 'react';
import { asyncConnect }                 from 'redux-connect';
import { isBrowser, isLoaded }          from '../../redux/utils/helpers';
import { fetchNews }                    from '../../redux/actions/newsActions';
import { Articles }                     from '../NewsList';

import {
  Grid,
  Row,
  Col,
  Tabs,
  Tab
}                                       from 'react-bootstrap';


const defaultProps = {
  news: {
    main: {
      list: [],
      page: 1,
      end: false
    },
    economy: {
      list: [],
      page: 1,
      end: false
    },
    politics: {
      list: [],
      page: 1,
      end: false
    },
    sidebar: {
      list: [],
      page: 1,
      end: false
    }
  },
  category: '',
  loading: false,
  perPage: 5,
  dispatch: () => {
  }
};

const propTypes = {
  news: PropTypes.object,
  category: PropTypes.string,
  loading: PropTypes.bool,
  perPage: PropTypes.number,
  dispatch: PropTypes.func
};

const asyncPromises = [
  {
    key: 'news.main',
    promise: ({ store }) => {
      if (!isBrowser()) {
        const state = store.getState();

        if (!isLoaded(state, 'news')) {
          return store.dispatch(fetchNews('main', 1, defaultProps.perPage));
        }
      }

      return null;
    }
  },
  {
    key: 'news.economy',
    promise: ({ store }) => {
      if (!isBrowser()) {
        const state = store.getState();

        if (!isLoaded(state, 'news')) {
          return store.dispatch(fetchNews('economy', 1, defaultProps.perPage));
        }
      }

      return null;
    }
  },
  {
    key: 'news.politics',
    promise: ({ store }) => {
      if (!isBrowser()) {
        const state = store.getState();

        if (!isLoaded(state, 'news')) {
          return store.dispatch(fetchNews('politics', 1, defaultProps.perPage));
        }
      }

      return null;
    }
  }
];

@asyncConnect(asyncPromises, state => ({ news: state.news }))
export default class NewsList extends Component {
  constructor(props) {
    super(props);
    this.handleWindowScroll = this.handleWindowScroll.bind(this);
    this.handleCategorySelect = this.handleCategorySelect.bind(this);
    this.handleWindowResize = this.handleWindowResize.bind(this);

    this.state = {
      currentCategory: 'main',
      activeTab: 1,
      showSocial: false
    };
  }

  componentDidMount() {
    if (typeof document !== 'undefined') {
      document.addEventListener('scroll', this.handleWindowScroll);
      window.addEventListener('resize', this.handleWindowResize);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleWindowScroll);
    window.removeEventListener('resize', this.handleWindowResize);
  }

  handleWindowScroll() {
    const footerPositionBottom = document.getElementsByClassName('uou-block-4a')[0].getBoundingClientRect().bottom;
    const windowHeight = document.documentElement.clientHeight;

    if (parseInt(footerPositionBottom - windowHeight, 10) === 0) {
      this.props.dispatch(fetchNews(
        this.state.currentCategory,
        this.props.news[this.state.currentCategory].page + 1,
        this.props.perPage
      ));
    }
  }

  handleWindowResize() {
    if (document.documentElement.offsetWidth >= 991 && this.state.showSocial) {
      this.setState({
        currentCategory: 'main',
        activeTab: 1,
        showSocial: false
      });
    }
  }

  handleCategorySelect(key) {
    let category = this.state.currentCategory;
    let activeTab;
    let showSocial = false;

    switch (key) {
      case 1:
        category = 'main';
        activeTab = 1;
        break;
      case 2:
        category = 'economy';
        activeTab = 2;
        break;
      case 3:
        category = 'politics';
        activeTab = 3;
        break;
      case 4:
        showSocial = true;
        activeTab = 4;
        break;
      case 5:
        category = 'sidebar';
        activeTab = 5;
        break;
      default:
        category = 'main';
        activeTab = 1;
        break;
    }

    if (key !== 4) {
      this.props.dispatch(fetchNews(category, this.props.news[category].page, this.props.perPage));
    }

    this.setState({
      currentCategory: category,
      activeTab,
      showSocial
    });
  }

  handleShowSocialNews() {
    this.setState({
      showSocial: true,
      activeTab: 4
    });
  }

  render() {
    // const mobile = (typeof document !== 'undefined' && document.documentElement.offsetWidth <= 991);

    return (
      <div className='mt20'>
        <Grid>

          {/* <Banner /> */}

          <Row>

            <Col xs={12} md={8}>
              <div className='main-tabs'>

                <Tabs id='categories'
                  activeKey={this.state.activeTab}
                  animation={false}
                  onSelect={this.handleCategorySelect}
                >
                  <Tab eventKey={1}
                    tabClassName='main-tabs__tab-item'
                    title={(
                      <h5 className='main-tabs__header mtb5'>
                        <i className='icofont icofont-globe' />
                        <span className={this.state.currentCategory !== 'main' ||
                        this.state.showSocial ? 'hidden-xs hidden-sm' : ''}
                        > Главные</span>
                      </h5>
                  )}
                  >
                    <Articles category='main' {...this.props.news.main} />
                  </Tab>
                  <Tab eventKey={2}
                    tabClassName='main-tabs__tab-item'
                    title={(
                      <h5 className='main-tabs__header mtb5'>
                        <i className='icofont icofont-chart-arrows-axis' />
                        <span className={this.state.currentCategory !== 'economy' ||
                        this.state.showSocial ? 'hidden-xs hidden-sm' : ''}
                        > Экономика</span>
                      </h5>
                  )}
                  >
                    <Articles category='economy' {...this.props.news.economy} />
                  </Tab>
                  <Tab eventKey={3}
                    tabClassName='main-tabs__tab-item'
                    title={(
                      <h5 className='main-tabs__header mtb5'>
                        <i className='icofont icofont-lawyer-alt-1' />
                        <span className={this.state.currentCategory !== 'politics' ||
                        this.state.showSocial ? 'hidden-xs hidden-sm' : ''}
                        > Политика</span>
                      </h5>
                  )}
                  >
                    <Articles category='politics' {...this.props.news.politics} />
                  </Tab>
                  <Tab eventKey={4}
                    tabClassName='main-tabs__tab-item is-hidden-until-md'
                    title={(
                      <h5 className='main-tabs__header mtb5'>
                        <i className='icofont icofont-coins' />
                        <span className={this.state.currentCategory !== 'sidebar' ||
                        this.state.showSocial ? 'hidden-xs hidden-sm' : ''}
                        > Новости ProFeels</span>
                      </h5>
                  )}
                  >
                    <div className={`content ${this.state.showSocial ? '' : 'hidden'}`}>
                      <div className='active' id={`news-${this.props.category}`}>
                        <article className='uou-block-7i profile-block'>
                          <div className='feed__header feed-header feed-header--borderless'>
                            <div className='feed-header__image'>
                              <div className='icon'>
                                <img src='/images/news-icon1.jpg' alt=''/>
                              </div>
                            </div>
                            <div className='feed-header__body'>
                              <a href='#' className='feed-header__caption'>
                              Заголовок новости довольно длинный, аж на 2 строки!</a>
                              <a href='#' className='feed-header__subheader'>Группа компаний “СибЭнерго”</a>
                              <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                            </div>
                          </div>
                          <img className='image' src='/images/news5.png' alt='' />
                        </article>
                        <article className='uou-block-7i profile-block'>
                          <div className='feed__header feed-header feed-header--borderless'>
                            <div className='feed-header__image'>
                              <div className='icon'>
                                <img src='/images/news-icon1.jpg' alt=''/>
                              </div>
                            </div>
                            <div className='feed-header__body'>
                              <a href='#' className='feed-header__caption'>
                              Заголовок новости довольно длинный, аж на 2 строки!</a>
                              <a href='#' className='feed-header__subheader'>Группа компаний “СибЭнерго”</a>
                              <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                            </div>
                          </div>
                          <img className='image' src='/images/news6.png' alt='' />
                        </article>
                        <article className='uou-block-7i profile-block'>
                          <div className='feed__header feed-header feed-header--borderless'>
                            <div className='feed-header__image'>
                              <div className='icon'>
                                <img src='/images/news-icon1.jpg' alt=''/>
                              </div>
                            </div>
                            <div className='feed-header__body'>
                              <a href='#' className='feed-header__caption'>
                              Заголовок новости довольно длинный, аж на 2 строки!</a>
                              <a href='#' className='feed-header__subheader'>Группа компаний “СибЭнерго”</a>
                              <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                            </div>
                          </div>
                          <img className='image' src='/images/news7.png' alt='' />
                        </article>
                      </div>
                    </div>

                  </Tab>
                </Tabs>
              </div>
            </Col>

            <Col xs={12}
              md={4}
              xsHidden
              smHidden
            >
              <div className='main-tabs main-tabs--is-single'>
                <ul className='nav nav-tabs'>
                  <li className=''>
                    <a href='#' className='pl0'>
                      <h5 className='main-tabs__header mtb5'>
                        <i className='icofont icofont-coins' />
                        <span className={this.state.currentCategory !== 'main' ||
                        this.state.showSocial ? 'hidden-xs hidden-sm' : ''}
                        > Новости ProFeels</span>
                      </h5>
                    </a>
                  </li>
                </ul>
                <div className='content'>
                  <div className='active' id=''>
                    <article className='uou-block-7i profile-block'>
                      <div className='feed__header feed-header feed-header--borderless'>
                        <div className='feed-header__image'>
                          <div className='icon'>
                            <img src='/images/news-icon1.jpg' alt=''/>
                          </div>
                        </div>
                        <div className='feed-header__body'>
                          <a href='#' className='feed-header__caption'>
                          Заголовок новости довольно длинный, аж на 2 строки!</a>
                          <a href='#' className='feed-header__subheader'>Группа компаний “СибЭнерго”</a>
                          <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                        </div>
                      </div>
                      <img className='image' src='/images/news5.png' alt='' />
                    </article>
                    <article className='uou-block-7i profile-block'>
                      <div className='feed__header feed-header feed-header--borderless'>
                        <div className='feed-header__image'>
                          <div className='icon'>
                            <img src='/images/news-icon1.jpg' alt=''/>
                          </div>
                        </div>
                        <div className='feed-header__body'>
                          <a href='#' className='feed-header__caption'>
                          Заголовок новости довольно длинный, аж на 2 строки!</a>
                          <a href='#' className='feed-header__subheader'>Группа компаний “СибЭнерго”</a>
                          <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                        </div>
                      </div>
                      <img className='image' src='/images/news6.png' alt='' />
                    </article>
                    <article className='uou-block-7i profile-block'>
                      <div className='feed__header feed-header feed-header--borderless'>
                        <div className='feed-header__image'>
                          <div className='icon'>
                            <img src='/images/news-icon1.jpg' alt=''/>
                          </div>
                        </div>
                        <div className='feed-header__body'>
                          <a href='#' className='feed-header__caption'>
                          Заголовок новости довольно длинный, аж на 2 строки!</a>
                          <a href='#' className='feed-header__subheader'>Группа компаний “СибЭнерго”</a>
                          <time className='feed-header__time' dateTime=''>12 мар в 17.42</time>
                        </div>
                      </div>
                      <img className='image' src='/images/news7.png' alt='' />
                    </article>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

NewsList.propTypes = propTypes;
NewsList.defaultProps = defaultProps;
