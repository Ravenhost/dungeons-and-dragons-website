import React, { Component, PropTypes } from 'react';
import moment from 'moment';

const propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  pubDate: PropTypes.string,
  link: PropTypes.string,
  source: PropTypes.object,
  image: PropTypes.object
};

const defaultProps = {
  title: '',
  description: '',
  pubDate: '',
  link: '',
  source: {},
  image: ''
};

export default class Article extends Component {
  constructor(props) {
    super(props);
  }
  removeQuotes(str) {
    return str.replace(/&raquo;/g, '»').replace(/&laquo;/g, '«');
  }
  render() {
    // eslint-disable-next-line react/no-danger
    const description = <p dangerouslySetInnerHTML={{ __html: this.removeQuotes(this.props.description) }} />;

    return (
      <article className='feed-external quoted'>
        <div className='feed-external__container'>
          <img src={`/api/images/${this.props.image._id}`} alt='' className='feed-external__thumb' />
          <div className='feed-external__body'>
            <span className='quoted__holder' />
            <div className='feed-external__meta'>
              <span className='feed-external__time-ago'>{moment(this.props.pubDate).format('DD.MM.YYYY hh:mm')}</span>
              <span className='feed-external__category'><a href={this.props.source.link}>{this.props.source.title}</a></span>
            </div>
            <h6 className='feed-external__header'>{this.removeQuotes(this.props.title)}</h6>
            {description}
          </div>
        </div>
        <div className='feed-external__link'>
          <a href={this.props.link} className='#'>Читать целиком в первоисточнике</a>
        </div>
        <hr className='feed-external__hr' />
      </article>
    );
  }
}

Article.propTypes = propTypes;
Article.defaultProps = defaultProps;
