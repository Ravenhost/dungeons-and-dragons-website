const mongoose = require('mongoose');

const projectSchema = new mongoose.Schema({
  owner:            { type: mongoose.Schema.Types.ObjectId,                       required: true},
  name:             { type: String,                                               required: true },
  image:            { type: String },
  description:      { type: String },
  commentsLevel:    { type: Number,                         'default': 1,         required: true },
  isHidden:         { type: Boolean,                        'default': false,     required: true },
  archivedBy:       { type: String },
  archivedDate:     { type: Date },
  createdDate:      { type: Date,                           'default': Date.now,  required: true },
  modifiedDate:     { type: Date,                           'default': Date.now,  required: true },
  modifiedBy:       { type: String }
}, {
  strict:       true,
  versionKey:   false,
  collection:   'projects'
});

module.exports.projectSchema = mongoose.model('Project', projectSchema);
