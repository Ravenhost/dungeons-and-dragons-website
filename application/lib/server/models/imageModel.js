const mongoose = require('mongoose');

const imageSchema = new mongoose.Schema({
  originalName:   { type: String,                       required: true },
  contentType:    String,
  width:          Number,
  height:         Number,
  storageId:      mongoose.Schema.Types.ObjectId, // Идентификатор файла в GridFS
  uploadStatus:   { type: String, 'default': 'open',    required: true, 'enum': ['open', 'upload', 'close'] },
  processStatus:  { type: String, 'default': 'pending', required: true, 'enum': ['pending', 'processing', 'completed'] },
  createdBy:      { type: String, 'default': 'default', required: true },
  createDate:     { type: Date,   'default': Date.now,  required: true },
  modifyDate:     { type: Date,   'default': Date.now,  required: true },
  removeDate:     Date
}, {
  strict:     true,
  collection: 'images',
  versionKey: false
});

module.exports.Image = mongoose.model('Image', imageSchema);
