'use strict';

const mongoose = require('mongoose');

const spellSchema = new mongoose.Schema({
  classColor: {type: String, required: true},
  className: {type: String, required: true},
  spellTitle: {type: String, required: true},
  spellType: {type: String, required: true},
  castingTimeValue: {type: String, required: true},
  rangeValue: {type: String, required: true},
  componentsValue: {type: String, required: true},
  durationValue: {type: String, required: true},
  spellDescription: {type: String, required: true},
  createBy: String,
  modifyBy: String,
  createDate: {type: Date, 'default': Date.now, required: true},
  modifyDate: {type: Date, 'default': Date.now, required: true},
  removeDate: Date
}, {
  strict: true,
  versionKey: false,
  collection: 'spells'
});

exports.Spell = mongoose.model('Spell', spellSchema);
