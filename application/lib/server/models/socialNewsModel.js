const mongoose = require('mongoose');

const socialNewsSchema = new mongoose.Schema({
  owner: {
    _id:    { type: mongoose.Schema.Types.ObjectId, required: true },
    name:   { type: String,                         required: true },
    avatar: { type: String,                         required: true }
  },
  title:          { type: String },
  text:           { type: String },
  images:         [ String ],
  sourceId:       { type: mongoose.Schema.Types.ObjectId, ref: 'SocialNews' }, // repost
  description:    { type: String },
  datePublished:  { type: Date,                           required: true, 'default': Date.now },
  showInUserMain: { type: Boolean,                        required: true, 'default': true },
  dateModified:   { type: Date,                           required: true, 'default': Date.now },
  modifiedBy:     String,
  likesCount:     { type: Number,                         required: true, 'default': 0 },
  repostsCount:   { type: Number,                         required: true, 'default': 0 },
  isHiddenComment:{ type: Boolean,                        required: true, 'default': false },
  likes: [{
    _id:          mongoose.Schema.Types.ObjectId,
    name:         String,
    avatar:       { type: String },
    dateCreated:  { type: Date, 'default': Date.now }
  }],
  reposts: [{
    _id:          mongoose.Schema.Types.ObjectId,
    name:         String,
    avatar:       { type: String }
  }],
  archivedDate:   Date,
  archivedBy:     String,
  files:          [ String ],
  project:        mongoose.Schema.Types.ObjectId,
  inMaking:       { type: Boolean,                        required: true, 'default': true },
  hashtag:        { type: String }
}, {
  strict:     true,
  versionKey: false,
  collection: 'social-news'
});

socialNewsSchema.index(
  { title: 'text', text: 'text', hashtag: 'text'},
  {
    weights: {
      title: 10, text: 5, hashtag: 10
    },
    default_language: 'russian',
    background: true
  }
);

module.exports.SocialNews = mongoose.model('SocialNews', socialNewsSchema);
