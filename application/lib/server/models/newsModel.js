const mongoose = require('mongoose');

const newsItemSchema = new mongoose.Schema({
  title:        { type: String, required: true },
  description:  { type: String, required: true },
  image: {
    _id:    mongoose.Schema.Types.ObjectId,
    link:   String,
    title:  String
  },
  source: {
    title:  { type: String, required: true },
    link:   { type: String, required: true }
  },
  pubDate:      { type: Date,   required: true },
  link:         { type: String, required: true },
  category:     { type: String, required: true },
  modifyBy:     String,
  createDate:   { type: Date,   required: true, 'default': Date.now },
  modifyDate:   { type: Date,   required: true, 'default': Date.now },
  removeDate:   Date
}, {
  strict:     true,
  versionKey: false,
  collection: 'news'
});

module.exports.NewsItem = mongoose.model('NewsItem', newsItemSchema);
