const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  login: { type: String, required: true },
  email: { type: String, required: false },
  password: { type: String, required: true },
  token: { type: String, required: false },
  userData: { type: Object, required: false }
}, {
  strict: true,
  versionKey: false,
  collection: 'users'
});

module.exports.userSchema = mongoose.model('User', userSchema);
