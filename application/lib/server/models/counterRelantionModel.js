const mongoose = require('mongoose');

const counterSchema = new mongoose.Schema({
  user:          { type: mongoose.Schema.Types.ObjectId, require: true, ref: 'User' },
  partners:      { type: Number, required: true, 'default': 0 },
  incoming:      { type: Number, required: true, 'default': 0 },
  outgoing:      { type: Number, required: true, 'default': 0 },
  subscriptions: { type: Number, required: true, 'default': 0 },
  followers:     { type: Number, required: true, 'default': 0 }
}, {
  strict:     true,
  versionKey: false,
  collection: 'counters'
});

module.exports.CounterRelation = mongoose.model('CounterRelation', counterSchema);
