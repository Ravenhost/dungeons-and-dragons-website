const mongoose = require('mongoose');

const relationsSchema = new mongoose.Schema({
  from:     { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
  fromName: { type: String,                         required: true },
  to:       { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
  toName:   { type: String,                         required: true },
  date:     { type: Date,                           required: true, 'default': Date.now },
  type:     { type: Number, 'enum': [0, 1, 2],      required: true, 'default': 0 }, // 0 = 'subscription', 1 = 'partnerRequest', 2 = 'partners'
  viewed:   { type: Boolean,                        required: true, 'default': false }
}, {
  strict:       true,
  versionKey:   false,
  collection:   'relationships'
});

module.exports.relationsSchema = mongoose.model('Relationship', relationsSchema);
