// region Module dependencies.
const express     = require('express');
const passport    = require('passport');
const mongoose    = require('mongoose');
const VError      = require('verror');
const _           = require('lodash');
const Promise     = require('bluebird');

const acl         = require('../middlewares/acl');
const constants   = require('../constants');
const dataOptions = require('../middlewares/dataOptions').dataOptions;
const sendTask    = require('../tasksWorker').sendTask;

const User         = mongoose.model('User');
const Relationship = mongoose.model('Relationship');

const router = express.Router();
// endregion.

function subscribe(req, res, next) {
  const user = req.user;
  const anotherUserId = req.params._id;

  if (_.isEqual(user._id.toString(), anotherUserId)) {
    return res.responses.notFoundResource('Нельзя подписаться на себя.');
  }

  const request = {
    from: user._id,
    to:   anotherUserId,
    type: constants.USER_RELATION.subscription
  };

  const anotherUserPromise = User.findOne({
    _id:          anotherUserId,
    archivedDate: { $exists: false }
  }).lean();
  const relationshipsPromise = Relationship.findOne(request);

  return Promise.all([anotherUserPromise, relationshipsPromise])
    .spread((anotherUser, relation) => {
      if (!anotherUser) {
        return res.responses.notFoundResource('Пользователь не найден.');
      }
      if (anotherUser.type === constants.USER_TYPE.person) {
        return res.responses.requestError('На физическое лицо нельзя подписаться.');
      }
      if (relation) {
        return res.responses.requestError('Заявка на подписку уже была подана.');
      }

      if (user.type === constants.USER_TYPE.person) {
        request.fromName = `${user.personData.lastName} ${user.personData.firstName}`;
        request.fromName = user.personData.middleName ? `${request.fromName} ${user.personData.middleName}` : request.fromName;
      } else {
        request.fromName = user.companyData.legalName;
      }
      request.toName = anotherUser.companyData.legalName;

      return Relationship.create(request)
        .then(() => {
          sendTask('addSubscription', { userId: user._id, partnerId: anotherUserId });

          return res.responses.success();
      });
    })
    .catch( err => next(err instanceof Error ? err : new VError(err)) );
}

function unsubscribe(req, res, next) {
  const user = req.user;
  const anotherUserId = req.params._id;
  const request = {
    from: user._id,
    to:   anotherUserId,
    type: constants.USER_RELATION.subscription
  };

  return Relationship.findOneAndRemove(request).lean()
    .then((relation) => {
      if (!relation) {
        return res.responses.requestError('Такой заявки на подписку не существует.');
      }
      sendTask('deleteSubscription', { userId: user._id, partnerId: anotherUserId });

      return res.responses.success();
    })
    .catch( err => next(err instanceof Error ? err : new VError(err)) );
}

function getFollowers(req, res, next) {
  const user = req.user;
  const search = req.query.search;
  const conditions = {
    to:   user._id,
    type: constants.USER_RELATION.subscription
  };

  if (search) {
    conditions.fromName = new RegExp(`.*${_.escapeRegExp(search)}.*`, 'i');
  }

  return Relationship.find(conditions, { 'from': 1 }, req.dataOptions)
    .populate('from', {
      avatar:       1,
      type:         1,
      personData:   1,
      description:  1,
      'companyData.legalName':    1,
      'companyData.director':     1,
      'companyData.directorPost': 1
    })
    .lean()
    .then((result) => {
      const subscriptions = [];

      if (result.length > 0) {
        // data conversion
        result.forEach((relation) => {
          if (relation.from.type === constants.USER_TYPE.person) {
            delete relation.from.companyData; // empty field
          }
          subscriptions.push(relation.from);
        });
      }
      sendTask('viewedSubscriptions', { userId: user._id });

      return res.json(subscriptions);
    })
    .catch((err) => next(err instanceof Error ? err : new VError(err)));
}

function getSubscriptions(req, res, next) {
  const user = req.user;
  const search = req.query.search;
  const conditions = {
    from: user._id,
    type: constants.USER_RELATION.subscription
  };

  if (search) {
    conditions.toName = new RegExp(`.*${_.escapeRegExp(search)}.*`, 'i');
  }

  return Relationship.find(conditions, { 'to': 1 }, req.dataOptions)
    .populate('to', {
      avatar:      1,
      type:        1,
      description: 1,
      'companyData.legalName':    1,
      'companyData.director':     1,
      'companyData.directorPost': 1
    })
    .lean()
    .then((result) => {
      const subscriptions = [];

      if (result.length > 0) {
        // data conversion
        result.forEach((relation) => {
          subscriptions.push(relation.to);
        });
      }
      return res.json(subscriptions);
    })
    .catch((err) => next(err instanceof Error ? err : new VError(err)));
}

router.post(  '/:_id([0-9A-F]{24})', passport.authenticate('jwt', { session: false }),                    subscribe);
router.delete('/:_id([0-9A-F]{24})', passport.authenticate('jwt', { session: false }),                    unsubscribe);
router.get(   '/followers',          passport.authenticate('jwt', { session: false }), dataOptions({sort: '-date'}), acl.withoutPerson, getFollowers);
router.get(   '/',                   passport.authenticate('jwt', { session: false }), dataOptions({sort: '-date'}),                    getSubscriptions);

module.exports = router;
