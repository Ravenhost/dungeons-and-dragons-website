// region Module dependencies.
const express =   require('express');
const VError =    require('verror');
const mongoose =  require('mongoose');

const logger =    require('../logger');

const Image = mongoose.model('Image');

const router = express.Router();
// endregion

function getImageById(req, res, next) {
  const imageId = req.params.imageId;
  const conditions = {
    _id: imageId,
    removeDate: {$exists: false}
  };
  return Image.findOne(conditions)
    .then((image) => {
      if (!image) { return res.responses.notFoundResource(); }
      const gs = new mongoose.mongo.GridStore(mongoose.connection.db, image.storageId, 'r');
      return gs.open()
        .then((gsConnection) => {
          if (!gsConnection) { return res.responses.notFoundResource(); }
          const gsStream = gsConnection.stream();
          res.set('Content-Disposition', `inline; filename=${image.originalName}`);
          res.set('Content-Type', image.contentType);
          gsStream.on('error', (err) => {
            logger.error('Error: imagesController: Stream file error');
            return Promise.reject(new VError(err, 'Stream file error'));
          });
          return gsStream.pipe(res);
        });
    })
    .catch((err) => {
      const error = err instanceof Error ? err : new VError(err);
      return next(error);
    });
}

router.get('/:imageId([0-9A-F]{24})', getImageById);

module.exports = router;
