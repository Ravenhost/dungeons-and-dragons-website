// region Module dependencies.
const express =               require('express');
const VError =                require('verror');
const mongoose =              require('mongoose');
const Promise =               require('bluebird');
const passport =              require('passport');
const _ =                     require('lodash');
const path =                  require('path');
const moment =                require('moment');
const fs =                    require('fs');
//const jsonValidator =         require('jsonschema').validate;

const config =                require('../config');
const multer =                require('../services/multer');
const partnerService =        require('../services/partnerService');
const projectsService =       require('../services/projectService');
const validate =              require('../middlewares/validateSchema');
const usersValid =            require('../validators/userSchemas');
const imageService =          require('../services/imageService');
const linkGenerationService = require('../services/linkGenerationService');
const errorService =          require('../services/errorService');
const constants =             require('../constants');
const sendTask =              require('../tasksWorker').sendTask;

// region Module multer settings.
const storage = multer.diskStorage({
  destination:  (req, file, cb) => {
    imageService.pathStore(`public/userData/tmp/${file.originalname}`);
    cb(null, 'public/userData/tmp');
  },
  filename:     (req, file, cb) => { cb(null, `${moment.utc().format('HH:mm:ss-DD-MM-YYYY')}-${file.originalname}`) }
});

function fileFilter (req, file, cb) {
  const extension = path.extname(file.originalname).toLowerCase();
  if (_.includes(AllowedTypes, extension)) {  cb(null, true); }
  cb(null, false);
}
const upload =  multer({
  storage: storage,
  fileFilter: fileFilter
}).single('avatar');
// endregion.

const User            = mongoose.model('User');
const SocialNews      = mongoose.model('SocialNews');
const CounterRelation = mongoose.model('CounterRelation');
const Relationship    = mongoose.model('Relationship');

const router =  express.Router();
// endregion.

/*const cropSchema = {
  "width":  "number",
  "height": "number",
  "x":      "number",
  "y":      "number",
};*/

const AllowedTypes = ['.jpg', '.jpeg', '.png'];

function get(req, res, next) {
  return User.findOne({
    login: req.params.login
  })
    .lean()
    .then((user) => {
      if (!user) {
        return res.responses.notFoundResource('Пользователь не найден.');
      }
      const leanUser = user;

      delete leanUser.password;

      if (leanUser.token) {
        delete leanUser.token;
      }
      return res.json(leanUser);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function signUp(req, res, next) {
  const userDoc = req.body;

  if (!userDoc.login) {
    return res.responses.validationError('Введите желаемый логин.');
  }

  if (!userDoc.password) {
    return res.responses.validationError('Введите желаемый пароль.');
  }

  return User.findOne({ login: userDoc.login })
    .then((user) => {
      if (user) {
        return res.responses.requestError('Пользователь с таким логином уже зарегистрирован.');
      }
      return User.create(userDoc)
        .then((createdUser) => {
          return res.responses.success('Аккаунт был успешно создан', createdUser);
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function signIn(req, res, next) {
  const userDoc = req.body;

  if (!userDoc.login) {
    return res.responses.validationError('Введите свой логин.');
  }

  if (!userDoc.password) {
    return res.responses.validationError('Введите свой пароль.');
  }

  return User.findOne({ login: userDoc.login })
    .then((user) => {
      if (!user) {
        return res.responses.notFoundResource('Пользователь не найден.');
      }
      if (user.password !== userDoc.password) {
        return res.responses.requestError('Неверный пароль.');
      }
      const token = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        const r = Math.random() * 16 | 0;
        const v = c === 'x' ? r : (r & 0x3 | 0x8);

        return v.toString(16);
      });

      return User.findOneAndUpdate({ login: userDoc.login }, { token })
        .then(() => {
          return res.responses.success(token);
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function getUserData(req, res, next) {
  const login = req.body.login;
  const token = req.body.token;

  return User.findOne({ login })
    .then((user) => {
      if (!user) {
        return res.responses.notFoundResource('Пользователь не найден.');
      }
      if (user.token !== token) {
        return res.responses.unauthorized('Для работы с системой необходимо залогиниться.');
      }
      return res.responses.success(user.userData ? user.userData : {});
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function saveUserData(req, res, next) {
  const login = req.body.login;
  const userData = req.body.data;
  const token = req.body.token;

  return User.findOne({ login })
    .then((user) => {
      if (!user) {
        return res.responses.notFoundResource('Пользователь не найден.');
      }
      if (user.token !== token) {
        return res.responses.unauthorized('Для работы с системой необходимо залогиниться.');
      }
      return User.findOneAndUpdate({ login }, { userData })
        .then(() => {
          return res.responses.success('Информация успешно сохранена.');
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function getIdByToken(req, res) {
  return res.json(req.user._id);
}

function changeProfile(req, res, next) {
  const profile = req.body;
  const user = req.user;
  const conditions = { _id: user._id };
  let update = setUpdateFields(profile, user.type);

  if (_.isEmpty(update)) {
    return res.responses.success();
  }
  return User.update(conditions, update)
    .then(() => {
      return res.responses.success();
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function setUpdateFields(profile, type) {
  if (typeof profile.description !== 'undefined') {
    return { description: profile.description};
  }

  if (type === constants.USER_TYPE.person) {
    if (profile.firstName && !_.isEmpty(profile.firstName)) {
      return { 'user.personData.firstName': profile.firstName };
    }
    if (typeof profile.middleName !== 'undefined') {
      return { 'personData.middleName': profile.middleName };
    }
    if (profile.lastName && !_.isEmpty(profile.lastName)) {
      return { 'personData.lastName': profile.lastName };
    }
  } else {
    if (typeof profile.brand !== 'undefined') {
      return { 'companyData.brand': profile.brand };
    }
  }
}

function uploadAvatar(req, res, next) {
  const file = req.file;
  if (!file) { return res.responses.requestError('Аватар должен быть формата jpeg, jpg или png.'); }
  /* let crop = req.body.crop;
  try {
    crop = JSON.parse(crop);
    if (!_.isEmpty(crop)) {
      if (!jsonValidator(crop, cropSchema)) {
        return res.responses.validationError('Не корректный объект обрезки.');
      }
    }
  } catch(err) {
    return res.responses.validationError('Не корректный объект обрезки.');
  }
  file.crop = crop;*/
  file.type = 'avatar';
  file.newPath = `public/userData/${req.user._id}/${file.type}/${file.originalname}.temp`;
  return Promise.resolve()
    .then(() => { return imageService.imageProcessing(file); })
    .then((result) => {
      if(!result.result) { return res.responses.requestError(result.message)}
      file.newPath = file.newPath.replace('public/userData/', '');
      file.newPath = file.newPath.replace(/.temp$/, '');
      file.newPath = linkGenerationService.usersFileLink(file.newPath);
      return User.update({_id: req.user._id}, {avatar: file.newPath})
        .then(() => {
          sendTask('updateUserAvatar', { userId: req.user._id, avatar: file.newPath });
          return res.json({ avatar: file.newPath });
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    })
    .finally(() => {
      try {
        fs.unlinkSync(file.path);
      } catch(err) {
        errorService.errorOutput(err, 'Error UploadAvatar when delete temporary file ');
      }

      let pathFrom = file.newPath.replace(/^.*data/, 'public/userData');
      const dir = path.parse(path.resolve(pathFrom)).dir;
      if (!fs.existsSync(dir)) {
        return ;
      }
      let files = fs.readdirSync(dir);

      // old and new images
      if (files.length === 4) {
        files.forEach((file) => {
          pathFrom = path.join(dir, file);
          if (/\.temp$/.test(pathFrom)) {
            return;
          }
          if (/-min\./.test(pathFrom)) {
            try {
              fs.unlinkSync(path.resolve(pathFrom));
            } catch (err) {
              errorService.errorOutput(err, 'Error UploadAvatar when delete unnecessary file ');
            }
          }
          let pathTo = pathFrom.replace(/^.*userData/, '$&/archived');
          imageService.pathStore(pathTo);
          pathTo = pathTo.replace(/\.[a-zA-Z]{3,4}$/, `-${moment.utc().format('HH:mm:ss-DD-MM-YYYY')}$&`);
          return fs.renameSync(pathFrom, pathTo);
        });
      }

      files = fs.readdirSync(dir);
      files.forEach((file) => {
        pathFrom = path.join(dir, file);
        if (!/\.temp$/.test(pathFrom)) {
          return;
        }
        let pathTo = pathFrom.replace(/\.temp$/, '');
        fs.renameSync(pathFrom, pathTo);
      });
    });
}

function avatarUploadHandler(req, res, next) {
  upload(req, res, (err) => {
    if (err) {
      return res.responses.requestError('Ошибка при загрузке аватарки.') ;
    }
    return next();
  });
}

function deleteAvatar(req, res, next) {
  const user = req.user;
  if (/data\/default-images/.test(user.avatar)) { return res.json({ avatar: user.avatar }); }

  // archive max image
  let pathFrom = user.avatar.replace(/^.*data/, 'public/userData');
  // pathFrom = pathFrom.replace(/\.[a-zA-Z]{3,4}$/, '@2x$&');
  let pathTo = pathFrom.replace(/^.*userData/, '$&/archived');
  pathTo = pathTo.replace(/\.[a-zA-Z]{3,4}$/, `-${moment.utc().format('HH:mm:ss-DD-MM-YYYY')}$&`);
  pathTo = imageService.pathStore(pathTo);
  fs.renameSync(path.resolve(pathFrom), pathTo);

  // delete other images
  pathFrom = user.avatar.replace(/^.*data/, 'public/userData');
  try {
    //fs.unlinkSync(path.resolve(pathFrom));

    pathFrom = pathFrom.replace(/\.[a-zA-Z]{3,4}$/, '-min$&');
    fs.unlinkSync(path.resolve(pathFrom));
  } catch (err) {
    errorService.errorOutput(err, 'Error DeleteAvatar when delete unnecessary file ');
  }

  const defaultImages = config.get('defaultImages');
  const avatar = linkGenerationService.usersFileLink(defaultImages);
  return User.update({ _id: req.user._id }, { avatar: avatar })
    .then(() => {
      sendTask('updateUserAvatar', { userId: req.user._id, avatar: avatar });
      return res.json({ avatar: avatar });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    })
}

function getCounters(req, res, next) {
  const userId = req.user._id;
  const conditions = {
    to:     userId,
    viewed: false,
    type:   constants.USER_RELATION.subscription
  };

  const newFollowersPromise = Relationship.find(conditions).count();
  const countersPromise = CounterRelation.findOne({ user: userId }, { _id: 0, user: 0 }).lean();

  return Promise.all([newFollowersPromise, countersPromise])
    .spread((newFollowers, counters) => {
      counters.followersNew = newFollowers;

      return res.json(counters);
    })
    .catch((err) => next(err instanceof Error ? err : new VError(err)));
}

router.get('/:login', get);
router.post('/signup', signUp);
router.post('/signin', signIn);
router.post('/get-data', getUserData);
router.post('/save-data', saveUserData);
router.put(   '/',        passport.authenticate('jwt', { session: false }), validate(usersValid.changeMainPage),  changeProfile);
router.post(  '/avatar',  passport.authenticate('jwt', { session: false }), avatarUploadHandler,                  uploadAvatar);
//router.post(  '/avatar',  passport.authenticate('jwt', { session: false }), validate(usersValid.changeAvatar),    uploadAvatar);
router.delete('/avatar',  passport.authenticate('jwt', { session: false }),                                       deleteAvatar);
router.get(   '/id',      passport.authenticate('jwt', { session: false }),                                       getIdByToken);
router.get(   '/counters',passport.authenticate('jwt', { session: false }),                                       getCounters);

module.exports = router;
