// region Module dependencies
const mongoose        = require('mongoose');
const express         = require('express');
const _               = require('lodash');
const passport        = require('passport');
const Promise         = require('bluebird');

const constants       = require('../constants');
const dataOptions     = require('../middlewares/dataOptions').dataOptions;
const acl             = require('../middlewares/acl');
const sendTask        = require('../tasksWorker').sendTask;
const partnerService  = require('../services/partnerService');

const User            = mongoose.model('User');
const Relationship    = mongoose.model('Relationship');
const CounterRelation = mongoose.model('CounterRelation');
// endregion.

const router = express.Router();

function getPartners(req, res, next) {
  const user   = req.user;
  const userId = mongoose.Types.ObjectId(req.params._id);
  const search = req.query.search;
  const conditions = {
    $or: [
      { from: userId },
      { to:   userId }
    ],
    type: constants.USER_RELATION.partners
  };

  let mutualPromise = null;

  if (search) {
    conditions['$or'][0].toName = new RegExp(`.*${_.escapeRegExp(search)}.*`, 'i');
    conditions['$or'][1].fromName = new RegExp(`.*${_.escapeRegExp(search)}.*`, 'i');
  }

  if (user) {
    mutualPromise = partnerService.getMutualPartners(user._id, userId);
  }

  const countersPromise = CounterRelation.findOne({ user: userId }, { _id: 0, partners: 1 }).lean();

  const relationsPromise = Relationship.find(conditions, {}, req.dataOptions)
    .populate('to', {
      avatar:      1,
      type:        1,
      description: 1,
      'companyData.legalName':    1,
      'companyData.director':     1,
      'companyData.directorPost': 1
    })
    .populate('from', {
      avatar:      1,
      type:        1,
      description: 1,
      'companyData.legalName':    1,
      'companyData.director':     1,
      'companyData.directorPost': 1
    })
    .lean();

  return Promise.all([relationsPromise, mutualPromise, countersPromise])
    .spread((result, mutualPartners, counters) => {
      const partners = [];

      if (result.length > 0) {
        // data conversion
        result.forEach((relation) => {
          if (relation.from.toString === userId.toString()) {
            partners.push(relation.to);
          } else {
            partners.push(relation.from);
          }
        });
      }

      counters.mutual = mutualPartners? mutualPartners.length : 0;

      return res.json({ partners: partners, counters: counters });
    })
    .catch((err) => next(err instanceof Error ? err : new VError(err)));
}

function deletePartner(req, res, next) {
  const userId = req.user._id;
  const partnerId = req.params._id;
  const conditions = {
    $or: [
      {
        from: userId,
        to:   partnerId
      },
      {
        from: partnerId,
        to:   userId
      }
    ],
    type: constants.USER_RELATION.partners
  };

  return Relationship.findOneAndRemove(conditions).lean()
    .then((relation) => {
      if (!relation) {
        return res.responses.requestError('Ошибка удаления партнера: вы не являетесь партнерами.');
      }
      sendTask('deletePartner', { userId: userId, partnerId: partnerId });

      return res.responses.success();
    })
    .catch((err) => next(err instanceof Error ? err : new VError(err)));
}

function getMutualPartners(req, res, next) {
  const dataOptions = req.dataOptions;
  const userId = req.user._id;
  const anotherUserId = mongoose.Types.ObjectId(req.params._id);
  const search = req.query.search ? new RegExp(`.*${_.escapeRegExp(req.query.search)}.*`, 'i') : null;

  return partnerService.getMutualPartners(userId, anotherUserId, dataOptions, search)
    .then((mutualPartners) => {
      return res.json(mutualPartners);
    })
    .catch((err) => next(err instanceof Error ? err : new VError(err)));
}

router.get(   '/:_id([0-9A-F]{24})',                                                        acl.getUserByToken, dataOptions({sort: '-date'}),  getPartners);
router.delete('/:_id([0-9A-F]{24})',      passport.authenticate('jwt', { session: false }), acl.withoutPerson,                                 deletePartner);
router.get('/mutual/:_id([0-9A-F]{24})',  passport.authenticate('jwt', { session: false }), acl.withoutPerson,  dataOptions({sort: '-date'}),  getMutualPartners);

module.exports = router;
