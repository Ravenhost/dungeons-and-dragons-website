// region Module dependencies.
const express =           require('express');
const mongoose =          require('mongoose');
const _ =                 require('lodash');

const dataOptions =       require('../middlewares/dataOptions').dataOptions;
const filterMiddleware =  require('../middlewares/filtersBuilder');

const NewsItem = mongoose.model('NewsItem');

const router = express.Router();
// endregion.

function getAll(req, res, next) {
  const filters = _.isEmpty(req.filters) ? { category: 'main' } : req.filters;
  const conditions = _.assign({ removeDate: {$exists: false} }, filters);
  return NewsItem.find(conditions, {}, req.dataOptions)
    .then((news) => {
      return res.json({news});
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

router.get('/', dataOptions({sort: '-pubDate'}), filterMiddleware(NewsItem), getAll);

module.exports = router;
