// region Module dependencies.
const express       = require('express');
const passport      = require('passport');
const _             = require('lodash');
const mongoose      = require('mongoose');
const Promise       = require('bluebird');

const acl           = require('../middlewares/acl');
const constants     = require('../constants');
const dataOptions   = require('../middlewares/dataOptions').dataOptions;
const sendTask      = require('../tasksWorker').sendTask;

const User          = mongoose.model('User');
const Relationship  = mongoose.model('Relationship');

const router = express.Router();
// endregion.

function addRequest(req, res, next) {
  const user = req.user;
  const anotherUserId = req.params._id;

  if (_.isEqual(user._id.toString(), anotherUserId)) {
    return res.responses.notFoundResource('Нельзя себе подать заявку в партнеры.');
  }

  const request = {
    from: user._id,
    to:   anotherUserId,
    type: constants.USER_RELATION.partnerRequest
  };
  const oppositeRequest = {
    from: anotherUserId,
    to:   user._id,
    type: constants.USER_RELATION.partnerRequest
  };
  const subscribeConditions = {
    from: user._id,
    to:   anotherUserId,
    type: constants.USER_RELATION.subscription
  };
  const partnerConditions = {
    $or: [
      {
        from: user._id,
        to:   anotherUserId
      },
      {
        from: anotherUserId,
        to:   user._id
      }
    ],
    type: constants.USER_RELATION.partners
  };

  const anotherUserPromise = User.findOne({
    _id:          anotherUserId,
    archivedDate: { $exists: false }
  }).lean();
  const relationshipsPromise = Relationship.findOne(request);
  const partnersPromise = Relationship.findOne(partnerConditions);
  const oppositePromise = Relationship.findOne(oppositeRequest);
  const subscribePromise = Relationship.findOne(subscribeConditions);

  return Promise.all([anotherUserPromise, relationshipsPromise, partnersPromise, oppositePromise, subscribePromise])
    .spread((anotherUser, relation, partner, opposite, subscription) => {
      let resultPromise = null;
      let subscriptionPromise = null;

      if (!anotherUser) {
        return res.responses.notFoundResource('Пользователь не найден.');
      }
      if (anotherUser.type === constants.USER_TYPE.person) {
        return res.responses.requestError('Физическому лицу нельзя подать заявку в партнеры.');
      }
      if (relation) {
        return res.responses.requestError('Заявка в партнеры уже была подана.');
      }
      if (partner) {
        return res.responses.requestError('Ошибка подачи заявки в партнеры: вы уже являетесь партнерами.');
      }

      if (opposite) { // if opposite request then auto accept both
        opposite.type = constants.USER_RELATION.partners;
        resultPromise = opposite.save();
        sendTask('addPartner', { userId: user._id, partnerId: anotherUserId });
      } else {
        request.fromName = user.companyData.legalName;
        request.toName = anotherUser.companyData.legalName;
        resultPromise = Relationship.create(request);
        sendTask('addRequest', { userId: user._id, partnerId: anotherUserId });
      }

      if (!subscription) {
        subscribeConditions.fromName = user.companyData.legalName;
        subscribeConditions.toName = anotherUser.companyData.legalName;
        subscriptionPromise = Relationship.create(subscribeConditions);
        sendTask('addSubscription', { userId: user._id, partnerId: anotherUserId });
      }

      return Promise.all([resultPromise, subscriptionPromise])
        .spread(() => res.responses.success());
    })
    .catch( err => next(err instanceof Error ? err : new VError(err)) );

}

function deleteRequest(req, res, next) {
  const user = req.user;
  const anotherUserId = req.params._id;
  const request = {
    from: user._id,
    to:   anotherUserId,
    type: constants.USER_RELATION.partnerRequest
  };

  return Relationship.findOneAndRemove(request).lean()
    .then((relation) => {
      if (!relation) {
        return res.responses.requestError('Такой заявки в партнеры не существует.');
      }
      sendTask('deleteRequest', { userId: user._id, partnerId: anotherUserId });

      return res.responses.success();
    })
    .catch( err => next(err instanceof Error ? err : new VError(err)) );
}

function getIncoming(req, res, next) {
  const userId = req.user._id;
  const search = req.query.search;
  const conditions = {
    to: userId,
    type: constants.USER_RELATION.partnerRequest
  };

  if (search) {
    conditions.fromName = new RegExp(`.*${_.escapeRegExp(search)}.*`, 'i');
  }

  return Relationship.find(conditions, { 'from': 1 }, req.dataOptions)
    .populate('from', {
      avatar:       1,
      type:         1,
      description:  1,
      'companyData.legalName':    1,
      'companyData.director':     1,
      'companyData.directorPost': 1
    })
    .lean()
    .then((result) => {
      const subscriptions = [];

      if (result.length > 0) {
        // data conversion
        result.forEach((relation) => {
          subscriptions.push(relation.from);
        });
      }
      return res.json(subscriptions);
    })
    .catch((err) => next(err instanceof Error ? err : new VError(err)));
}

function getOutgoing(req, res, next) {
  const userId = req.user._id;
  const search = req.query.search;
  const conditions = {
    from: userId,
    type: constants.USER_RELATION.partnerRequest
  };

  if (search) {
    conditions.toName = new RegExp(`.*${_.escapeRegExp(search)}.*`, 'i');
  }

  return Relationship.find(conditions, { 'to': 1 }, req.dataOptions)
    .populate('to', {
      avatar:       1,
      type:         1,
      description:  1,
      'companyData.legalName':    1,
      'companyData.director':     1,
      'companyData.directorPost': 1
    })
    .lean()
    .then((result) => {
      const subscriptions = [];

      if (result.length > 0) {
        // data conversion
        result.forEach((relation) => {
          subscriptions.push(relation.to);
        });
      }
      return res.json(subscriptions);
    })
    .catch((err) => next(err instanceof Error ? err : new VError(err)));
}

function acceptRequest(req, res, next) {
  const user = req.user;
  const anotherUserId = req.params._id;

  const requestConditions = {
    from: anotherUserId,
    to:   user._id,
    type: constants.USER_RELATION.partnerRequest
  };
  const subscribeConditions = {
    from: user._id,
    to:   anotherUserId,
    type: constants.USER_RELATION.subscription
  };

  const requestPromise = Relationship.findOne(requestConditions);
  const subscriptionPromise = Relationship.findOne(subscribeConditions);

  return Promise.all([requestPromise, subscriptionPromise])
    .spread((request, subscription) => {
      let subscriptionPromise = null;

      if (!request) {
        return res.responses.requestError('Такой заявки не существует.')
      }

      request.type = constants.USER_RELATION.partners;
      const resultPromise = request.save();

      if (!subscription) {
        subscribeConditions.fromName = request.toName;
        subscribeConditions.toName = request.fromName;
        subscriptionPromise = Relationship.create(subscribeConditions);
        sendTask('addSubscription', { userId: user._id, partnerId: anotherUserId });
      }

      return Promise.all([resultPromise, subscriptionPromise])
        .spread(() => {
          sendTask('addPartner', { userId: user._id, partnerId: anotherUserId });

          return res.responses.success();
        });
    })
    .catch( err => next(err instanceof Error ? err : new VError(err)) );
}

function rejectRequest(req, res, next) {
  const user = req.user;
  const anotherUserId = req.params._id;
  const request = {
    to:   user._id,
    from: anotherUserId,
    type: constants.USER_RELATION.partnerRequest
  };

  return Relationship.findOneAndRemove(request).lean()
    .then((relation) => {
      if (!relation) {
        return res.responses.requestError('Такой заявки в партнеры не существует.');
      }
      sendTask('deleteRequest', { partnerId: user._id, userId: anotherUserId });

      return res.responses.success();
    })
    .catch( err => next(err instanceof Error ? err : new VError(err)) );
}

router.post(  '/:_id([0-9A-F]{24})',        passport.authenticate('jwt', { session: false }), acl.withoutPerson, addRequest);
router.delete('/:_id([0-9A-F]{24})',        passport.authenticate('jwt', { session: false }), acl.withoutPerson, deleteRequest);
router.get(   '/incoming',                  passport.authenticate('jwt', { session: false }), acl.withoutPerson, dataOptions({sort: '-date'}), getIncoming);
router.get(   '/',                          passport.authenticate('jwt', { session: false }), acl.withoutPerson, dataOptions({sort: '-date'}), getOutgoing);
router.put(   '/accept/:_id([0-9A-F]{24})', passport.authenticate('jwt', { session: false }), acl.withoutPerson, acceptRequest);
router.put(   '/reject/:_id([0-9A-F]{24})', passport.authenticate('jwt', { session: false }), acl.withoutPerson, rejectRequest);

module.exports = router;
