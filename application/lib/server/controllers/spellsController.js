'use strict';

// region Module dependencies.
const express = require('express');
const VError = require('verror');
const mongoose = require('mongoose');

const Spell = mongoose.model('Spell');
// endregion

const router = express.Router();

function create(req, res, next) {
  const spellDoc = req.body;
  const conditions = {
    spellTitle: spellDoc.spellTitle,
    className: spellDoc.className
  };
  return Spell.findOne(conditions)
    .then((foundSpell) => {
      if (foundSpell) {
        return res.responses.requestError('Such a spell already exists.');
      }
      return Spell.create(spellDoc)
        .then((createdSpell) => {
          return res.responses.success(createdSpell);
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function getByClassName(req, res, next) {
  const className = req.params._className;
  const conditions = {
    className: className,
    removeDate: {$exists: false}
  };
  return Spell.find(conditions)
    .then((spells) => {
      if (!spells) {
        return res.responses.notFoundResource();
      }
      return res.json(spells);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function getAll(req, res, next) {
  const conditions = {
    removeDate: {$exists: false}
  };
  return Spell.find(conditions)
    .then((spells) => {
      if (!spells) {
        return res.responses.notFoundResource();
      }
      return res.json(spells);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

router.post('/', create);
router.get('/', getAll);
router.get('/:_className', getByClassName);

module.exports = router;
