// region Module dependencies.
const express =               require('express');
const VError =                require('verror');
const mongoose =              require('mongoose');
const _ =                     require('lodash');
const bcrypt =                require('bcryptjs-then');
const moment =                require('moment');
const Promise =               require('bluebird');
const passport =              require('passport');

const logger =                require('../logger');
const validate =              require('../middlewares/validateSchema');
const authValid =             require('../validators/authSchemas');
const requisiteService =      require('../services/requisiteService');
const userService =           require('../services/userService');
const config =                require('../config');
const randomBytes =           require('../services/randomBytes');
const notificationService =   require('../services/notificationService');
const linkGenerationService = require('../services/linkGenerationService');
const constants =             require('../constants');

const User = mongoose.model('User');

const router = express.Router();
// endregion

function signIn(req, res, next) {
  return Promise.resolve()
    .then(() => {
      logger.info('Auth by login "%s:...', req.body.email);
      return passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err) {
          return next(new VError(err, 'Ошибка авторизации.'));
        }
        if (!user) {
          return res.responses.unauthorized(info.message);
        }
        const token = userService.generateToken(user);
        return req.logIn(user, (err) => {
          if (err) {
            return next(new VError(err, 'Ошибка авторизации.'));
          }
          logger.info('Account "%s" auth successfully.', req.body.email);
          return res.json({ message: 'Авторизация прошла успешно.', token: `JWT ${token}`, id: user._id });
        });
      })(req, res, next);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function signOut(req, res, next) {
  return Promise.resolve()
    .then(() => {
      if (req.user) {
        logger.info('Account "%s" logout.', req.user.email);
        req.logout();
      }
      return res.status(204).end();
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function signUp(req, res, next) {
  const userFromSession = req.session && req.session.user || null;
  const user = req.body;
  if (!_.isEqual(user.type, constants.USER_TYPE.person) && (!userFromSession || !_.isEqual(userFromSession.ogrn, user.ogrn))) {
    return res.responses.requestError('ОГРН был изменен: либо верните предыдущий вариант, либо проверьте реквизиты для текущего значения.');
  }
  let userDoc = {
    email: user.email,
    type: user.type,
    avatar:  linkGenerationService.usersFileLink(config.get('defaultImages'))
  };
  if (_.isEqual(userDoc.type, constants.USER_TYPE.person)) {
    _.assign(userDoc, {
      personData: {
        firstName: user.firstName,
        middleName: user.middleName,
        lastName: user.lastName
      }
    });
  }
  else {
    _.assign(userDoc, {
      companyData: {
        ogrn: user.ogrn,
        brand: user.brand,
        legalName: userFromSession.companyData.legalName,
        inn: userFromSession.companyData.inn,
        kpp: userFromSession.companyData.kpp,
        legalAddress: userFromSession.companyData.legalAddress,
        registerDate: userFromSession.companyData.registerDate,
        director: userFromSession.companyData.director,
        directorPost: userFromSession.companyData.directorPost,
        okvd: userFromSession.companyData.okvd
      }
    });
    if (userFromSession.companyData.city) {
      userDoc.city = userFromSession.companyData.city;
      userDoc.citySearch = userDoc.city;
    }
  }
  return bcrypt.hash(user.password)
    .then((hash) => {
      userDoc.password = hash;
      return User.create(userDoc);
    })
    .then((user) => {
      const tokenPromise = randomBytes.randomBytes(config.get('mail.invitationTokenLength'));
      return [user, tokenPromise];
    })
    .spread((user, token) => {
      user.passwordRecoveryHash = token;
      user.passwordRecoveryDate = new Date(moment.utc().add(config.get('tokenTTL'), 'hours'));
      return user.save()
        .then((user) => {
          req.session.destroy();
          notificationService.confirmEmail({ email: user.email, token: token });
          return res.responses.success('Регистрация прошла успешно. Вам отправлено письмо на подтверждения e-mail.');
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function checkRequisites(req, res, next) {
  const user = req.body;
  return requisiteService(user.ogrn)
    .then((data) => {
      if (!data) { return res.responses.requestError(`Не найдена информация по ОГРН ${user.ogrn}`); }
      // save temporary data
      req.session.user = user;
      req.session.user.companyData = data;
      return res.json(data);
    })
    .catch((error) => { return res.responses.serverError(`Не найдена информация по ОГРН ${user.ogrn}`); });
}

function confirmEmail(req, res, next) {
  const token = req.body.token;
  const conditions = {
    emailConfirmed: false,
    passwordRecoveryHash: token,
    passwordRecoveryDate: { $gte: new Date(moment.utc()) },
    archived: false
  };
  return User.findOne(conditions)
    .then((user) => {
      if (!user) { return res.responses.notFoundResource('Ошибка: неверный или истекший токен.'); }
      user.emailConfirmed = true;
      user.passwordRecoveryHash = null;
      user.passwordRecoveryDate = null;
      return user.save()
        .then(() => {
          logger.info('Auth by login "%s:...', user.email);
          const token = userService.generateToken(user);
          return req.logIn(user, (err) => {
            if (err) {
              return next(new VError(err, 'Ошибка авторизации.'));
            }
            logger.info('Account "%s" auth successfully.', user.email);
            return res.json({ message: 'E-mail подтвержден.', token: `JWT ${token}`, id: user._id });
          });
        });
    })
    .catch((err) => { return next(err instanceof Error ? err : new VError(err)); });
}

function resendConfirmEmail(req, res, next) {
  const email = req.body.email;
  const conditions = {
    emailConfirmed: false,
    email: email,
    archived: false
  };
  return User.findOne(conditions)
    .then((user) => {
      if (!user) { return res.responses.notFoundResource('Ошибка: неверный e-mail.'); }
      return randomBytes.randomBytes(config.get('mail.invitationTokenLength'))
        .then((token) => {
          user.passwordRecoveryHash = token;
          user.passwordRecoveryDate = new Date(moment.utc().add(config.get('tokenTTL'), 'hours'));
          return [token, user.save()];
        })
        .spread((token, user) => {
          notificationService.confirmEmail({ email: user.email, token: token });
          return res.responses.success('Вам переотправлено письмо на подтверждение e-mail.');
        });
    })
    .catch((err) => { return next(err instanceof Error ? err : new VError(err)); });
}

function forgotPassword(req, res, next) {
  const email = req.body.email;
  const conditions = {
    emailConfirmed: true,
    email: email,
    archived: false
  };
  return User.findOne(conditions)
    .then((user) => {
      if (!user) { return res.responses.notFoundResource('Данный e-mail не найден.'); }
      return randomBytes.randomBytes(config.get('mail.invitationTokenLength'))
        .then((token) => {
          user.passwordRecoveryHash = token;
          user.passwordRecoveryDate = new Date(moment.utc().add(config.get('passwordTTL'), 'hours'));
          return [token, user.save()];
        })
        .spread((token, user) => {
          notificationService.resetPassword({ email: user.email, token: token });
          return res.responses.success('Вам отправлено письмо на сброс пароля.');
        });
    })
    .catch((err) => { return next(err instanceof Error ? err : new VError(err)); });
}

function resetPassword(req, res, next) {
  const token = req.body.token;
  const password = req.body.password;
  const conditions = {
    emailConfirmed: true,
    passwordRecoveryHash: token,
    passwordRecoveryDate: { $gte: new Date(moment.utc()) },
    archived: false
  };
  return User.findOne(conditions)
    .then((user) => {
      if (!user) { return res.responses.notFoundResource('Ошибка: неверный или истекший токен.'); }
      user.passwordRecoveryHash = null;
      user.passwordRecoveryDate = null;
      return bcrypt.hash(password)
        .then((hash) => {
          user.password = hash;
          return user.save();
        })
        .then((user) => {
          logger.info('Auth by login "%s:...', user.email);
          const token = userService.generateToken(user);
          return req.logIn(user, (err) => {
            if (err) {
              return next(new VError(err, 'Ошибка авторизации.'));
            }
            logger.info('Account "%s" auth successfully.', user.email);
            return res.json({ message: 'Пароль успешно изменен.', token: `JWT ${token}`, id: user._id });
          });
        });
    })
    .catch((err) => { return next(err instanceof Error ? err : new VError(err)); });
}

router.post(  '/',                  validate(authValid.signIn),           signIn);
router.get(   '/', passport.authenticate('jwt', { session: false }),      signOut);
router.post(  '/signup',            validate(authValid.signUp),           signUp);
router.post(  '/check',             validate(authValid.checkRequisites),  checkRequisites);
router.put(   '/confirm',           validate(authValid.checkToken),       confirmEmail);
router.post(  '/confirm',           validate(authValid.checkEmail),       resendConfirmEmail);
router.put(   '/password/forgot',   validate(authValid.checkEmail),       forgotPassword);
router.put(   '/password/change',   validate(authValid.checkPassword),    resetPassword);

module.exports = router;
