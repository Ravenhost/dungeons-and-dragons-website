import React                                from 'react';
import { match }                            from 'react-router';
import { ReduxAsyncConnect, loadOnServer }  from 'redux-connect';
import routes                               from '../../client/redux/routes';
import { Provider }                         from 'react-redux';
import ReactDOM                             from 'react-dom/server';
import configureStore                       from '../../client/redux/configureStore';
import config                               from '../../server/config';

module.exports = function (req, res) {

  const store = configureStore();

  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      return res.redirect(301, redirectLocation.pathname + redirectLocation.search);
    }

    if (error) {
      return res.status(500).send(error.message);
    }

    if (!renderProps) {
      return res.status(404).send('Not found');
    }

    loadOnServer({...renderProps, store}).then(() => {

      const componentHTML = ReactDOM.renderToString(
        <Provider store={store} key="provider">
          <ReduxAsyncConnect {...renderProps} />
        </Provider>
      );

      const content = renderHTML(componentHTML, store.getState());

      res.send(content);
    });
  });
};

const HOST = process.env.APP_HOST ? process.env.APP_HOST : config.get('http.host');

const assetUrl = process.env.NODE_ENV === 'dev'
  ? `http://${HOST}:8056/public`
  : '';

function renderHTML(componentHTML, initialState) {
  return `<!DOCTYPE html>
    <html lang="ru">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Social Network</title>
      
      <link rel="icon" type="image/x-icon" href="/images/favicon.ico">

      <!-- Fonts Online -->
      <link href="/css/icofont.css" rel="stylesheet">
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300' rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&amp;subset=cyrillic" rel="stylesheet">

      <!-- Style Sheet -->
      <link rel="stylesheet" href="/css/style.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.css">
      

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      
      <link rel="stylesheet" href="/css/style.css">
    </head>

    <body>
    <div id="react-view">${componentHTML}</div>
    <!-- Scripts -->
    <script type="application/javascript">
      window.REDUX_INITIAL_STATE = ${JSON.stringify(initialState)};
    </script>
    
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>
  
    <script src="http://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/plugins/superfish.min.js"></script>
    <script src="/js/jquery.ui.min.js"></script>
    <script src="/js/plugins/rangeslider.min.js"></script>
    <script src="/js/plugins/jquery.flexslider-min.js"></script>
    <script src="/js/uou-accordions.js"></script>
    <script src="/js/uou-tabs.js"></script>
    <script src="/js/plugins/select2.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/gmap3.min.js"></script>
    <script src="/js/scripts.js"></script>
    <script type="application/javascript" src="${assetUrl}/assets/bundle.js"></script>
    </body>
    </html>`;
}
