// region Module dependencies.
const express =               require('express');
const VError =                require('verror');
const mongoose =              require('mongoose');
const passport =              require('passport');
const _ =                     require('lodash');
const path =                  require('path');
const moment =                require('moment');
const fs =                    require('fs');
const Promise =               require('bluebird');

const dataOptions =           require('../middlewares/dataOptions').dataOptionsWithDate;
const validate =              require('../middlewares/validateSchema');
const socialNewsValid =       require('../validators/socialNewItemSchemas');
const acl =                   require('../middlewares/acl');
const config =                require('../config');
const multer =                require('../services/multer');
const errorService =           require('../services/errorService');
const imageService =          require('../services/imageService');
const linkGenerationService = require('../services/linkGenerationService');
const socialNewsService =     require('../services/socialNewsService');
const fsService =             require('../services/fsService');
const sendTask =              require('../tasksWorker').sendTask;
const constants =             require('../constants');

// region Module multer settings.
const storage = multer.diskStorage({
  destination:  (req, file, cb) => {
    imageService.pathStore(`public/userData/tmp/${file.originalname}`);
    cb(null, 'public/userData/tmp');
  },
  filename:     (req, file, cb) => { cb(null, `${moment.utc().format('HH:mm:ss-DD-MM-YYYY')}-${file.originalname}`) }
});

function fileFilter (req, file, cb) {
  const extension = path.extname(file.originalname).toLowerCase();
  if (_.includes(AllowedTypes[file.fieldname], extension)) {  return cb(null, true); }
  errorService.multerError(req, file.fieldname, file.originalname, 'запрещенный для вложения файл');
  return cb(null, false);
}
const upload =  multer({
  storage: storage,
  fileFilter: fileFilter,
  limits: { fileSize: 5242880} // 5 MB
}).fields([{
  name: 'images',
  maxCount: 10
}, {
  name: 'attachments',
  maxCount: 10
}]);

const AllowedTypes = {
  attachments: ['.pdf', '.djvu', '.doc', '.docx', '.dotx', '.ppt', '.pptx', '.rtf', '.csv', '.thmx', '.odt',
    '.ott', '.odg', '.otg', '.odc', '.otc', '.xls', '.xlsx', '.gsfx', '.dwg', '.zip', '.rar', '.7z'
  ],
  images: ['.jpg', '.jpeg', '.png']
};
// endregion.

const SocialNews   = mongoose.model('SocialNews');
const Relationship = mongoose.model('Relationship');

const router = express.Router();
// endregion.

const limitOnDisplay = `-${config.get('likesLimit.forNews')}`;

function getAll(req, res, next) {
  const id = req.params._id;
  const search = req.query.search;
  const conditions = {
    'owner._id': id,
    showInUserMain: true,
    archivedDate: {$exists: false},
    inMaking: false
  };
  if (search) {
    conditions['$text'] = { $search: search };
  }
  const date = req.query.datePublished ? new Date(req.query.datePublished): new Date();
  conditions.datePublished = {
    '$lt': new Date(date)
  };

  return SocialNews.find(conditions, {
    dateModified:   0,
    modifiedBy:     0,
    // text:           0,
    showInUserMain: 0,
    inMaking:       0,
    likes: {
      '$slice': Number(limitOnDisplay)
    },
    reposts: {
      '$slice': Number(limitOnDisplay)
    }
  }, req.dataOptions).populate('sourceId', {
    dateModified:   0,
    modifiedBy:     0,
    // text:           0,
    showInUserMain: 0,
    inMaking:       0,
    isHiddenComment:0,
    likes: {
      '$slice': Number(limitOnDisplay)
    },
    reposts: {
      '$slice': Number(limitOnDisplay)
    }
  })
    .lean()
    .then((socialNews) => {
      socialNews.forEach((socialNewsItem) => {
        if (!_.isEqual(socialNewsItem.text, socialNewsItem.description)) {
          socialNewsItem.isExistFullText = true;
          delete socialNewsItem.text;
        }

        if (socialNewsItem.sourceId && !_.isEqual(socialNewsItem.sourceId.text, socialNewsItem.sourceId.description)) {
          socialNewsItem.sourceId.isExistFullText = true;
          delete socialNewsItem.sourceId.text;
        }
      });
      return res.json(socialNews);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function getById(req, res, next) {
  const id = req.params._id;
  const conditions = {
    _id: id,
    archivedDate: {$exists: false},
    inMaking: false
  };
  return SocialNews.findOne(conditions, {
    dateModified:   0,
    modifiedBy:     0,
    description:    0,
    showInUserMain: 0,
    isHiddenComment:0,
    likes: {
      '$slice': Number(limitOnDisplay)
    },
    reposts: {
      '$slice': Number(limitOnDisplay)
    }
  }).populate('sourceId', {
    dateModified:   0,
    modifiedBy:     0,
    description:    0,
    showInUserMain: 0,
    inMaking:       0,
    likes: {
      '$slice': Number(limitOnDisplay)
    },
    reposts: {
      '$slice': Number(limitOnDisplay)
    }
  }).lean()
    .then((socialNewItem) => {
      return res.json(socialNewItem);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function create(req, res, next) {
  const user = req.user;
  const sourceId = req.body.sourceId;
  let sourcePromise = null;
  if (sourceId) {
    sourcePromise = SocialNews.findOne({
      _id: sourceId,
      inMaking: false,
      archivedDate: {$exists: false}
    }).lean();
  }
  return Promise.resolve(sourcePromise)
    .then((source) => {
      const socialNewsDoc = {
        owner: {
          _id: user._id,
          name: user.companyData.legalName,
          avatar: user.avatar
        }
      };
      if (source) {
        if (source.sourceId) {
          return res.responses.accessDenied('Репост репоста запрещен');
        }
        if (source.owner._id.toString() === user._id.toString()) {
          return res.responses.accessDenied('Репост сам себя запрещен.');
        }
        if (_.findIndex(source.reposts, {_id: user._id}) > -1) {
          return res.responses.accessDenied('Повторный репост запрещен.');
        }
        socialNewsDoc.sourceId = sourceId;
      }
      return SocialNews.create(socialNewsDoc)
        .then((socialNewsItem) => {
          return res.json(socialNewsItem.id);
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function getText(req, res, next) {
  const id = req.params._id;
  const conditions = {
    _id: id,
    showInUserMain: true,
    inMaking: false,
    archivedDate: {$exists: false}
  };
  return SocialNews.findOne(conditions, {
    _id:  0,
    text: 1
  }).lean()
    .then((text) => {
      if (!text) { return res.responses.notFoundResource('Текст не найден.'); }
      return res.json(text);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function getForEditing(req, res, next) {
  const user = req.user;
  const id = req.params._id;
  const conditions = {
    _id:          id,
    archivedDate: { $exists: false }
  };
  return SocialNews.findOne(conditions, {
    datePublished:  0,
    dateModified:   0,
    modifiedBy:     0,
    description:    0,
    showInUserMain: 0,
    likesCount:     0,
    repostsCount:   0,
    likes:          0,
    reposts:        0
  }).lean()
    .then((socialNewsItem) => {
      if (!socialNewsItem) { return res.responses.notFoundResource('Новость не найдена.'); }
      if (socialNewsItem.owner._id.toString() !== user._id.toString()) { return res.responses.accessDenied('Доступ запрещен.'); }
      return res.json(socialNewsItem);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function change(req, res, next) {
  const body = req.body;
  if (_.isEmpty(body)) { return res.responses.requestError('Нет данных для обновления новости.'); }
  const id = req.params._id;
  const user = req.user;
  const conditions = {
    _id: id,
    archivedDate: {$exists: false}
  };
  return SocialNews.findOne(conditions)
    .then((socialNewsItem) => {
      if (!socialNewsItem) { return res.responses.notFoundResource('Новость не найдена.'); }
      if (socialNewsItem.owner._id.toString() !== user._id.toString()) { return res.responses.accessDenied('Доступ запрещен.'); }

      const pathStore = `public/userData/${user._id}/${id}/`;
      if (!_.isEmpty(body.title) && !socialNewsItem.sourceId) {
        socialNewsItem.title = body.title;
      }
      if (!_.isEmpty(body.text))  {
        socialNewsItem.text = body.text;
        socialNewsItem.description = socialNewsItem.text.substr(0, config.get('newDescription'));
      }
      if (!socialNewsItem.sourceId && !_.isEqual(body.images, socialNewsItem.images)) {
        body.images = _.uniq(body.images);
        socialNewsItem.images = body.images;
        if (!_.isEmpty(body.images)) {
          socialNewsItem.images = checkFiles(`${pathStore}images`, body.images);
        }
      }
      if (!socialNewsItem.sourceId && !_.isEqual(body.files, socialNewsItem.files)) {
        body.files = _.uniq(body.files);
        socialNewsItem.files = body.files;
        if (!_.isEmpty(body.files)) {
          socialNewsItem.files = checkFiles(`${pathStore}files`, body.files);
        }
      }
      if (!_.isEmpty(body.hashtag)) {
        socialNewsItem.hashtag = body.hashtag;
      }

      let sourcePromise = null;
      if (socialNewsItem.inMaking && !body.inMaking) {
        socialNewsItem.inMaking = false;
        socialNewsItem.datePublished = Date.now();

        if (socialNewsItem.sourceId) {
          const doc = {
            $inc: {
              repostsCount: 1
            },
            $push: {
              reposts: {
                _id: user._id,
                name: user.companyData.legalName,
                avatar: user.avatar
              }
            }
          };
          sourcePromise = SocialNews.update({ _id: socialNewsItem.sourceId }, doc);
        }
      }

      socialNewsItem.dateModified = Date.now();
      socialNewsItem.modifiedBy = user.companyData.legalName;

      const updatePromise = socialNewsItem.save();
      return Promise.all([updatePromise, sourcePromise])
        .spread((updatedSocialNews) => {
          updatedSocialNews = updatedSocialNews.toObject();
          delete updatedSocialNews.dateModified;
          delete updatedSocialNews.modifiedBy;
          delete updatedSocialNews.text;
          delete updatedSocialNews.showInUserMain;
          delete updatedSocialNews.isHiddenComment;
          updatedSocialNews.likes = updatedSocialNews.likes.slice(limitOnDisplay);
          updatedSocialNews.reposts = updatedSocialNews.reposts.slice(limitOnDisplay);
          if (!updatedSocialNews.sourceId) {
            return res.json(updatedSocialNews);
          }
          return SocialNews.findOne(
            { _id: updatedSocialNews.sourceId },
            {
              dateModified: 0,
              modifiedBy: 0,
              text: 0,
              showInUserMain: 0,
              inMaking: 0,
              isHiddenComment: 0,
              likes: {
                '$slice': Number(limitOnDisplay)
              },
              reposts: {
                '$slice': Number(limitOnDisplay)
              }
            }).lean()
            .then((repost) => {
              updatedSocialNews.sourceId = repost;
              return res.json(updatedSocialNews);
            });
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

function addFiles(req, res, next) {
  if (!req.files) { return res.responses.success(); }
  const id = req.params._id;
  const user = req.user;
  let errors = req.files.errors;
  let files = req.files.attachments;
  let images = req.files.images;
  const pathStore = `public/userData/${user._id}/${id}/`;
  const response = {
    files: [],
    images: []
  };

  if (!files && !images && errors) { return res.json({ errors: errors, files: [], images: [] }); }
  if (!files) {
    files = [];
  }
  if (!images) {
    images = [];
  }

  const conditions = {
    _id: id,
    archivedDate: {$exists: false}
  };
  return SocialNews.findOne(conditions)
    .then((socialNewsItem) => {
      if (!socialNewsItem) {
        return res.responses.notFoundResource('Новость не найдена.');
      }
      if (socialNewsItem.owner._id.toString() !== user._id.toString()) {
        return res.responses.accessDenied('Доступ запрещен.');
      }

      return Promise.mapSeries(files, (file) => {
        let newPath = `${pathStore}files/${file.originalname}`;
        const pathTo = imageService.pathStore(newPath);
        // copy file
        const data = fs.readFileSync(file.path);
        fs.writeFileSync(pathTo, data);

        newPath = newPath.replace(/^.+userData\//, '');
        newPath = linkGenerationService.usersFileLink(newPath);
        response.files.push(newPath);
      })
        .then(() => {
          return Promise.mapSeries(images, (image) => {
            image.type = 'socialNews';
            image.newPath = `${pathStore}images/${moment.utc().format('x')}-${image.originalname}`;
            return Promise.resolve()
              .then(() => { return imageService.imageProcessing(image); })
              .then((result) => {
                if(!result.result) {
                  if (!errors) { errors = {};}
                  if (!errors.images) { errors.images = []; }
                  errors.images.push(`${image.originalname} - ${result.message[0].toLowerCase()}${result.message.slice(1)}`);
                  return ;
                }
                image.newPath = image.newPath.replace(/^.+userData\//, '');
                image.newPath = linkGenerationService.usersFileLink(image.newPath);
                response.images.push(image.newPath);
              });
          })
        })
        .then(() => {
          if (errors) { response.errors = errors; }
          return res.json(response);
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    })
    .finally(() => {
      files.forEach((file) => {
        if (!fs.existsSync(file.path)) {
          return false;
        }
        fs.unlinkSync(file.path);
      });
      images.forEach((image) => {
        if (!fs.existsSync(image.path)) {
          return false;
        }
        fs.unlinkSync(image.path);
      });
    });
}

function deleteSocialNews(req, res, next) {
  const id = req.params._id;
  const user = req.user;
  const conditions = {
    _id: id,
    archivedDate: {$exists: false}
  };
  return SocialNews.findOne(conditions)
    .then((socialNewsItem) => {
      if (!socialNewsItem) {
        return res.responses.notFoundResource('Новость не найдена.');
      }
      if (socialNewsItem.owner._id.toString() !== user._id.toString()) {
        return res.responses.accessDenied('Доступ запрещен.');
      }

      if (socialNewsItem.files.length !== 0) {
        socialNewsItem.files.forEach((file, index) => {
          const pathFrom = file.replace(/^.*data/, 'public/userData');
          let pathTo = file.replace(/^.*data/, 'public/userData/archived');
          pathTo = imageService.pathStore(pathTo);
          fs.renameSync(path.resolve(pathFrom), pathTo);
          return socialNewsItem.files[index] = file.replace(/data/, 'data/archived');
        });
      }
      socialNewsItem.markModified('files');
      if (socialNewsItem.images.length !== 0) {
        socialNewsItem.images.forEach((image, index) => {
          const pathFrom = image.replace(/^.*data/, 'public/userData');
          let pathTo = image.replace(/^.*data/, 'public/userData/archived');
          pathTo = imageService.pathStore(pathTo);
          fs.renameSync(path.resolve(pathFrom), pathTo);
          return socialNewsItem.images[index]  = image.replace(/data/, 'data/archived');
        });
      }
      socialNewsItem.markModified('images');
      socialNewsItem.archivedDate = Date.now();
      socialNewsItem.archivedBy = user.companyData.legalName;

      return socialNewsItem.save()
        .then((updatedSocialNews) => {
          // delete directory
          const pathTmp = path.resolve(`public/userData/${user._id}/${id}`);
          fsService.deleteDirectory(pathTmp);
          sendTask('deleteSocialNewsDependencies',
            {
              socialNewsItem: {
                _id:      id,
                sourceId: socialNewsItem.sourceId
              },
              user: {
                _id:          user._id,
                companyName:  user.companyData.legalName
              }
            }
          );
          return res.responses.success();
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

/*function deleteAttachment(req, res, next) {
  const body = req.body;
  const user = req.user;
  const conditions = {
    archivedDate: {$exists: false}
  };
  conditions[body.type] = body.name;
  return SocialNews.findOne(conditions)
    .then((socialNewsItem) => {
      if (!socialNewsItem) {
        return res.responses.notFoundResource('Новость не найдена.');
      }
      if (socialNewsItem.owner._id.toString() !== user._id.toString()) {
        return res.responses.accessDenied('Доступ запрещен.');
      }
      const pathFrom = body.name.replace(/^.*data/, 'public/userData');
      let pathTo = body.name.replace(/^.*data/, 'public/userData/archived');
      pathTo = imageService.pathStore(pathTo);
      fs.renameSync(path.resolve(pathFrom), pathTo);
      socialNewsItem[body.type].pull(body.name);
      socialNewsItem.dateModified = Date.now();
      socialNewsItem.modifiedBy = user.companyData.legalName;
      return socialNewsItem.save()
        .then((updatedSocialNews) => {
          return res.responses.success();
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}*/ // TODO: delete when front-end is ready

/*function publish(req, res, next) {
  const id = req.params._id;
  const user = req.user;
  const conditions = {
    _id: id,
    inMaking: true,
    archivedDate: {$exists: false}
  };
  return SocialNews.findOne(conditions)
    .then((socialNewsItem) => {
      if (!socialNewsItem) {
        return res.responses.notFoundResource('Новость не найдена.');
      }
      if (socialNewsItem.owner._id.toString() !== user._id.toString()) {
        return res.responses.accessDenied('Доступ запрещен.');
      }
      socialNewsItem.inMaking = false;
      socialNewsItem.dateModified = Date.now();
      socialNewsItem.datePublished = Date.now();
      socialNewsItem.modifiedBy = user.companyData.legalName;
      socialNewsItem.save()
        .then((updatedSocialNews) => {
          return res.responses.success();
        });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}*/

function like(req, res, next) {
  const id = req.params._id;
  const user = req.user;
  const conditions = {
    _id: id,
    inMaking: false,
    archivedDate: {$exists: false}
  };
  return SocialNews.findOne(conditions)
    .then((socialNewsItem) => {
      if (!socialNewsItem) {
        return res.responses.notFoundResource('Новость не найдена.');
      }
      if (socialNewsItem.owner._id.toString() === user._id.toString()) {
        return res.responses.accessDenied('Лайк своей новости запрещен.');
      }
      if (socialNewsItem.sourceId) {
        return res.responses.accessDenied('Репост нельзя лайкать.');
      }

      if (_.findIndex(socialNewsItem.likes, {_id: user._id}) === -1) {
        socialNewsItem.likesCount++;
        socialNewsItem.likes.push({
          _id: user._id,
          name: user.companyData.legalName,
          avatar: user.avatar
        });
      } else { // if liked then remove this like
        socialNewsItem.likesCount--;
        socialNewsItem.likes.remove({_id: user._id});
      }
      return socialNewsItem.save()
        .then((updatedSocialNews) => {
          return res.json({
            likes:      updatedSocialNews.likes.slice(limitOnDisplay),
            likesCount: updatedSocialNews.likesCount
          });
      });
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    })
}

function getMyNewsFeed(req, res, next) {
  const userId = req.user._id;
  const search = req.query.search;
  return Relationship.aggregate([
    {
      $match: {
        from: userId,
        type: constants.USER_RELATION.subscription
      }
    },
    {
      $group: {
        _id: false,
        list: { $push: '$to' }
      }
    }
  ])
    .then((listSources) => {
      listSources = listSources[0].list;
      listSources.push(userId);
      const conditions = {
        'owner._id':    { $in: listSources },
        showInUserMain: true,
        archivedDate:   {$exists: false},
        inMaking:       false
      };
      if (search) {
        conditions['$text'] = { $search: search };
      }
      const date = req.query.datePublished ? new Date(req.query.datePublished): new Date();
      conditions.datePublished = {
        '$lt': new Date(date)
      };
      return SocialNews.find(conditions, {
        dateModified:   0,
        modifiedBy:     0,
        // text:        0,
        showInUserMain: 0,
        inMaking:       0,
        likes: {
          '$slice': Number(limitOnDisplay)
        },
        reposts: {
          '$slice': Number(limitOnDisplay)
        }
      }, req.dataOptions)
        .populate('sourceId', {
          dateModified:    0,
          modifiedBy:      0,
          // text:         0,
          showInUserMain:  0,
          inMaking:        0,
          isHiddenComment: 0,
          likes: {
            '$slice': Number(limitOnDisplay)
          },
          reposts: {
            '$slice': Number(limitOnDisplay)
          }
        })
        .lean();
    })
    .then((socialNews) => {
      socialNews.forEach((socialNewsItem) => {
        if (!_.isEqual(socialNewsItem.text, socialNewsItem.description)) {
          socialNewsItem.isExistFullText = true;
          delete socialNewsItem.text;
        }

        if (socialNewsItem.sourceId && !_.isEqual(socialNewsItem.sourceId.text, socialNewsItem.sourceId.description)) {
          socialNewsItem.sourceId.isExistFullText = true;
          delete socialNewsItem.sourceId.text;
        }
      });
      return res.json(socialNews);
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

// region Module auxiliary function.
function checkFiles(storedDir, receivedFiles) {
  const result = [];

  if (!fs.existsSync(path.resolve(storedDir))) {
    return result;
  }

  const storedFiles = fs.readdirSync(path.resolve(storedDir));
  // delete files outside list
  storedFiles.forEach((storedFile) => {
    /*
    // images duplicate
    if (/@2x\./.test(storedFile)) {
      return;
    }
    */
    const isFound = receivedFiles.find((receivedFile) => {
      const regexp = new RegExp(`\/${storedFile}$`);
      return regexp.test(receivedFile);
    });

    if (isFound) {
      return result.push(isFound);
    }
    let pathFile = path.join(storedDir, storedFile);
    try {
      fs.unlinkSync(pathFile);
    } catch (err) {
      errorService.errorOutput(err, 'Error SaveNews when delete unnecessary file ');
    }
    /*
    pathFile = pathFile.replace(/\.[a-zA-Z]{3,4}$/, '@2x$&');
    if (fs.existsSync(pathFile)) {
      fs.unlinkSync(pathFile);
    }
    */
  });

  // save file order
  receivedFiles.some((receivedFile, index) => {
    const isFound = result.find((file) => {
      return _.isEqual(file, receivedFile);
    });
    if (!isFound) {
      receivedFiles.splice(index, 1);
    }
  });
  return receivedFiles;
}

function uploadHandler (req, res, next) {
  upload(req, res, (err) => {
    if (err) {
      return res.responses.requestError('Ошибка при загрузке файлов.') ;
    }
    return next();
  })
}
// endregion.

router.get(   '/user/:_id([0-9A-F]{24})',     dataOptions({sort: '-datePublished'}),  getAll);
router.get(   '/:_id([0-9A-F]{24})/text',                                             getText);
router.get(   '/:_id([0-9A-F]{24})',                                                  getById);
router.get(   '/:_id([0-9A-F]{24})/edited',   passport.authenticate('jwt', { session: false }), acl.withoutPerson, getForEditing);
router.post(  '/',                            passport.authenticate('jwt', { session: false }), acl.withoutPerson,
  validate(socialNewsValid.createRepost),         create);
router.put(   '/:_id([0-9A-F]{24})',          passport.authenticate('jwt', { session: false }), acl.withoutPerson,
  validate(socialNewsValid.createSocialNewItem),  change);
router.put(   '/:_id([0-9A-F]{24})/files',    passport.authenticate('jwt', { session: false }), acl.withoutPerson, uploadHandler, addFiles);
router.delete('/:_id([0-9A-F]{24})',          passport.authenticate('jwt', { session: false }), acl.withoutPerson, deleteSocialNews);
//router.delete('/attachment',                  passport.authenticate('jwt', { session: false }), acl.withoutPerson,
//  validate(socialNewsValid.deleteAttachment),     deleteAttachment);
//router.put(   '/:_id([0-9A-F]{24})/publish',  passport.authenticate('jwt', { session: false }), acl.withoutPerson, publish);
router.put(   '/:_id([0-9A-F]{24})/like',     passport.authenticate('jwt', { session: false }), acl.withoutPerson,
  socialNewsService.checkLikesLimit, like);
router.get(   '/',                            passport.authenticate('jwt', { session: false }), dataOptions({sort: '-datePublished'}),  getMyNewsFeed);

module.exports = router;
