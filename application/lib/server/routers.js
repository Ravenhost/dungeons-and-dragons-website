module.exports = (app) => {
  // News feed
  // app.use('/api/news',          require('./controllers/newsController'));
  // app.use('/api/images',        require('./controllers/imagesController'));
  // Partner network
  // app.use('/api/auth',          require('./controllers/authController'));
  app.use('/api/users',         require('./controllers/usersController'));
  // app.use('/api/social-news',   require('./controllers/socialNewsController'));
  // app.use('/api/subscribe',     require('./controllers/subscriptionsController'));
  // app.use('/api/partners',      require('./controllers/partnersController'));
  // app.use('/api/requests',      require('./controllers/partnerRequestsController'));
  app.use('/api/spells', require('./controllers/spellsController'));
};
