// region Module dependencies.
const path =      require('path');
const convict =   require('convict');
// endregion.

const conf = convict({
  env: {
    doc: 'The application environment.',
    format: ['qa', 'dev', 'staging', 'production'],
    'default': 'dev',
    env: 'NODE_ENV',
    arg: 'node-env'
  },
  uploadChunkSize: { doc: 'Upload file chunk size', format: Number, 'default': 70000 },
  db: {
    uri: {
      doc: 'Mongodb connection string',
      format: String, 'default': 'mongodb://127.0.0.1/dnd-spells-db',
      env: 'MONGO_URI'
    },
    dropDatabaseAlways: { format: Boolean, 'default': false },
    autoApplyMigrations: {
      doc: 'Auto apply migrations',
      format: Boolean,
      'default': false
    },
    debug: { format: Boolean, 'default': false }
  },
  http: {
    session: { doc: 'Session type', format: String, 'default': 'memory' },
    serveStatic: { doc: 'Enable processing static content requests', format: Boolean, 'default': true },
    host: { doc: 'React fetching host', format: String, 'default': 'http://localhost' },
    port: { doc: 'Http listening port', format: 'port', 'default': 80, arg: 'http-port' },
    url: { format: String }
  },
  session: {
    secret: { format: String, 'default': 'xjksxnjzJHDHHD' },
    maxAge: { format: Number, 'default': 300000 }
  },
  dadata: {
    apiKey: {
      doc: 'Dadata api-key',
      format: String,
      'default': 'ab5b28603d036b55ea8c0574893395f47a096f11'
    }
  },
  mailgun: {
    auth: {
      api_key: {
        doc: 'Mailgun api-key',
        format: String,
        'default': 'key-715ef36190cda03fc4803e085bef8def'
      },
      domain: {
        doc: 'Domain name',
        format: String,
        'default': 'rateprice.ru'
      }
    }
  },
  mail: {
    from: {
      doc: 'Default mail sender',
      format: String,
      'default': 'no-reply@partner-network.com'
    },
    emailSupport: {
      doc: 'Default email signature',
      format: String,
      'default': 'support@partner-network.com'
    },
    notificationSubject: {
      doc: 'Default email notification subject',
      format: String,
      'default': 'Оповещение "Партнерская сеть"'
    },
    hostname: {
      format: String,
      'default': 'localhost:8080'
    },
    invitationTokenLength: {
      format: Number,
      'default': 32
    }
  },
  tokenTTL: {
    doc: 'Time to live of tokens in hours',
    format: Number,
    'default': 2
  },
  passwordTTL: {
    doc: 'Time to live of key reset password in hours',
    format: Number,
    'default': 2
  },
  jwt: {
    secret: {
      format: String,
      'default':'BLAblaBlA'
    },
    expiresIn: {
      doc: 'Expressed in seconds or a string describing a time span zeit/ms.',
      'default': '2 days'
    }
  },
  newDescription: {
    doc: 'Length of new\'s description',
    format: Number,
    'default': 400
  },
  defaultImages: {
    doc: 'Default avatar for users',
    format: String,
    'default': 'default-images/default-avatar.jpg'
  },
  imageSizeLimits: {
    avatar: {
      max: {
        width: {
          doc: 'Default max width of avatar',
          format: Number,
          'default': 1600
        },
        height: {
          doc: 'Default max height of avatar',
          format: Number,
          'default': 1600
        }
      },
      min: {
        width: {
          doc: 'Default min width of avatar',
          format: Number,
          'default': 400
        },
        height: {
          doc: 'Default min height of avatar',
          format: Number,
          'default': 200
        }
      },
      forComment: {
        width: {
          doc: 'Default width of avatar in comment',
          format: Number,
          'default': 100
        }
      }
    },
    socialNews: {
      min: {
        width: {
          doc: 'Default min width of image in post',
          format: Number,
          'default': 800
        }
      }
    }
  },
  likesLimit: {
    perDay: {
      doc: 'Limit on count of likes during day',
      format: Number,
      'default': 200
    },
    forNews: {
      doc: 'Limit on count of likes displayed into social news',
      format: Number,
      'default': 5
    }
  },
  dataOptions: {
    sort: {
      doc: 'Default sorting field',
      format: String,
      'default': '-modifyDate'
    },
    perPage: {
      doc: 'Default number of elements into 1 page',
      format: Number,
      'default': 20
    },
    maxPerPage: {
      doc: 'Max number of elements into 1 page',
      format: Number,
      'default': 100
    }
  },
  rabbit: {
    url: {
      'default': 'amqp://admin:ADmIn72@localhost:5672/'
    },
    queueTaskName: { format: String, 'default': 'tasks' },
    prefetchCount: { format: Number, 'default': 1 },
    tasksCount: { format: Number, 'default': 5 }
  }
});

const filePath = path.resolve(__dirname, 'env', `${conf.get('env')}.json`);

conf.loadFile(filePath);
conf.validate();

module.exports = conf;
