// region Module dependencies.
require('../models/newsModel');
require('../models/versionModel');
require('../models/imageModel');

const newsItemService = require('../services/newsItemService');
// endregion.

newsItemService.updateNewsFeed();
