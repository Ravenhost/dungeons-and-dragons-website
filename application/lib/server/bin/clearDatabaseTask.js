// region Module dependencies.
require('../models/newsModel');
require('../models/versionModel');

const mongoose =      require('mongoose');

const logger =        require('../logger');
const mongodb =       require('../mongodb');
const retryService =  require('../services/retryService');
const errorService =  require('../services/errorService').errorOutput;

const NewsItem = mongoose.model('NewsItem');
// endregion.

function deleteNews() {
  logger.debug('Start delete old news.');

  return mongodb.connect()
    .then(() => {
      const curDate = new Date();
      const date = curDate.setDate(curDate.getDate() - 3);
      const conditions = { 'pubDate': {'$lt': date} };
      return NewsItem.remove(conditions);
    })
    .finally(() => { return mongodb.disconnect(); })
    .catch((err) => { return errorService(err, 'Unexpected error when remove old news.')});
}

deleteNews();
