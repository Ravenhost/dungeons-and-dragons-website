// region Module dependencies.
const mongoose  = require('mongoose');
const Promise   = require('bluebird');
const _         = require('lodash');

const SocialNews  = mongoose.model('SocialNews');
// endregion

const deleteSocialNewsDependencies = {
  name: 'deleteSocialNewsDependencies',
  handler: (context) => {
    const socialNewsItem = context.socialNewsItem;
    const user = context.user;

    const updatedEntities = [
      { // delete reposts
        model:      SocialNews,
        conditions: { sourceId: socialNewsItem._id },
        doc:        { $set: { archivedDate: Date.now(), archivedBy: user.companyName } }
      },
      { // decrement of repost
        model:      SocialNews,
        conditions: { _id: socialNewsItem.sourceId },
        doc: {
          $inc:   { repostsCount: -1 },
          $pull:  { reposts: {_id: user._id} }
        }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;
      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

module.exports = {
  deleteSocialNewsDependencies: deleteSocialNewsDependencies
};
