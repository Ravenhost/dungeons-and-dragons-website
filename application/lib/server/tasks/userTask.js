// region Module dependencies.
const mongoose  = require('mongoose');
const Promise   = require('bluebird');
const _         = require('lodash');

const SocialNews  = mongoose.model('SocialNews');
// endregion

const updateUserAvatar = {
  name: 'updateUserAvatar',
  handler: (context) => {
    const userId = context.userId;
    const avatar = context.avatar;

    const updatedEntities = [
      {
        model:      SocialNews,
        conditions: { 'owner._id': userId },
        doc:        { $set: { 'owner.avatar': avatar } }
      },
      {
        model:      SocialNews,
        conditions: { likes: { $elemMatch: { _id: userId } } },
        doc:        { $set: { 'likes.$.avatar': avatar } }
      },
      {
        model:      SocialNews,
        conditions: { reposts: { $elemMatch: { _id: userId } } },
        doc:        { $set: { 'reposts.$.avatar': avatar } }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;

      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

module.exports = {
  updateUserAvatar: updateUserAvatar
};
