// region Module dependencies.
const mongoose  = require('mongoose');
const Promise   = require('bluebird');
const _         = require('lodash');

const User  = mongoose.model('User');
const CounterRelation  = mongoose.model('CounterRelation');
// endregion

const addPartner = {
  name: 'addPartner',
  handler: (context) => {
    const userId = context.userId;
    const partnerId = context.partnerId;

    const updatedEntities = [
      {
        model:      User,
        conditions: { _id: userId },
        doc:        { $push: { partners: partnerId } }
      },
      {
        model:      CounterRelation,
        conditions: { user: userId },
        doc:        { $inc: { partners: 1 } }
      },
      {
        model:      CounterRelation,
        conditions: { user: userId },
        doc:        { $inc: { incoming: -1 } }
      },
      {
        model:      CounterRelation,
        conditions: { user: partnerId },
        doc:        { $inc: { outgoing: -1 } }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;

      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

const deletePartner = {
  name: 'deletePartner',
  handler: (context) => {
    const userId = context.userId;
    const partnerId = context.partnerId;

    const updatedEntities = [
      {
        model:      User,
        conditions: { _id: userId },
        doc:        { $pull: { partners: partnerId } }
      },
      {
        model:      CounterRelation,
        conditions: { user: userId },
        doc:        { $inc: { partners: -1 } }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;

      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

module.exports = {
  addPartner:    addPartner,
  deletePartner: deletePartner
};
