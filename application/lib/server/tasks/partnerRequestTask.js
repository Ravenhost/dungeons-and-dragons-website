// region Module dependencies.
const mongoose  = require('mongoose');
const Promise   = require('bluebird');
const _         = require('lodash');

const User  = mongoose.model('User');
const CounterRelation  = mongoose.model('CounterRelation');
// endregion

const addRequest = {
  name: 'addRequest',
  handler: (context) => {
    const userId = context.userId;
    const partnerId = context.partnerId;

    const updatedEntities = [
      {
        model:      CounterRelation,
        conditions: { user: partnerId },
        doc:        { $inc: { incoming: 1 } }
      },
      {
        model:      CounterRelation,
        conditions: { user: userId },
        doc:        { $inc: { outgoing: 1 } }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;

      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

const deleteRequest = {
  name: 'deleteRequest',
  handler: (context) => {
    const userId = context.userId;
    const partnerId = context.partnerId;

    const updatedEntities = [
      {
        model:      CounterRelation,
        conditions: { user: partnerId },
        doc:        { $inc: { incoming: -1 } }
      },
      {
        model:      CounterRelation,
        conditions: { user: userId },
        doc:        { $inc: { outgoing: -1 } }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;

      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

module.exports = {
  addRequest:    addRequest,
  deleteRequest: deleteRequest
};
