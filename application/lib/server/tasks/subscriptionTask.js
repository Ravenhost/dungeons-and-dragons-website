// region Module dependencies.
const mongoose  = require('mongoose');
const Promise   = require('bluebird');
const _         = require('lodash');

const constants = require('../constants');

const User            = mongoose.model('User');
const Relationship    = mongoose.model('Relationship');
const CounterRelation = mongoose.model('CounterRelation');
// endregion

const addSubscription = {
  name: 'addSubscription',
  handler: (context) => {
    const userId = context.userId;
    const partnerId = context.partnerId;

    const updatedEntities = [
      {
        model:      CounterRelation,
        conditions: { user: partnerId },
        doc:        { $inc: { followers: 1 } }
      },
      {
        model:      CounterRelation,
        conditions: { user: userId },
        doc:        { $inc: { subscriptions: 1 } }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;

      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

const deleteSubscription = {
  name: 'deleteSubscription',
  handler: (context) => {
    const userId = context.userId;
    const partnerId = context.partnerId;

    const updatedEntities = [
      {
        model:      CounterRelation,
        conditions: { user: partnerId },
        doc:        { $inc: { followers: -1 } }
      },
      {
        model:      CounterRelation,
        conditions: { user: userId },
        doc:        { $inc: { subscriptions: -1 } }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;

      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

const viewedSubscriptions = {
  name: 'viewedSubscriptions',
  handler: (context) => {
    const userId = context.userId;

    const updatedEntities = [
      {
        model:      Relationship,
        conditions: {
          to:     userId,
          viewed: false,
          type:   constants.USER_RELATION.subscription
        },
        doc:        { $set: { viewed: true } }
      }
    ];

    return Promise.map(updatedEntities, (updatedEntity) => {
      const Model = updatedEntity.model;
      const baseConditions = {
        archivedDate: { $exists: false }
      };
      const resultConditions = _.assign({}, baseConditions, updatedEntity.conditions);
      const doc = updatedEntity.doc;

      return Model.update(resultConditions, doc, { multi: true });
    });
  }
};

module.exports = {
  addSubscription:     addSubscription,
  deleteSubscription:  deleteSubscription,
  viewedSubscriptions: viewedSubscriptions
};
