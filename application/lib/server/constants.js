module.exports = {
  RSS: {
    MAILRU: {
      title: 'Mail.Ru',
      link: 'https://news.mail.ru/',
      main: ['https://news.mail.ru/rss/main/'],
      economy: ['https://news.mail.ru/rss/economics/'],
      politics: ['https://news.mail.ru/rss/politics/']
    },
    RAMBLER: {
      title: 'Rambler',
      link: 'https://news.rambler.ru/',
      main: ['https://news.rambler.ru/rss/head/'],
      economy: ['https://news.rambler.ru/rss/business/'],
      politics: ['https://news.rambler.ru/rss/politics/']
    },
    LENTA: {
      title: 'Lenta.ru',
      link: 'https://lenta.ru/',
      main: ['https://lenta.ru/rss/top7'],
      economy: ['http://lenta.ru/rss/news/business', 'http://lenta.ru/rss/news/economics'],
      politics: []
    },
    REGNUM: {
      title: 'Regnum',
      link: 'https://regnum.ru/',
      main: [],
      economy: ['https://regnum.ru/rss/economy'],
      politics: ['https://regnum.ru/rss/polit']
    }
  },
  TRANSLATE_CATEGORY: {
    main: 'Главные новости',
    economy: 'Новости экономики',
    politics: 'Новости политики'
  },
  COUNT_DEFAULT_IMAGE: 7,
  USER_TYPE: {
    organization: 0,
    entrepreneur: 1,
    person: 2
  },
  USER_RELATION: {
    subscription:   0,
    partnerRequest:  1,
    partners:       2
  },
  DEFAULT_IMAGE_SIZE: {
    width: 420,
    height: 630
  }
};
