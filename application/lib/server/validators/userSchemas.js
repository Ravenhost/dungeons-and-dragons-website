// region Module dependencies.
const _ =             require('lodash');

const constants =     require('../constants').USER_TYPE;
const trimStrings =   require('../services/sanitizeService').trimStrings;
// endregion.

function changeMainPage(req) {
  req.body = trimStrings(req.body);
  const user = req.user;
  const baseSchema = {};
  if (_.isEqual(user.type, constants.organization) || _.isEqual(user.type, constants.entrepreneur)) {
    _.assign(baseSchema, {
      'brand': {
        optional: {options: [{checkFalsy: true}]},
        isLength: {
          options: [{max: 255}],
          errorMessage: 'Максимальная длина 255 символов.'
        }
      }
    });
  }
  if (_.isEqual(user.type, constants.person)) {
    _.assign(baseSchema, {
      'firstName': {
        optional: {options: [{checkFalsy: true}]},
        isLength: {
          options: [2, 20],
          errorMessage: 'Допустимая длина имени - до 20 символов.'
        }
      },
      'middleName': {
        optional: {options: [{checkFalsy: true}]},
        isLength: {
          options: [2, 30],
          errorMessage: 'Допустимая длина отчества - до 30 символов.'
        }
      },
      'lastName': {
        optional: {options: [{checkFalsy: true}]},
        isLength: {
          options: [2, 60],
          errorMessage: 'Допустимая длина фамилии - до 60 символов.'
        }
      }
    });
  }
  return baseSchema;
}

function changeAvatar(req) {
  req.body = trimStrings(req.body);
  return {
    'avatar': {
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    }
  };
}

module.exports = {
  changeMainPage: changeMainPage,
  changeAvatar:   changeAvatar
};
