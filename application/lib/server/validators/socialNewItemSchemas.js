// region Module dependencies.
const _ =             require('lodash');
const mongoose =      require('mongoose');

const trimStrings =   require('../services/sanitizeService').trimStrings;
const config =        require('../config');

const SocialNews = mongoose.model('SocialNews');
// endregion.

function createSocialNewItem(req) {
  req.body = trimStrings(req.body);
  return {
    'title': {
      optional: {options: [{checkFalsy: true}]},
      isLength: {
        options: [{ max: 150 }],
        errorMessage: 'Максимальный размер заголовка новости - 150 символов.'
      }
    },
    'text': {
      optional: {options: [{checkFalsy: true}]},
      isLength: {
        options: [{ max: 15895 }],
        errorMessage: 'Максимальный размер новости - 15895 символов.'
      }
    },
    'images': {
      optional: {options: [{checkFalsy: true}]},
      isArray: {
        errorMessage: 'Должен быть массив.'
      },
      isLengthArray: {
        options: [10],
        errorMessage: 'Количество картинок должно быть от 1 до 10.'
      }
    },
    'files': {
      optional: {options: [{checkFalsy: true}]},
      isArray: {
        errorMessage: 'Должен быть массив.'
      },
      isLengthArray: {
        options: [10],
        errorMessage: 'Количество картинок должно быть от 1 до 10.'
      }
    },
    'inMaking': {
      optional: {options: [{checkFalsy: true}]},
      isBoolean: {
        errorMessage: 'Допустимые значения: true и false.'
      }
    }
  };
}

function deleteAttachment(req) {
  req.body = trimStrings(req.body);
  return {
    'type': {
      isContains: {
        options: [['images', 'files']],
        errorMessage: 'Допустимые значения: images и files.'
      },
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    },
    'name': {
      isURL: { errorMessage: 'Удаляемое вложение должно быть ссылкой.' },
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    }
  };
}

function createRepost(req) {
  req.body = trimStrings(req.body);
  return {
    'sourceId': {
      optional: {options: [{checkFalsy: true}]},
      isExist: {
        options: [SocialNews],
        errorMessage: 'Несуществующий идентификатор новости.'
      }
    }
  }
}

module.exports = {
  createSocialNewItem:  createSocialNewItem,
  deleteAttachment:     deleteAttachment,
  createRepost:         createRepost
};
