// region Module dependencies.
const _ =             require('lodash');

const trimStrings =   require('../services/sanitizeService').trimStrings;
const config =        require('../config');
const constants =     require('../constants').USER_TYPE;
// endregion.

function checkRequisites(req) {
  req.body = trimStrings(req.body);
  const baseSchema = {
    'type': {
      isInt: {
        options: [{ min: 0, max: 1 }],
        errorMessage: 'Неверное значение. Допустимые значения: 0 или 1.'
      },
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    },
    'email': {
      isCustomEmail: { errorMessage: 'Неверный формат электронной почты.' },
      isUserExistsByEmail: { errorMessage: 'Пользователь с таким адресом электронной почты уже зарегистрирован в системе.' },
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    },
    'password': {
      isLength: {
        options: [{ min: 8 }],
        errorMessage: 'Минимальная длина пароля 8 символов.'
      },
      isPassword: { errorMessage: 'Пароль должен содержать не менее 8 символов, хотя бы одну цифру, одну прописную и одну строчную буквы.' },
      notEmpty: {errorMessage: 'Поле должно быть заполнено.'}
    }
  };
  if (_.isEqual(req.body.type, constants.organization)) {
    _.assign(baseSchema, {
      'ogrn': {
        isLength: {
          options: [13, 13],
          errorMessage: 'Допустимая длина 13 символов.'
        },
        notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
      }
    });
  }
  if (_.isEqual(req.body.type, constants.entrepreneur)) {
    _.assign(baseSchema, {
      'ogrn': {
        isLength: {
          options: [15, 15],
          errorMessage: 'Допустимая длина 15 символов.'
        },
        notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
      }
    });
  }
  return baseSchema;
}

function signUp(req) {
  req.body = trimStrings(req.body);
  const baseSchema = {
    'type': {
      isInt: {
        options: [{ min: 0, max: 2 }],
        errorMessage: 'Неверное значение. Допустимые значения: 0, 1 или 2.'
      },
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    },
    'email': {
      isCustomEmail: { errorMessage: 'Неверный формат электронной почты.' },
      isUserExistsByEmail: { errorMessage: 'Пользователь с таким адресом электронной почты уже зарегистрирован в системе.' },
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    },
    'password': {
      isLength: {
        options: [{ min: 8 }],
        errorMessage: 'Минимальная длина пароля 8 символов.'
      },
      isPassword: { errorMessage: 'Пароль должен содержать не менее 8 символов, хотя бы одну цифру, одну прописную и одну строчную буквы.' },
      notEmpty: {errorMessage: 'Поле должно быть заполнено.'}
    }
  };

  // end registration for organizer
  if (_.isEqual(req.body.type, constants.organization)) {
    _.assign(baseSchema, {
      'ogrn': {
        isLength: {
          options: [13, 13],
          errorMessage: 'Допустимая длина 13 символов.'
        },
        notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
      },
      'brand': {
        optional: {options: [{checkFalsy: true}]},
        isLength: {
          options: [{ max: 255 }],
          errorMessage: 'Максимальная длина 255 символов.'
        }
      }
    });
  }

  // end registration for entrepreneur
  if (_.isEqual(req.body.type, constants.entrepreneur)) {
    _.assign(baseSchema, {
      'ogrn': {
        isLength: {
          options: [15, 15],
          errorMessage: 'Допустимая длина 15 символов.'
        },
        notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
      },
      'brand': {
        optional: {options: [{checkFalsy: true}]},
        isLength: {
          options: [{ max: 255 }],
          errorMessage: 'Максимальная длина 255 символов.'
        }
      }
    });
  }

  // start registration for individual
  if (_.isEqual(req.body.type, constants.person)) {
    _.assign(baseSchema, {
      'firstName': {
        isLength: {
          options: [2, 20],
          errorMessage: 'Допустимая длина имени - до 20 символов.'
        },
        notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
      },
      'middleName': {
        optional: {options: [{checkFalsy: true}]},
        isLength: {
          options: [2, 30],
          errorMessage: 'Допустимая длина отчества - до 30 символов.'
        }
      },
      'lastName': {
        isLength: {
          options: [2, 60],
          errorMessage: 'Допустимая длина фамилии - до 60 символов.'
        },
        notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
      }
    });
  }

  return baseSchema;
}

function checkToken(req) {
  req.body = trimStrings(req.body);
  const tokenLength = config.get('mail.invitationTokenLength') * 2; // length is in bytes
  return {
    token: {
      isLength: {
        options: [tokenLength, tokenLength],
        errorMessage: `Допустимая длина ${tokenLength} символов.`
      },
      notEmpty: {errorMessage: 'Поле должно быть заполнено'}
    }
  };
}

function signIn(req) {
  req.body = trimStrings(req.body);
  return {
    'email': {
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    },
    'password': {
      notEmpty: {errorMessage: 'Поле должно быть заполнено.'}
    }
  };
}

function checkEmail(req) {
  req.body = trimStrings(req.body);
  return {
    'email': {
      notEmpty: { errorMessage: 'Поле должно быть заполнено.' }
    }
  };
}

function checkPassword(req) {
  req.body = trimStrings(req.body);
  const tokenLength = config.get('mail.invitationTokenLength') * 2; // length is in bytes
  return {
    'password': {
      isLength: {
        options: [{ min: 8 }],
        errorMessage: 'Минимальная длина пароля 8 символов.'
      },
      isPassword: { errorMessage: 'Пароль должен содержать не менее 8 символов, хотя бы одну цифру, одну прописную и одну строчную буквы.' },
      notEmpty: {errorMessage: 'Поле должно быть заполнено.'}
    },
    token: {
      isLength: {
        options: [tokenLength, tokenLength],
        errorMessage: `Допустимая длина ${tokenLength} символов.`
      },
      notEmpty: {errorMessage: 'Поле должно быть заполнено'}
    }
  };
}

module.exports = {
  signUp:           signUp,
  checkRequisites:  checkRequisites,
  checkToken:       checkToken,
  signIn:           signIn,
  checkEmail:       checkEmail,
  checkPassword:    checkPassword
};
