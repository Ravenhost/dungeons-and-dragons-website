// region Module dependencies.
const path = require('path');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const fsp = require('fs-promise');
const mongoose = require('mongoose');
const Promise = require('bluebird');

const User = mongoose.model('User');
const Spell = mongoose.model('Spell');
// endregion

function createUsers() {
  const usersFilePath = path.resolve(__dirname, 'json', 'users.json');
  return fsp.readFile(usersFilePath)
    .then((text) => {
      let users = JSON.parse(text);
      return Promise.each(users, (user) => {
        const userDoc = {
          email: user.email,
          role: user.role,
          firstName: user.firstName,
          middleName: user.middleName,
          secondName: user.secondName
        };
        bcrypt.genSalt = Promise.promisify(bcrypt.genSalt);
        bcrypt.hash = Promise.promisify(bcrypt.hash);
        return bcrypt.genSalt()
          .then((salt) => {
            return bcrypt.hash(user.password, salt);
          })
          .then((hash) => {
            return User.create(_.assign(userDoc, {password: hash}));
          });
      });
    });
}

function createSpells() {
  const usersFilePath = path.resolve(__dirname, 'json', 'spells.json');
  return fsp.readFile(usersFilePath)
    .then((text) => {
      let spells = JSON.parse(text);
      return Promise.each(spells, (spell) => {
        const spellDoc = {
          classColor: spell.classColor,
          className: spell.className,
          spellTitle: spell.spellTitle,
          spellType: spell.spellType,
          castingTimeValue: spell.castingTimeValue,
          rangeValue: spell.rangeValue,
          componentsValue: spell.componentsValue,
          durationValue: spell.durationValue,
          spellDescription: spell.spellDescription,
        };
        return Spell.create(spellDoc);
      });
    });
}


function getInfo() {
  return Promise.resolve({
    version: '0.0.1',
    requiredVersion: '0.0.0'
  });
}

function migrate() {
  return Promise.resolve()
    .then(createUsers)
    .then(createSpells);
}

exports.getInfo = getInfo;
exports.migrate = migrate;
