// region Module dependencies.
const constants   = require('../constants');
const userService = require('../services/userService');
// endregion

function withoutPerson(req, res, next) {
  const user = req.user;
  if (!user) { return res.responses.unauthorized(); }
  if (user.type === constants.USER_TYPE.person) { return res.responses.accessDenied('Доступ запрещен.'); }
  return next();
}

async function getUserByToken(req, res, next) {
  if (!req.headers.authorization) {
    return next();
  }

  // get the last part from a authorization header string like "JWT token-value"
  const token = req.headers.authorization.split(' ')[1];
  const user = await userService.getUserByToken(token);

  if (user) {
    req.user = user;
  }

  return next();
}

module.exports = {
  withoutPerson:  withoutPerson,
  getUserByToken: getUserByToken
};
