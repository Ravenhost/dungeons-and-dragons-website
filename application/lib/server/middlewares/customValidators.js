// region Module dependencies.
const expressValidator =  require('express-validator');
const _ =                 require('lodash');
const validator =         require('validator');

const userService =       require('../services/userService');
// endregion

function isContains(item, targetItems) {
  if (!item) {
    return false;
  }
  return _.includes(targetItems, item);
}

function isCustomEmail(email) {
  return /^[-a-zA-Z0-9!#$%&'*+\/=?^_`{|}~]+@[a-z0-9.\-]+$/.test(email);
}

function isUserExistsByEmail(email) {
  if (!email) {return Promise.resolve();}
  return userService.isExistsEmail(email)
    .then((isExist) => {
      if (isExist) { return Promise.reject(); }
      return Promise.resolve();
    });
}

function isPassword(password) {
  return /(?=.*[0-9])(?=.*[а-яёa-z])(?=.*[A-ZА-ЯЁ])[0-9a-zA-Z.,';\]\[{}:"<>?!@#$%^&*()_\-+=|\/№А-Яа-яЁё]{8,}/.test(password);
}

function isLengthArray(array, length) {
  return array.length <= length;
}

function isExist(items, Model) {
  if (!items) { return false; }
  if (!_.isArray(items)) { items = [items]; }
  let ids;
  if (_.isPlainObject(items[0])) {
    ids = _.map(items, '_id');
  } else {
    ids = items;
  }
  return Promise.resolve()
    .then(() => {
      const conditions = {_id: {$in: ids}};
      if (Model.schema.paths.archivedDate) { conditions.archivedDate = {$exists: false}; }
      if (Model.schema.paths.inMaking)     { conditions.inMaking = false; }
      const isMongoIds = _.some(ids, (id) => { return validator.isMongoId(id); });
      if (!isMongoIds) { return Promise.reject(); }
      return Model.count(conditions)
        .then((count) => {
          if (items.length !== count) { return Promise.reject(); }
          return Promise.resolve();
        });
    });
}

function isArray (value) {
  return Array.isArray(value);
}

module.exports = () => {
  return expressValidator({
    customValidators: {
      isContains:           isContains,
      isCustomEmail:        isCustomEmail,
      isUserExistsByEmail:  isUserExistsByEmail,
      isPassword:           isPassword,
      isLengthArray:        isLengthArray,
      isExist:              isExist,
      isArray:              isArray
    }
  });
};
