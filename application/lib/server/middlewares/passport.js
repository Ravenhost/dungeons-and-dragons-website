// region Module dependencies.
const passport =        require('passport');
const LocalStrategy =   require('passport-local').Strategy;
const JwtStrategy =     require('passport-jwt').Strategy;
const ExtractJwt =      require('passport-jwt').ExtractJwt;
const bcrypt =          require('bcryptjs-then');
const VError =          require('verror');
const mongoose =        require('mongoose');

const config =          require('../config');

const User = mongoose.model('User');
// endregion

module.exports = (app) => {
  // Serialize sessions
  passport.serializeUser((user, done) => {
    return done(null, user._id);
  });

  // Deserialize sessions
  passport.deserializeUser((_id, done) => {
    User.findOne({_id}, { password: false })
      .then((user) => {
        return done(null, user);
      })
      .catch((err) => {
        return done(err);
      });
  });

  // Basic стратегия
  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password'
    },
    (email, password, next) => {
      return User.findOne({email: email}).lean()
        .then((user) => {
          if (!user) {
            return next(null, false, {message: 'Неверный email или пароль.'});
          }
          if (user.archived) {
            return next(null, false, {message: 'Пользователь архивирован.'});
          }
          if (!user.emailConfirmed) {
            return next(null, false, {message: 'Пользователь не подтвердил e-mail.'});
          }
          return bcrypt.compare(password, user.password)
            .then((res) => {
              if (!res) {
                return next(null, false, {message: 'Неверный email или пароль.'});
              }
              delete user.password;
              return next(null, user);
            });
        })
        .catch((err) => {
          return next(new VError(err));
        });
    }
  ));

  // JWT стратегия
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  opts.secretOrKey = config.get('jwt.secret');

  passport.use(new JwtStrategy(opts, (jwt_payload, next) => {
    return User.findOne({_id: jwt_payload.userId}).lean()
      .then((user) => {
        if (!user) {
          return next(null, false);
        }
        return next(null, user);
      })
      .catch((err) => {
        return next(new VError(err));
      });
  }));

  // Add passport's middleware
  app.use(passport.initialize());
  app.use(passport.session());
};
