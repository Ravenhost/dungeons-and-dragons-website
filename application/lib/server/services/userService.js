// region Module dependencies.
const mongoose     = require('mongoose');
const  jwt         = require('jsonwebtoken');
const Promise = require('bluebird');

const config       = require('../config');
const errorService = require('./errorService').errorOutput;

const User = mongoose.model('User');
// endregion

function isExistsEmail(email) {
  const criteria = {
    $or: [
      { email: email }
    ]
  };
  return User.findOne(criteria)
    .then((user) => { return Boolean(user); });
}

function generateToken(user) {
  const payload = { userId: user._id };
  const options = {
    expiresIn: config.get('jwt.expiresIn'),
  };
  return jwt.sign(payload, config.get('jwt.secret'), options);
}

function getUserByToken(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.get('jwt.secret'), (err, decoded) => {
      if (err) {
        return resolve(null);
      }

      const userId = decoded.userId;

      return User.findById(userId).lean()
        .then((user) => {
          if (!user) {
            return resolve(null);
          }
          return resolve(user);
        })
        .catch((err) => {
          errorService(err, 'Error when get user by token');
          return resolve(null);
        });
    });
  });
}

module.exports = {
  isExistsEmail:  isExistsEmail,
  generateToken:  generateToken,
  getUserByToken: getUserByToken
};
