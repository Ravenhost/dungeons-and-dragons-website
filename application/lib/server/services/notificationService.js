// region Module dependencies.
const mailerService =           require('./mailerService');
const linkGenerationService =   require('./linkGenerationService');
// endregion.

function confirmEmail(data) {
  const notificationBody = {
    email: data.email,
    name: 'confirmationEmail',
    data: {
      confirmationLink: linkGenerationService.confirmationLink(data.token)
    },
    subject: 'Подтверждение e-mail адреса на сайте Партнерская сеть'
  };
  return mailerService.generationEmailNotification(notificationBody);
}

function resetPassword(data) {
  const notificationBody = {
    email: data.email,
    name: 'resetPassword',
    data: {
      confirmationLink: linkGenerationService.passwordLink(data.token)
    },
    subject: 'Восстановление пароля на сайте Партнерская сеть'
  };
  return mailerService.generationEmailNotification(notificationBody);
}

module.exports = {
  confirmEmail:   confirmEmail,
  resetPassword:  resetPassword
};
