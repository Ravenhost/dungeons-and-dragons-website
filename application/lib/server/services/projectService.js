// region Module dependencies
const mongoose =      require('mongoose');

const errorService =  require('../services/errorService').errorOutput;

const Project = mongoose.model('Project');
// endregion.

const RANDOM_NUMBER = 3; // on main page

function getRandomList(portfolioElements) {
  if (!portfolioElements) { return []; }
  const portfolio = portfolioElements.slice();
  const elementsId = [];
  for (let i = 0; i < RANDOM_NUMBER; i++) {
    if (portfolio.length === 0) { continue; }
    const index = Math.floor(Math.random() * (portfolio.length));
    elementsId.push(portfolio[index]);
    portfolio.splice(index, 1);
  }
  return Project.find({
    _id: { $in: elementsId },
    isHidden: false,
    archivedDate: { $exists: false }
  }, {
    image: 1,
    name: 1
  }).lean()
    .then((randomProjects) => {
      return randomProjects;
    })
    .catch((err) => {
      errorService(err, 'Error when get random projects');
      return [];
    });
}

module.exports = {
  getRandomList: getRandomList
};
