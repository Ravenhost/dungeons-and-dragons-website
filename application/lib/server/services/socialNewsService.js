// region Module dependencies.
const mongoose =    require('mongoose');
const moment =      require('moment');

const config =      require('../config');

const SocialNews =  mongoose.model('SocialNews');
// endregion.

function checkLikesLimit(req, res, next) {
  const dateFrom = moment.utc().subtract(1, 'days');
  const conditions = {
    likes: {
      $elemMatch: {
        _id:          req.user._id,
        dateCreated:  { $gte: dateFrom }
      }
    },
    archivedDate:     { $exists: false }
  };
  return SocialNews.count(conditions)
    .then((count) => {
      if (count < config.get('likesLimit.perDay')) {
        return next();
      }
      return res.responses.accessDenied('Вы превысили лимит на количество лайков в день.');
    })
    .catch((err) => {
      return next(err instanceof Error ? err : new VError(err));
    });
}

module.exports = {
  checkLikesLimit: checkLikesLimit
};
