// region Module dependencies.
const mongoose =      require('mongoose');
const Promise =       require('bluebird');
const feedParser =    require('feedparser-promised');

const logger =        require('../logger');
const mongodb =       require('../mongodb');
const constants =     require('../constants');
const errorService =  require('./errorService').errorOutput;
const imageService =  require('./imageService');
const retryService =  require('./retryService');

const NewsItem = mongoose.model('NewsItem');
// endregion.

function create(newsItem) {
  newsItem.date =  newsItem.date.setMinutes(newsItem.date.getMinutes() + 1);
  const curDate = new Date();
  const date = curDate.setDate(curDate.getDate() - 3);
  if (newsItem.date < date) { return; }
  return NewsItem.findOne({link: newsItem.link})
    .then((existingNewsItem) => {
      if(existingNewsItem) { return; }
      let imagePromise = null;
      const newsItemDoc = {
        title: newsItem.title,
        description: newsItem.description,
        pubDate: newsItem.date,
        link: newsItem.link,
        image: {},
        category: newsItem.newsCategory,
        source: {
          title: newsItem.newsSource,
          link: newsItem.newsSourceLink
        }
      };
      if(newsItem.image.link) { newsItemDoc.image.link = newsItem.image.link; }
      else { imagePromise = imageService.randomDefaultImage(); }
      return Promise.resolve(imagePromise)
        .then((imageId) => {
          if (imageId) { newsItemDoc.image._id = imageId; }
          return NewsItem.create(newsItemDoc);
        });
    })
    .catch((error) => {
      return errorService(error, `Unexpected Error when saving ${newsItem.newsSource} news item.`)
    });
}

async function updateNewsFeed(migration) {
  logger.debug('Start update news feed.');
  if(!migration) { await  mongodb.connect(); }

  const rssSource = constants.RSS;

  await retryService(getNews, rssSource.MAILRU);
  await retryService(getNews, rssSource.LENTA);
  await retryService(getNews, rssSource.RAMBLER);
  await retryService(getNews, rssSource.REGNUM);
  logger.debug('Done get ALL');
  if(!migration) { await mongodb.disconnect(); }
}

function getNews(rss) {
  return Promise.map(rss.main, (rssSource) => {
    return feedParser.parse(rssSource)
      .then((items) => {
        return Promise.each(items, (item) => {
          item.newsCategory = 'main';
          item.newsSource = rss.title;
          item.newsSourceLink = rss.link;
          create(item);
        });
      });
  })
    .then(() => {
      return Promise.map(rss.economy, (rssSource) => {
        return feedParser.parse(rssSource)
          .then((items) => {
            return Promise.each(items, (item) => {
              item.newsCategory = 'economy';
              item.newsSource = rss.title;
              item.newsSourceLink = rss.link;
              create(item);
            });
          });
      })
    })
    .then(() => {
      return Promise.map(rss.politics, (rssSource) => {
        return feedParser.parse(rssSource)
          .then((items) => {
            return Promise.each(items, (item) => {
              item.newsCategory = 'politics';
              item.newsSource = rss.title;
              item.newsSourceLink = rss.link;
              create(item);
            });
          });
      })
    })
    .then(() => {
      logger.debug(`Done get ${rss.title}`);
    })
    .catch((error) => {
      return errorService(error, `Unexpected Error when getting ${rss.title} news item.`);
    });
}

module.exports = {
  create:           create,
  updateNewsFeed:   updateNewsFeed
};
