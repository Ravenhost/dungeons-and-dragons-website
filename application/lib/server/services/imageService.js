// region Module dependencies.
const mongoose =    require('mongoose');
const gm =          require('gm');
const Promise =     require('bluebird');
const fs =          require('node-fs');
const path =        require('path');
const _ =           require('lodash');

const constants =   require('../constants');
const config =      require('../config');

const Image = mongoose.model('Image');
// endregion.

function randomDefaultImage() {
  const imageNumber = Math.floor(Math.random() * constants.COUNT_DEFAULT_IMAGE);
  return Image.findOne({'createdBy': 'default'}, {'_id': 1}).skip(imageNumber).lean()
    .then((image) => {
      return image._id;
    });
}

function getImageSize(imageBuffer) {
  return new Promise((resolve, reject) => {
    gm(imageBuffer)
      .size((err, size) => {
        if (err) { return reject(err); }
        return resolve(size);
      });
  });
}

function resizeImage(imageBuffer, sizeObj) {
  return new Promise((resolve, reject) => {
    gm(imageBuffer)
      .interlace('plane')
      .resize(sizeObj.width) // next options: height and option
      .autoOrient()
      .noProfile()
      .quality(100)
      .toBuffer((err, preparedBuffer) => {
        if (err) { return reject(err); }
        return resolve(preparedBuffer);
      });
  });
}

function thumbnailImage(imageBuffer, sizeObj) {
  return new Promise((resolve, reject) => {
    gm(imageBuffer)
      .resize(sizeObj.width, sizeObj.width, '^>')
      .gravity('Center')
      .extent(sizeObj.width, sizeObj.width)
      .toBuffer((err, preparedBuffer) => {
        if (err) { return reject(err); }
        return resolve(preparedBuffer);
      });
  });
}

function cropImage(imageBuffer, cropObj) {
  return new Promise((resolve, reject) => {
    return gm(imageBuffer)
      .interlace('plane')
      .crop(cropObj.width, cropObj.height, cropObj.x, cropObj.y)
      .autoOrient()
      .noProfile()
      .quality(100)
      .toBuffer((err, preparedBuffer) => {
        if (err) { return reject(err); }
        return resolve(preparedBuffer);
      });
  });
}

function writeImage(imageBuffer, newPath){
  return new Promise((resolve, reject) => {
    newPath = pathStore(newPath);
    return gm(imageBuffer)
      .write(newPath, (err) => {
        if (err) { return reject(err); }
        return resolve(true);
      });
  });
}

async function imageProcessing(file){
  const defaultSize = config.get(`imageSizeLimits.${file.type}`);
  let tempAvatar = file.path;
  const size = await getImageSize(file.path);

  if (size.width < defaultSize.min.width) {
    return {result: false, message: `Изображение меньше ${defaultSize.min.width} пикселей в ширину.`};
  }
  if (defaultSize.min.height && size.height < defaultSize.min.height) {
    return {result: false, message: `Изображение меньше ${defaultSize.min.height} пикселей в высоту.`};
  }
  if (!_.isEmpty(file.crop)) {
    tempAvatar = await cropImage(tempAvatar, file.crop);
  }
  if (file.type === 'socialNews') {
    tempAvatar = await resizeImage(tempAvatar, defaultSize.min);
  }
  if (defaultSize.max && size.width > defaultSize.max.width) {
    return {result: false, message: `Изображение больше ${defaultSize.max.width} пикселей в ширину.`};
  }
  if (defaultSize.max && defaultSize.max.height && size.height > defaultSize.max.height) {
    return {result: false, message: `Изображение больше ${defaultSize.max.height} пикселей в высоту.`};
  }

  // save image for retina
  // await writeImage(tempAvatar, file.newPath.replace(/\.[a-zA-Z]{3,4}(\.temp)?$/, '@2x$&'));
  // save image as usual
  // tempAvatar = await resizeImage(tempAvatar, { width: size.width / 2 });
  await writeImage(tempAvatar, file.newPath);
  // save image for comments
  if (file.type === 'avatar') {
    tempAvatar = await thumbnailImage(tempAvatar, defaultSize.forComment);
    await writeImage(tempAvatar, file.newPath.replace(/\.[a-zA-Z]{3,4}(\.temp)?$/, '-min$&'));
  }
  return { result: true };
}

function pathStore(newPath) {
  newPath = path.resolve(newPath);
  const result = path.parse(newPath);
  if (!fs.existsSync(result.dir)) {
    fs.mkdirSync(result.dir, 0o777, true);
  }
  return newPath;
}


module.exports = {
  randomDefaultImage: randomDefaultImage,
  imageProcessing:    imageProcessing,
  pathStore:          pathStore
};
