// region  Module dependencies.
const errorService = require('./errorService').errorOutput;

const maxRetryAttempts = 2;
// endregion.

async function retryFnCall(fn, ...args) {
  let retryCount = 0;
  do {
    try {
      return await fn.apply(this, args);
    } catch (err) {
      retryCount += 1;
      if (retryCount >= maxRetryAttempts) { return errorService(err, `Error during the execution of the ${fn.name} function.`); }
    }
  } while (retryCount < maxRetryAttempts);
}

module.exports = retryFnCall;
