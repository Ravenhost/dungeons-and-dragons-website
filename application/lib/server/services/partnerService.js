// region Module dependencies
const mongoose      = require('mongoose');

const constants     = require('../constants');
const errorService  = require('../services/errorService').errorOutput;

const User          = mongoose.model('User');
const Relationship  = mongoose.model('Relationship');
// endregion.

const RANDOM_NUMBER = 4; // on main page

function getRandomList(userId) {
  const listPartners = {
    count:    0,
    partners: []
  };

  return Relationship.find({
    $or: [
      { from: userId },
      { to:   userId }
    ],
    type: constants.USER_RELATION.partners
  })
    .then((partners) => {
      if (partners.length === 0) {
        return listPartners;
      }

      const partnersId = [];
      listPartners.count = partners.length;

      for (let i = 0; i < RANDOM_NUMBER; i++) {
        if (partners.length === 0) { continue; }

        const index = Math.floor(Math.random() * (partners.length));

        if (partners[index].from.toString() === userId.toString()) {
          partnersId.push(partners[index].to);
        } else {
          partnersId.push(partners[index].from);
        }
        partners.splice(index, 1);
      }

      return User.find({
        _id: { $in: partnersId },
        archived: false
      }, {
        avatar: 1,
        'companyData.legalName': 1
      }).lean()
        .then((randomPartners) => {
          listPartners.partners = randomPartners;
          return listPartners;
        });
    })
    .catch((err) => {
      errorService(err, 'Error when get random partners');
      return listPartners;
    });
}

function getMutualPartners(userId, anotherUserId, dataOptions, search) {
  const aggregationPipeline = [
    { $match: { _id: { $in: [userId, anotherUserId] } } },  // search necessary users
    { $project: { _id: 0, partners: 1 } },                // get only partners
    { $unwind: '$partners' },
    { $group: { _id: '$partners', count: { $sum: 1 } } }, // count the same elements
    { $match: { count: 2 } },                             // get only mutual
    {
      $lookup: {                                          // get info about partners
        from: 'users',
        localField: '_id',
        foreignField: '_id',
        as: '_id'
      }
    },
    { $project: { _id: { $arrayElemAt: [ '$_id', 0 ] } } } // transformation
  ];

  if (search) {
    aggregationPipeline.push({ $match: { '_id.companyData.legalName': search } })
  }

  if (dataOptions) {
    aggregationPipeline.push(
      { $skip: dataOptions.skip },
      { $limit: dataOptions.limit }
    );
  }

  aggregationPipeline.push(
    {
      $project: {                                 // get need field
        '_id._id':         1,
        '_id.avatar':      1,
        '_id.type':        1,
        '_id.description': 1,
        '_id.companyData.legalName':    1,
        '_id.companyData.director':     1,
        '_id.companyData.directorPost': 1
      }
    },
    { $group: { _id: false, mutual: { $push: '$_id' } } }  // create one array
  );

  return User.aggregate(aggregationPipeline)
    .then((result) => {
      if (result.length === 0) {
        return [];
      }
      return result[0].mutual;
    });
}

module.exports = {
  getRandomList:      getRandomList,
  getMutualPartners:  getMutualPartners
};
