// region Module dependencies.
const logger = require('../logger');
// endregion

function multerError(req, fieldName, originalName, message) {
  if (!req.files.errors) { req.files.errors = {}; }
  if (! req.files.errors[`${fieldName}`]) { req.files.errors[`${fieldName}`] = []; }

  req.files.errors[`${fieldName}`].push( `${originalName} - ${message}.` );
}

function errorOutput (err, msg) {
  logger.error(msg || err.msg);
  logger.error(err.stack);
};

module.exports = {
  errorOutput:  errorOutput,
  multerError:  multerError
};
