// region Module dependencies.
const request =       require('request-promise');
const VError =        require('verror');

const config =        require('../config');
const errorService =  require('./errorService').errorOutput;
// endregion.

const apiKey = config.get('dadata.apiKey');

module.exports = (ogrn) => {
  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: `Token ${apiKey}`
  };
  const body = { 'query': ogrn };
  return request({
    method: 'POST',
    url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party',
    headers: headers,
    body: JSON.stringify(body)
  })
    .then((data) => {
      data = JSON.parse(data);
      if (data && data.suggestions && data.suggestions[0]) {
        data = data.suggestions[0];
        const info = {
          legalName: data.unrestricted_value,
          inn: data.data.inn,
          kpp: data.data.kpp ? data.data.kpp : null,
          // data.data.address.data don't exist if entrepreneur and uncorrect
          legalAddress: null,
          director: data.data.management ? data.data.management.name : null,
          directorPost: data.data.management ? data.data.management.post : null,
          registerDate: data.data.state.registration_date,
          okvd: data.data.okved
        };
        if (data.data.address) {
          info.legalAddress = data.data.address.data && data.data.address.data.postal_code ?
            `${data.data.address.data.postal_code} ${data.data.address.unrestricted_value}` : `${data.data.address.unrestricted_value}`;
          if (data.data.address.data) {
            info.city = data.data.address.data.city;
          }
        }
        return info;
      }
      return null;
    })
    .catch((err) => {
      err = err instanceof Error ? err : new VError(err);
      errorService(err, 'Error get info about organization.');
      return null;
    });
};
