// region Module dependencies.
const crypto =    require('crypto');
const VError =    require('verror');
const Promise =   require('bluebird');

const logger =    require('../logger');

const randomBytesAsync = Promise.promisify(crypto.randomBytes, crypto);
// endregion.

function randomBytes(n) {
  return randomBytesAsync(n)
    .then((buf) => {
      return buf.toString('hex');
    })
    .catch((err) => {
      logger.error(err instanceof Error ? err : new VError(err));
      return Promise.reject(err);
    });
}

module.exports = {
  randomBytes: randomBytes
};
