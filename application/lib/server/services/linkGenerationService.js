// region Module dependencies.
const config = require('../config');

const HOSTNAME = config.get('mail.hostname');
// endregion.

function confirmationLink(token) {
  return `http://${HOSTNAME}/sign-up/confirm-email/${token}`;
}

function passwordLink(token) {
  return `http://${HOSTNAME}/reset-password/${token}`;
}

function usersFileLink(file) {
  return `http://${HOSTNAME}/data/${file}`;
}

module.exports = {
  confirmationLink: confirmationLink,
  passwordLink:     passwordLink,
  usersFileLink:    usersFileLink
};
