// region Module dependencies.
const EmailTemplate =     require('email-templates').EmailTemplate;
const nodemailer =        require('nodemailer');
const path =              require('path');
const VError =            require('verror');
const Promise =           require('bluebird');
const _ =                 require('lodash');

const mailgunTransport =  require('./transports/mailgun-transport');
const config =            require('../config');
const errorService =      require('./errorService').errorOutput;
// endregion

// region Email constants.
const TEMPLATE_DIR =  'lib/server/services/templates/';
const MAIL_FROM =     config.get('mail.from');
const SUGNATURE =     config.get('mail.emailSupport');
const SUBJECT =       config.get('mail.notificationSubject');
let _mailer;
// endregion

// region Email notifications.
function getMailer() {
  if (!_mailer) {
    const auth = config.get('mailgun');
    _mailer = nodemailer.createTransport(mailgunTransport(auth));
    _mailer.sendMail = Promise.promisify(_mailer.sendMail);
  }
  return _mailer;
}

function generationEmailNotification(notification) {
  const context = {
    emailSupport: SUGNATURE
  };
  _.assign(context, notification.data);
  const mailOptions = {
    to: notification.email,
    subject: notification.subject || SUBJECT
  };
  return renderAndSendEmail(notification.name, context, mailOptions);
}

/**
 *
 * @param templateName - Name of dir, where located template
 * @param {Object} renderEntity
 * @param {Object} mailOptions
 * @param attachments
 * @param mailOptions.to
 * @param mailOptions.subject
 * @returns {Promise|Promise.<T>}
 */
function renderAndSendEmail(templateName, renderEntity, mailOptions) {
  if (!mailOptions.to || !mailOptions.subject) {
    return Promise.reject(new VError('mailOptions.to and mailOptions.subject must be not empty'));
  }
  const templatePath = path.resolve(`${TEMPLATE_DIR}`, templateName);
  const template = new EmailTemplate(templatePath);
  return template.render(renderEntity)
    .then((renders) => {
      const mailer = getMailer();
      mailOptions.from = MAIL_FROM;
      mailOptions.text = renders.text;
      mailOptions.html = renders.html;
      return mailer.sendMail(mailOptions)
        .then((info) => {
          return console.log(info);
        });
    })
    .catch((err) => {
      err = err instanceof Error ? err : new VError(err);
      errorService(err, 'Unexpected Error in renderAndSendEmail');
    });
}
// endregion

module.exports = {
  generationEmailNotification: generationEmailNotification
};
